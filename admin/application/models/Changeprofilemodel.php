<?php

class Changeprofilemodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
    public function get_pf_data($admin_id)
    {
        $query = $this->db->get_where('tbl_admin',array('admin_id'=>$admin_id));
        return $query->row_array();
    }

    public function edit_prf($admin_id,$data_array)
    {
        $this->db->where('admin_id',$admin_id);
        $query = $this->db->update('tbl_admin',$data_array);
        if($query)
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }
    
    public function edit_pass($admin_id,$new_password)
    {
        $this->db->where('admin_id',$admin_id);
        $query = $this->db->update('tbl_admin',array('admin_password'=>$new_password));
        if($query)
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }
}