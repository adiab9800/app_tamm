<?php

class Addonsmodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
    public function get_addon_data()
    {
        $this->db->order_by('addon_id','DESC');
                 $this->db->join('tbl_package','tbl_package.package_id = tbl_addon.addon_packid','LEFT');
        $query = $this->db->get_where('tbl_addon',array('addon_delete'=>'1'));
        return $query->result_array();
    }
  
    public function get_packing_data()
    {
        $this->db->order_by('package_id','ASC');
                 
        $query = $this->db->get_where('tbl_package',array('package_delete'=>'1'));
        return $query->result_array();
    }

    public function Add_loc($data_ca)
    {
        $query = $this->db->get_where('tbl_addon',array('addon_name'=>$data_ca['addon_name'],'addon_packid'=>$data_ca['addon_packid'],'addon_delete '=>'1'));
        if($query->num_rows() == 0)
        {
           
            $this->db->insert('tbl_addon',$data_ca);
            $messge = array('message_del' => 'AddOns Add Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'AddOns Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    public function selected_addon_data($addon_id)
    {
        $query = $this->db->get_where('tbl_addon',array('addon_id'=>$addon_id));
        return $query->row_array(); 
    }



    public function Edit_loc($data_ca)
    {
        $this->db->where('addon_id',$data_ca['addon_id']);
        $query = $this->db->update('tbl_addon',$data_ca);
        
        if($query)
        {
           
            
            $messge = array('message_del' => 'Addons Edit Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Addons Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    
    
    
    
  
    public function Delete($addon_id)
    {
        $this->db->set('addon_delete','2');
        $this->db->where('addon_id',$addon_id);
        $this->db->update('tbl_addon');
        $messge = array('message_del' => 'Addons Delete Successfully','message_type' => 'Success');
        $this->session->set_flashdata('item', $messge);
        return 1;    
    }
    
    public function subcat_bycat($cat_id)
    {
        $query = $this->db->get_where('tbl_location',array('cat_id'=>$cat_id));
        return $query->result_array();
    }

    
}