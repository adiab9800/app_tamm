<?php

class Booking_statusmodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
    public function get_booking_data()
    {
        $this->db->order_by('booking_status_id','DESC');
        $query = $this->db->get('tbl_booking_status');
        return $query->result_array();
    }

    public function selected_booking_data($booking_id)
    {
        $query = $this->db->get_where('tbl_booking_status',array('booking_status_id'=>$booking_id));
        return $query->row_array();
    }
    
    public function Edit_c($booking_id,$data)
    {
        $this->db->where('booking_status_id',$booking_id);
        $this->db->update('tbl_booking_status',$data);
    }
    
    public function Delete($booking_id)
    {
        $this->db->where('booking_status_id',$booking_id);
        $this->db->delete('tbl_booking_status');
        return;
    }

    
}