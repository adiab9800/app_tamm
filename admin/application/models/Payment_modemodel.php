<?php

class Payment_modemodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
    public function get_mode_data()
    {
        $this->db->order_by('payment_mode_id','DESC');
        $query = $this->db->get('tbl_payment_mode');
        return $query->result_array();
    }

    public function selected_booking_data($booking_id)
    {
        $query = $this->db->get_where('tbl_payment_mode',array('payment_mode_id'=>$booking_id));
        return $query->row_array();
    }
    
    public function Edit_c($booking_id,$data)
    {
        $this->db->where('payment_mode_id',$booking_id);
        $this->db->update('tbl_payment_mode',$data);
    }
    
    public function Delete($booking_id)
    {
        $this->db->where('payment_mode_id',$booking_id);
        $this->db->delete('tbl_payment_mode');
        return;
    }

    
}