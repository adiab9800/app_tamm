<?php

class Locationmodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
    public function get_subcat_data()
    {
        $this->db->order_by('location_id','DESC');
        $query = $this->db->get_where('tbl_location',array('location_delete'=>'1'));
        return $query->result_array();
    }

    public function selected_location_data($location_id)
    {
       // $this->db->join('tbl_category', 'tbl_category.cat_id = tbl_location.cat_id');
        $query = $this->db->get_where('tbl_location',array('location_id'=>$location_id));
        return $query->row_array();
    }
    
    public function Add_loc($data_ca)
    {
        $query = $this->db->get_where('tbl_location',array('location_state'=>$data_ca['location_state'],'location_city'=>$data_ca['location_city'],'location_delete '=>'1'));
        if($query->num_rows() == 0)
        {
           
            $this->db->insert('tbl_location',$data_ca);
            $messge = array('message_del' => 'Location Add Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Location Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    public function Edit_loc($data_ca)
    {
        $this->db->where('location_id',$data_ca['location_id']);
        $query = $this->db->update('tbl_location',$data_ca);
        
        if($query)
        {
           
            
            $messge = array('message_del' => 'Location Edit Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Location Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    
    
    
    
  
    public function Delete($location_id)
    {
        $this->db->set('location_delete','2');
        $this->db->where('location_id',$location_id);
        $this->db->update('tbl_location');
        $messge = array('message_del' => 'Location Delete Successfully','message_type' => 'Success');
        $this->session->set_flashdata('item', $messge);
        return 1;    
    }
    
    public function subcat_bycat($cat_id)
    {
        $query = $this->db->get_where('tbl_location',array('cat_id'=>$cat_id));
        return $query->result_array();
    }

    
}