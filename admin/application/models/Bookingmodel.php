<?php

class Bookingmodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
    public function get_booking_data()
    {
        $this->db->order_by('booking_id','DESC');
        $query = $this->db->get('tbl_booking');
        return $query->result_array();
    }

    public function selected_booking_data($booking_id)
    {
        $query = $this->db->get_where('tbl_booking',array('booking_id'=>$booking_id));
        return $query->row_array();
    }
    
    public function Edit_c($booking_id,$data)
    {
        $this->db->where('booking_id',$booking_id);
        $this->db->update('tbl_booking',$data);
    }
    
    public function Delete($booking_id)
    {
        $this->db->where('booking_id',$booking_id);
        $this->db->delete('tbl_booking');
        return;
    }

    
}