<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiModel extends CI_Model {

	public function __construct()
    {
        parent::__construct();   
    }
    
    public function login($user_phno)
    {
        $query = $this->db->get_where('tbl_user',array('user_phno'=>$user_phno,'user_delete'=>'0'));
        
        
        if($query->num_rows() == '1')
        {
            $rslt = $query->row_array();
          //  $random_no = rand(1,9).$rslt['user_id'].date('d').rand(1,9);
            
            $random_no = rand(000000,999999);
                   $this->db->set('user_verify',$random_no);
                   $this->db->where('user_id',$rslt['user_id']);
            $ins = $this->db->update('tbl_user');
            if($ins)
            {
                $data_array= array('user_id'=>$rslt['user_id'],'verify_code'=>$random_no);
                return $rslt;
            }
        }
        else
        {
            return 0;
        }
    }
    
    public function washer_login($user_phno)
    {
        $query = $this->db->get_where('tbl_washer',array('user_phno'=>$user_phno,'user_delete'=>'0'));
        
        
        if($query->num_rows() == '1')
        {
            $rslt = $query->row_array();
          //  $random_no = rand(1,9).$rslt['user_id'].date('d').rand(1,9);
            
            $random_no = rand(000000,999999);
                   $this->db->set('user_verify',$random_no);
                   $this->db->where('user_id',$rslt['user_id']);
            $ins = $this->db->update('tbl_washer');
            if($ins)
            {
                $data_array= array('user_id'=>$rslt['user_id'],'verify_code'=>$random_no);
                return $rslt;
            }
        }
        else
        {
            return 0;
        }
    }
    
    public function verify_login($user_id,$user_verifycode)
    {
       $query = $this->db->get_where('tbl_user',array('user_id'=>$user_id,'user_verify'=>$user_verifycode,'user_delete'=>'0'));
        if($query->num_rows() == '1')
        {
            return 1;
            
        }
        else
        {
            return 0;
        } 
    }
    
    
     public function edit_profile($data)
    {
        $this->db->where('user_id',$data['user_id'],'user_phno',$data['user_phno']);
        $res = $this->db->update('tbl_user',$data);
        if($res)
        {
            return 1;
            
        }
        else
        {
            return 0;
        } 
    }
    
    public function booking($user_array)
    {
         $ins = $this->db->insert('tbl_booking',$user_array);
         if($ins)
         {
             return 0;
         }
         else{
             return 1;
         }
    }
    public function Register($user_array)
    {
        $query = $this->db->get_where('tbl_user',array('user_phno'=>$user_array['user_phno'],'user_delete'=>'0'));
        if($query->num_rows() == '0')
        {
            $ins = $this->db->insert('tbl_user',$user_array);
            if($ins)
            {
             
             $user_id = $this->db->insert_id();
             $this->db->select("*");
             $this->db->from('tbl_user');
             $this->db->where('user_id',$user_id);
             
             $res = $this->db->get();
             return $res;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }
    
     public function add_vehicle($user_array)
    {
        $query = $this->db->get_where('tbl_vehicle',array('vhl_name'=>$user_array['vhl_name'],'vhl_delete'=>'1'));
        if($query->num_rows() == '0')
        {
            $ins = $this->db->insert('tbl_vehicle',$user_array);
            if($ins)
            {
             
             $user_id = $this->db->insert_id();
             $this->db->select("*");
             $this->db->from('tbl_vehicle');
             $this->db->where('vhl_id',$user_id);
             
             $res = $this->db->get();
             return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }
    
    
     public function add_user_vehicle($user_array)
    {
        $query = $this->db->get_where('tbl_vhl_detail',array('vhl_detl_vhlno'=>$user_array['vhl_detl_vhlno']));
        if($query->num_rows() == '0')
        {
            $ins = $this->db->insert('tbl_vhl_detail',$user_array);
            if($ins)
            {
             
             /*$user_id = $this->db->insert_id();
             $this->db->select("*");
             $this->db->from('tbl_vehicle');
             $this->db->where('vhl_id',$user_id);
             $res = $this->db->get();*/
             return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }
    
    public function Getvehiclebyuser($vhl_detl_userid)
    {
         $this->db->select('tbl_vhl_detail.*,tbl_vehicle_type.*');
         
                $this->db->join('tbl_vehicle','tbl_vehicle.vhl_id = tbl_vhl_detail.vhl_detl_vhlid','LEFT');
                $this->db->join('tbl_vehicle_type','tbl_vehicle_type.vhl_type_id = tbl_vehicle.vhl_category','LEFT');
         $res = $this->db->get_where('tbl_vhl_detail',array('vhl_detl_userid'=>$vhl_detl_userid));
         return $res;
        
       
        
    }
    
    public function get_addons($package_id)
    {
        $this->db->select("*");
        $this->db->from('tbl_addon');
        $this->db->where('addon_packid',$package_id);
        $this->db->where('addon_delete','1');
        
        $this->db->order_by("addon_id", "ASC");
        $res = $this->db->get();
        return $res;
    }
    
    
     public function get_subservices($package_id)
    {
        $this->db->select("*");
        $this->db->from('tbl_subservice');
        $this->db->where('sub_packid',$package_id);
        $this->db->where('sub_delete','1');
        
        $this->db->order_by("sub_servid", "ASC");
        $res = $this->db->get();
        return $res;
    }
    
    public function Get_location()
    {
        $this->db->select("*");
        $this->db->from('tbl_location');
        $this->db->where('location_delete','1');
        $this->db->where('location_status','1');
       // $this->db->group_by("location_state");
        $this->db->order_by("location_state", "ASC");
        $res = $this->db->get();
        return $res;
    }
    
    public function Get_service_bycity($location_id)
    {
        $this->db->select("*");
        $this->db->from('tbl_location');
        $this->db->where('location_delete','1');
        $this->db->where('location_status','1');
        $this->db->order_by("location_city", "ASC");
        $this->db->where("location_id", $location_id);
        $res = $this->db->get();
        return $res;
    }
    
    public function Get_location_bystate($state_name)
    {
        $this->db->select("*");
        $this->db->from('tbl_location');
        $this->db->where('location_delete','1');
        $this->db->where('location_status','1');
        $this->db->order_by("location_city", "ASC");
        $this->db->where("location_state", $state_name);
        $res = $this->db->get();
        return $res;
    }
    
    public function GetPackagedetail($vhl_id,$srv_id)
    { 
               $this->db->join('tbl_category','tbl_category.cat_id = tbl_package.package_service','LEFT');  
        $res = $this->db->get_where('tbl_package',array('package_vehicle'=>$vhl_id,'package_delete'=>'1','package_service'=>$srv_id));
        return $res;
    }
    
    public function GetAllVehicledetail()
    {
        $this->db->select("*");
        $this->db->from('tbl_vehicle_type');
        $this->db->where('vhl_type_delete','1');
        $this->db->order_by("vhl_type_name", "ASC");
        $res = $this->db->get();
        return $res;
    }
    
    public function GetAllVehicles($cat_id)
    {
        $this->db->select("*");
        $this->db->from('tbl_vehicle');
        $this->db->where('vhl_category',$cat_id);
        $this->db->where('vhl_delete','1');
        $this->db->order_by("vhl_name", "ASC");
        $res = $this->db->get();
        return $res;
    }
    
    
    public function GetAllVehicle()
    {
        $this->db->select("*");
        $this->db->from('tbl_vehicle');
        $this->db->where('vhl_delete','1');
        $this->db->order_by("vhl_name", "ASC");
        $res = $this->db->get();
        return $res;
    }
    
    public function Get_service($vehicle_type)
    {
        $this->db->select("*");
        $this->db->from('tbl_category');
        $this->db->where('cat_delete','0');
        $this->db->where("vehicle_type", $vehicle_type);
        $this->db->order_by("cat_name", "ASC");
        $res = $this->db->get();
        return $res;
    }
    
    public function GetAllCategory()
    {
        $this->db->select("*");
        $this->db->from('tbl_category');
        $this->db->where('cat_delete','0');
        $this->db->order_by("cat_name", "ASC");
        $res = $this->db->get();
        return $res;
    }
    
    
    public function GetAllBanner()
    {
        $this->db->select("*");
        $this->db->from('tbl_banner');
        $this->db->where('banner_status','0');
        $this->db->order_by("banner_id", "DESC");
        $res = $this->db->get();
        return $res;
    }
    
    
    public function GetAllSubcategory($cat_id)
    {
        $this->db->select("*");
        $this->db->from('tbl_subcategory');
        $this->db->where('subcat_delete','0');
        $this->db->where('cat_id',$cat_id); 
        $this->db->order_by("subcat_name", "ASC");
        $res = $this->db->get();
        return $res;
    }
    public function Get_category_by_location($location)
    {
        $this->db->select("*");
        $this->db->from('tbl_category');
        $this->db->where('cat_city',$location); 
        $this->db->order_by("cat_name", "ASC");
        $res = $this->db->get();
        return $res;
    }


    public function manage_wallet($op_id,$user_id,$amount,$msg_del)
    {
        $user_data = $this->db->get_where('tbl_user',array('user_id'=>$user_id))->row_array();
        
        if($op_id == '1')
        {
             $tl_amount = $user_data['user_wallet'] + $amount ;
             $this->db->set('user_wallet',$tl_amount);
             $this->db->where('user_id',$user_id);
             $this->db->update('tbl_user');
             
             $msg = $msg_del." : ".$amount;
             $Data_ar  = array(
                 'trans_msg' =>$msg,
                 'trans_db_cr' =>'Cr',
                 'trans_time'=> date('Y-m-d H:i:s'),
                 'trans_userid'=>$user_id
                 );
                $this->db->insert('tbl_transaction',$Data_ar);
                return 1;
        }
        else
        {
            $tl_amount = $user_data['user_wallet'] - $amount ;
             $this->db->set('user_wallet',$tl_amount);
             $this->db->where('user_id',$user_id);
             $this->db->update('tbl_user');
             
             $msg = $msg_del." : ".$amount;
             $Data_ar  = array(
                 'trans_msg' =>$msg,
                 'trans_db_cr' =>'Dr',
                 'trans_time'=> date('Y-m-d H:i:s'),
                 'trans_userid'=>$user_id
                 );
                $this->db->insert('tbl_transaction',$Data_ar);
                return 2;
        }
    }

  public function Add_Address($data_array)
    {
        $ins = $this->db->insert('tbl_user_address',$data_array);
        if($ins) {
            return $data_array['addr_userid'];
        }
    }
    
    public function edit_Address($address_array)
    {
        $this->db->where('addr_id',$address_array['addr_id']);
        $query = $this->db->update('tbl_user_address',$address_array);
        if($query) {
            return $address_array['addr_userid'];
        }
    }
    
    public function delete_Address($addr_id,$addr_userid)
    {
        $this->db->set('addr_delete','1');
        $this->db->where('addr_id',$addr_id);
        $this->db->where('addr_userid',$addr_userid);
        $query = $this->db->update('tbl_user_address');
        if($query)
        {
            return $addr_userid;
        }
    }
     public function delete_vehicle($vehicle_id,$userid)
    {
       $this->db->where('vhl_detl_id', $vehicle_id);
       $this->db->where('vhl_detl_userid', $userid);
       $query= $this->db->delete('tbl_vhl_detail');
       if($query)
        {
            return 1;
        }
    }
    
    public function Get_Address_ByUser($userid) {
        $this->db->select("*");        
        $this->db->where('addr_userid',$userid);
        $this->db->where('addr_delete','0');
        $this->db->order_by("addr_id", "dESC");
        $res = $this->db->get('tbl_user_address');
        //$res = $res->result_array();
        return $res;
    }
  
   
}
