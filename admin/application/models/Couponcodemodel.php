<?php

class Couponcodemodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
	public function get_coupon_data()
	{
	    $this->db->order_by('coupon_id','ASC');
        $query = $this->db->get_where('tbl_coupon',array('coupon_delete'=>'0'));
        return $query->result_array();
	    
	}
	
	public function get_product_data()
	{
	    $this->db->order_by('prod_id','ASC');
        $query = $this->db->get_where('tbl_product',array('prod_delete'=>'0'));
        return $query->result_array();
	}
	
    public function get_cat_data()
    {
        $this->db->order_by('cat_id','DESC');
        $query = $this->db->get_where('tbl_category',array('cat_delete'=>'0'));
        return $query->result_array();
    }

    public function selected_coupon_data($coupon_id)
    {
        $query = $this->db->get_where('tbl_coupon',array('coupon_id'=>$coupon_id));
        return $query->row_array();
    }
    
    public function Add_c($data_prdt)
    {
        $query = $this->db->get_where('tbl_coupon',array('coupon_code'=>$data_prdt['coupon_code'],'coupon_delete'=>'0'));
        if($query->num_rows() == 0)
        {
           
            $this->db->insert('tbl_coupon',$data_prdt);
            $messge = array('message_del' => 'Coupon code Added successfully','message_type' => 'success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Coupon code already Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    public function Edit_c($data_prdt,$coupon_id)
    {
       
            
            $this->db->where('coupon_id',$coupon_id);
           $query= $this->db->update('tbl_coupon',$data_prdt);
           
       if($query)
       {
        $messge = array('message_del' => 'Coupon code Edited successfully','message_type' => 'success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Coupon code Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    public function Get_Data($coupon_id,$coupon_type)
    {
        $query = $this->db->get_where('tbl_coupon',array('coupon_id'=>$coupon_id));
        return $query->row_array();
    }
    
    
    public function Pending_Request()
    {
        $this->db->join('tbl_merchant_register','tbl_merchant_register.mrnt_id = tbl_coupon.coupon_addby','Left');
        $query = $this->db->get_where('tbl_coupon',array('coupon_status'=>'2','coupon_delete'=>'0'));
        return $query->result_array();
    }
    
    public function Delete_data($data_prdt,$coupon_id)
    {
        $pr_value = implode(',',$data_prdt);
        $this->db->set('coupon_prdt',$pr_value);
        $this->db->where('coupon_id',$coupon_id);
        $this->db->update('tbl_coupon');
        $messge = array('message_del' => 'Data Delete successfully','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
        return 1;  
    }
    
    public function Delete_coupon($coupon_id)
    {
        $this->db->set('coupon_delete','1');
        $this->db->where('coupon_id',$coupon_id);
        $this->db->update('tbl_coupon');
        $messge = array('message_del' => 'Coupon code Delete successfully','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
        return 1;    
    }

    public function Accept($coupon_id)
    {
        $this->db->set('coupon_status','1');
        $this->db->where('coupon_id',$coupon_id);
        $this->db->update('tbl_coupon');
        $messge = array('message_del' => 'Coupon code Request Accept successfully','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
        return 1; 
    }
    
}