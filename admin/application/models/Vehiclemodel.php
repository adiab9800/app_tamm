
<?php

class Vehiclemodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
	public function get_vehivle_type_data()
	{
	   $query = $this->db->get_where('tbl_vehicle_type',array('vhl_type_delete'=>'1'));
        return $query->result_array(); 
	}
	
    public function get_vehicle_data()
    {
        $this->db->order_by('vhl_id','DESC');
        $this->db->join('tbl_model', 'tbl_model.model_id = tbl_vehicle.model_id','LEFT');
        $this->db->join('tbl_type', 'tbl_type.type_id = tbl_vehicle.type_id','LEFT');
        $this->db->join('tbl_user', 'tbl_user.user_id = tbl_vehicle.user_id','LEFT');

        $this->db->join('tbl_brand', 'tbl_brand.brand_id = tbl_vehicle.brand_id','LEFT');
        $query = $this->db->get_where('tbl_vehicle',array('vhl_delete'=>'1'));
        return $query->result_array();
    }

    public function selected_vhl_data($vhl_slug)
    {
        $this->db->order_by('vhl_id','DESC');
        $this->db->join('tbl_model', 'tbl_model.model_id = tbl_vehicle.model_id','LEFT');
        $this->db->join('tbl_type', 'tbl_type.type_id = tbl_vehicle.type_id','LEFT');
        $this->db->join('tbl_user', 'tbl_user.user_id = tbl_vehicle.user_id','LEFT');

        $this->db->join('tbl_brand', 'tbl_brand.brand_id = tbl_vehicle.brand_id','LEFT');
       $query = $this->db->get_where('tbl_vehicle',array('vhl_id'=>$vhl_slug));
        return $query->row_array();
    }
    
    public function Add_subcat($data_su_ca)
    {
        $query = $this->db->get_where('tbl_vehicle',array('vhl_name'=>$data_su_ca['vhl_name'],'vhl_delete'=>'0'));
        if($query->num_rows() == 0)
        {
           
            $this->db->insert('tbl_vehicle',$data_su_ca);
            $messge = array('message_del' => 'Vehicle Added Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Vehicle Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    public function Edit_subcat($data_s_cat)
    {
                    $this->db->where('vhl_id',$data_s_cat['vhl_id']);
         $query =   $this->db->update('tbl_vehicle',$data_s_cat);
        if($query)
        {
           
            
            $messge = array('message_del' => 'Vehicle Edit Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Vehicle Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    
    
    
    
  
    public function Delete($vhl_id)
    {
        $this->db->set('vhl_delete','2');
        $this->db->where('vhl_slug',$vhl_id);
        $this->db->update('tbl_vehicle');
        $messge = array('message_del' => 'Vehicle Delete Successfully','message_type' => 'Success');
        $this->session->set_flashdata('item', $messge);
        return 1;    
    }
    
    public function subcat_bycat($cat_id)
    {
        $query = $this->db->get_where('tbl_subcategory',array('cat_id'=>$cat_id));
        return $query->result_array();
    }

    
}