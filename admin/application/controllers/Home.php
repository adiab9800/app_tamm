<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model('Homemodel');
	}
	
	public function index()
	{
		if ($this->session->userdata('is_admin_login')) {
			redirect(base_url('Dashboard'));
        } else {
           
            $this->load->view('login');
		}
			
	}
	
	public function notification()
	{
		if (!$this->session->userdata('is_admin_login')) {
			redirect(base_url('Dashboard'));
        } else {
           
            $this->load->view('notification');
		}
			
	}
	public function new_map()
	{
		if (!$this->session->userdata('is_admin_login')) {
			redirect(base_url('Dashboard'));
        } else {
           
            $this->load->view('new_map');
		}
			
	}
    public function new_map_c()
	{
		if (!$this->session->userdata('is_admin_login')) {
			redirect(base_url('Dashboard'));
        } else {
           
               $area=$this->input->post('area');
               $name=$this->input->post('name');
             
             $user_array = array(
            'area'=> $area,
            'short_name'=> $name,
              );
        $res = $this->db->insert('tbl_new_map',$user_array);
    	redirect(base_url('home/new_map'));

		}
			
	}
    public function delete_new_map($id)
	{
		if (!$this->session->userdata('is_admin_login')) {
			redirect(base_url('Dashboard'));
        } else {
           
            $this->db->where('new_map_id',$id);
            $result=$this->db->delete('tbl_new_map');
            if($result)
            {
                   $messge = array('message_del' => 'Data deleted successfully.','message_type' => 'success');
                   $this->session->set_flashdata('item', $messge);
            }
            else{
                   $messge = array('message_del' => 'data not deleted.Please try again.','message_type' => 'error');
                   $this->session->set_flashdata('item', $messge);
             
            }
            redirect(base_url('home/new_map'));

        
		}
			
	}
	public function select_location()
	{
		if (!$this->session->userdata('is_admin_login')) {
			redirect(base_url('Dashboard'));
        } else {
           
            $this->load->view('select_location');
		}
			
	}
    	
	public function select_location_c()
	{
		if (!$this->session->userdata('is_admin_login')) {
			redirect(base_url('Dashboard'));
        } else {
           if($this->input->post('area_name'))
           {
               $area=$this->input->post('area_name');
           }
           else{
               $area=$this->input->post('area');
           }
             $user_array = array(
            'area'=> $area,
              );
        $res = $this->db->insert('tbl_allowed_area',$user_array);
    	redirect(base_url('home/select_location'));

        
		}
			
	}
   
    public function delete_allowed_area($id)
	{
		if (!$this->session->userdata('is_admin_login')) {
			redirect(base_url('Dashboard'));
        } else {
           
            $this->db->where('allowed_area_id',$id);
            $result=$this->db->delete('tbl_allowed_area');
            if($result)
            {
                   $messge = array('message_del' => 'Data deleted successfully.','message_type' => 'success');
                   $this->session->set_flashdata('item', $messge);
            }
            else{
                   $messge = array('message_del' => 'data not deleted.Please try again.','message_type' => 'error');
                   $this->session->set_flashdata('item', $messge);
             
            }
            redirect(base_url('home/select_location'));

        
		}
			
	}

	
	public function user_count(){
	   $result=$this->db->query("SELECT COUNT(user_id) as Count 
        FROM tbl_user
        WHERE date(user_creattime) > DATE_SUB(NOW(), INTERVAL 1 WEEK) AND MONTH(user_creattime) = MONTH(CURDATE()) AND YEAR(user_creattime) = YEAR(CURDATE())
        GROUP BY DAYNAME(user_creattime) ORDER BY (user_creattime)")->result_array();
        $count=array();
        foreach($result as $result) {
            $count[]=$result['Count'];
        }
        $string = implode(',',$count) ;
        echo $string;
	}
	
	public function year_booking_count(){
	    
	    
	$result=$this->db->query("SELECT Months.m AS month, IFNULL(SUM(tbl_booking.total_amount), 0) AS total FROM ( SELECT 1 as m UNION SELECT 2 as m UNION SELECT 3 as m UNION SELECT 4 as m UNION SELECT 5 as m UNION SELECT 6 as m UNION SELECT 7 as m UNION SELECT 8 as m UNION SELECT 9 as m UNION SELECT 10 as m UNION SELECT 11 as m UNION SELECT 12 as m ) as Months LEFT JOIN tbl_booking on Months.m = MONTH(tbl_booking.service_date) GROUP BY Months.m")->result_array();

	  $count=array();
        foreach($result as $result) {
            $count[]=$result['total'];
        }
        $string = implode(',',$count) ;
        echo $string;
	}	

	public function year_user_count(){
	    
	    
	$result=$this->db->query("SELECT Months.m AS countrys,date_format(tbl_user.user_creattime,'%M') as country , IFNULL(COUNT(tbl_user.user_creattime), 0) AS visits FROM ( SELECT 1 as m UNION SELECT 2 as m UNION SELECT 3 as m UNION SELECT 4 as m UNION SELECT 5 as m UNION SELECT 6 as m UNION SELECT 7 as m UNION SELECT 8 as m UNION SELECT 9 as m UNION SELECT 10 as m UNION SELECT 11 as m UNION SELECT 12 as m ) as Months LEFT JOIN tbl_user on Months.m = MONTH(tbl_user.user_creattime) GROUP BY Months.m")->result_array();

        echo json_encode($result);
	}
	
	public function year_washer_count(){
	$result=$this->db->query("SELECT Months.m AS countrys,date_format(tbl_washer.user_creattime,'%M') as country , IFNULL(COUNT(tbl_washer.user_creattime), 0) AS visits FROM ( SELECT 1 as m UNION SELECT 2 as m UNION SELECT 3 as m UNION SELECT 4 as m UNION SELECT 5 as m UNION SELECT 6 as m UNION SELECT 7 as m UNION SELECT 8 as m UNION SELECT 9 as m UNION SELECT 10 as m UNION SELECT 11 as m UNION SELECT 12 as m ) as Months LEFT JOIN tbl_washer on Months.m = MONTH(tbl_washer.user_creattime) GROUP BY Months.m")->result_array();
    echo json_encode($result);
	}
   public function year_earning_count(){
	$result=$this->db->query("SELECT Months.m AS countrys, IFNULL(SUM(tbl_booking.total_amount), 0) AS visits,date_format(tbl_booking.service_date,'%M') as country FROM ( SELECT 1 as m UNION SELECT 2 as m UNION SELECT 3 as m UNION SELECT 4 as m UNION SELECT 5 as m UNION SELECT 6 as m UNION SELECT 7 as m UNION SELECT 8 as m UNION SELECT 9 as m UNION SELECT 10 as m UNION SELECT 11 as m UNION SELECT 12 as m ) as Months LEFT JOIN tbl_booking on Months.m = MONTH(tbl_booking.service_date) GROUP BY Months.m")->result_array();
    echo json_encode($result);
	}


	public function booking_count(){
	   $result=$this->db->query("SELECT date_format(tn.service_date,'%m') as Month, count(booking_id) as total_booking, date_format(tn.service_date,'%Y') as Year
        FROM tbl_booking tn
        GROUP BY Year,Month
        ORDER by Year,Month;")->result_array();
        $count=array();
        foreach($result as $result) {
            $count[]=$result['total_booking'];
        }
        $string = implode(',',$count) ;
        echo $string;
	}

	public function check_login()
	{
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));
		$result = $this->Homemodel->check_login($email,$password);
		if($result == 1)
		{
		
            $messge = array('message_del' => 'Login Successfully','message_type' => 'success');
            $this->session->set_flashdata('item', $messge);
			redirect(base_url('Dashboard'));
		}
		else{
			$messge = array('message_del' => 'Please Try Again Email And Password Wrong','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
			$this->load->view('login');
		}
	}

	public function logout() {
	   
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('admin_username');
        $this->session->unset_userdata('admin_email');
        $this->session->unset_userdata('is_admin_login');   
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
		$messge = array('message' => 'Successfully','message_title' => 'Logout','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
        redirect('Home');
		
    }

}
