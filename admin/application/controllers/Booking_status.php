<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_status extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Booking_statusmodel');
		
	}
	
	public function index()
	{
     
		$data['booking_data'] = $this->Booking_statusmodel->get_booking_data();
        $this->load->view('booking_status/view',$data);
        
    }
    public function Add()
	{
        $this->load->view('booking_status/add',$data);
    } 
    
    public function add_c()
	{
     
		  $data = array(
            'status' => $this->input->post('status'),
            'arabic_status' => $this->input->post('arabic_status'),
            
            );
         $this->db->insert('tbl_booking_status',$data);
        $messge = array('message_del' => 'Data Added Successfully!','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
     
      redirect('booking_status');
         
    }
    public function edit($id)
	{
        $data['booking_data'] = $this->db->get_where('tbl_booking_status',array('booking_status_id'=>$id))->row_array();
        $this->load->view('booking_status/edit',$data);
    } 
    public function view($id)
	{
        $data['booking_data'] = $this->db->get_where('tbl_booking_status',array('booking_status_id'=>$id))->row_array();
        $this->load->view('booking_status/view_data',$data);
    } 
    
    public function edit_c()
	{  
	      $booking_status_id=$this->input->post('booking_status_id');
	      $data = array(
            'status' => $this->input->post('status'),
            'arabic_status' => $this->input->post('arabic_status'),
            
            );
        $this->db->where('booking_status_id',$booking_status_id);
        $this->db->update('tbl_booking_status',$data);
        $messge = array('message_del' => 'Data Updated Successfully!','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
     
      redirect('booking_status');
         
    }
     public function delete($id)
	{    
         $this->db->where('booking_status_id',$id);
        $result=$this->db->delete('tbl_booking_status');
        if($result)
        {
               $messge = array('message_del' => 'Data deleted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not deleted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('booking_status');
      
    
    }
}
?>