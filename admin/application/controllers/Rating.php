<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rating extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
	}
	
	public function index()
	{
     
		if ($this->session->userdata('is_admin_login')) {
		   $this->db->select('tbl_rating.*,tbl_washer.user_id as washer_id,tbl_washer.user_fname as washer_fname,tbl_washer.user_lname as washer_lname,tbl_user.user_fname as user_fname,tbl_user.user_lname as user_lname');
    	   $this->db->join('tbl_washer','tbl_washer.user_id=tbl_rating.washer_id','LEFT');
    	   $this->db->join('tbl_user','tbl_user.user_id=tbl_rating.user_id','LEFT');
    	   $data['rating_data'] = $this->db->get_where('tbl_rating',array('user_washer'=>'1'))->result_array();
	       
            $this->load->view('rating/view',$data);
        } 
        else
        {
            $messge = array('message_del' => 'Login Please...','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
               redirect('Home');
       	}
    }
}?>