<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		
	}
	
	public function index()
	{
     
		if ($this->session->userdata('is_admin_login')) {
           
            $data['term_data'] = $this->db->get('tbl_about')->row_array();
            $this->load->view('about',$data);
        } 
        else
        {
            $messge = array('message_del' => 'Login Please...','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
               redirect('Home');
       	}
    }
    
    
    public function Edit_c()
	{
	    $data = array(
            'about_desc' => $this->input->post('about_desc')
            );
             $this->db->where('about_id','1');
        $this->db->update('tbl_about',$data);
        
        redirect('About');
    } 
    
    
}
?>