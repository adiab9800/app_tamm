<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offer extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model('Offermodel');
	}
	
	public function index()
	{
     
		if ($this->session->userdata('is_admin_login')) {
           
            $data['off_data'] = $this->Offermodel->get_offer_data();
            $this->load->view('offer/view',$data);
        } 
        else
        {
            $messge = array('message_del' => 'Login Please...','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
            redirect('Home');
       	}
    }
    
    public function Add()
    {
        $this->load->view('offer/add');
    }
    
    public function Add_c()
    {  
        $start_date = $this->input->post('offer_st_date');
        $exp_date = $this->input->post('offer_end_date');
        
       
       
         $data_ins = array(
             'offer_name'=> $this->input->post('offer_name'),
             'offer_code'=> $this->input->post('promo_code'),
             'offer_type' => $this->input->post('offer_type'),
             'offer_st_date' => $start_date,
             'offer_end_date' => $exp_date,
             'offer_val'=> $this->input->post('offer_val'),
             'offer_allowed'=> $this->input->post('off_user'),
             'offer_addby' => '0',
             'offer_delete' => '0',
            );
        $this->Offermodel->Add_c($data_ins);  
        redirect('Offer');
    }
    
    public function delete($off_id)
    {
        $this->db->where('offer_id',$off_id);
        $this->db->delete('tbl_offer');
        redirect('Offer');
    }
    
   
    
}
?>