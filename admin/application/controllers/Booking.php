<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model('Bookingmodel');
		$this->load->model('Washermodel');
	}
	
	public function index()
	{
        if ($this->session->userdata('is_admin_login')) {
            $this->db->select('tbl_category.cat_name,tbl_service_center.service_center_name,tbl_vehicle.vhl_no,tbl_vehicle.vhl_name,tbl_vehicle.vhl_id,tbl_washer.user_id as washer_id,tbl_washer.user_fname as washer_fname,tbl_washer.user_lname as washer_lname,tbl_washer.user_id,tbl_user.user_fname,tbl_user.user_lname,tbl_booking.* ');
            $this->db->join('tbl_vehicle','tbl_booking.vehicle_id=tbl_vehicle.vhl_id','LEFT');
		    $this->db->join('tbl_category','tbl_booking.cat_id=tbl_category.cat_id','LEFT');
		    $this->db->join('tbl_service_center','tbl_booking.service_center_id=tbl_service_center.service_center_id','LEFT');
		    $this->db->join('tbl_washer','tbl_washer.user_id=tbl_booking.washer_id','LEFT');
		    $this->db->join('tbl_user','tbl_user.user_id=tbl_booking.user_id','LEFT');
		    $data['booking']=$this->db->get_where('tbl_booking')->result_array();
		    $data['washer_data'] = $this->Washermodel->get_user_data();
          
		    $this->load->view('booking/view',$data);
        } 
        else
        {
            $messge = array('message_del' => 'Login Please...','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
            redirect('Home');
       	}
    }

    public function Add()
    {
        $this->load->view('booking/add');
    }

    public function Add_c()
    {
       $data = array(
            'user_id' => $this->input->post('user_id'),
            'washer_id' => $this->input->post('washer_id'),
            'vehicle_id' => $this->input->post('vehicle_id'),
            //'subpackage_id' => $this->input->post('subpackage_id'),
            'service_date' => $this->input->post('service_date'),
            'service_time' => $this->input->post('service_time'),
            'payment_mode' => $this->input->post('payment_mode'),
            'booking_desc' => $this->input->post('booking_desc'),
            'booking_status' => $this->input->post('booking_status'),
            
            );
         $this->db->insert('tbl_booking',$data);
        $messge = array('message_del' => 'Data Added Successfully!','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
        redirect('Booking');
    }
   
    public function Edit($id)
    {       $this->db->select('tbl_category.cat_name,tbl_service_center.service_center_name,tbl_vehicle.vhl_no,tbl_vehicle.vhl_name,tbl_vehicle.vhl_id,tbl_washer.user_id as washer_id,tbl_washer.user_fname as washer_fname,tbl_washer.user_lname as washer_lname,tbl_washer.user_id,tbl_user.user_fname,tbl_user.user_lname,tbl_booking.* ');
            $this->db->join('tbl_vehicle','tbl_booking.vehicle_id=tbl_vehicle.vhl_id','LEFT');
		    $this->db->join('tbl_category','tbl_booking.cat_id=tbl_category.cat_id','LEFT');
		    $this->db->join('tbl_service_center','tbl_booking.service_center_id=tbl_service_center.service_center_id','LEFT');
		    $this->db->join('tbl_washer','tbl_washer.user_id=tbl_booking.washer_id','LEFT');
		    $this->db->join('tbl_user','tbl_user.user_id=tbl_booking.user_id','LEFT');
		    $data['booking_data']=$this->db->get_where('tbl_booking',array('booking_id'=>$id))->row_array();
	    $this->db->order_by('user_id','asc');
        $this->db->where('user_status','0');
        $data['user_data'] = $this->db->get('tbl_user')->result_array();
    
        $this->db->order_by('user_id','asc');
        $this->db->where('user_status','0');
        $data['washer_data'] = $this->db->get('tbl_washer')->result_array();
    
        $this->db->order_by('vhl_id','asc');
        $data['vehicle_data'] = $this->db->get('tbl_vehicle')->result_array();
       
        $this->db->order_by('package_id','asc');
        $data['package_data'] = $this->db->get('tbl_package')->result_array();
      
      
        $this->load->view('booking/edit',$data);
    }
    
    public function view($id)
    {
          $this->db->select('tbl_category.cat_name,tbl_service_center.service_center_name,tbl_vehicle.vhl_no,tbl_vehicle.vhl_name,tbl_vehicle.vhl_id,tbl_washer.user_id as washer_id,tbl_washer.user_fname as washer_fname,tbl_washer.user_lname as washer_lname,tbl_washer.user_phno as washer_phno,tbl_washer.user_id,tbl_user.user_fname,tbl_user.user_lname,tbl_user.user_phno,tbl_booking.* ');
            $this->db->join('tbl_vehicle','tbl_booking.vehicle_id=tbl_vehicle.vhl_id','LEFT');
		    $this->db->join('tbl_service_center','tbl_booking.service_center_id=tbl_service_center.service_center_id','LEFT');
		    $this->db->join('tbl_washer','tbl_washer.user_id=tbl_booking.washer_id','LEFT');
		    $this->db->join('tbl_user','tbl_user.user_id=tbl_booking.user_id','LEFT');
		    $this->db->join('tbl_booking_service','tbl_booking_service.booking_id=tbl_booking.booking_id','LEFT');
		    $this->db->join('tbl_category','tbl_booking_service.service_id=tbl_category.cat_id','LEFT');
		    $data['booking_data']=$this->db->get_where('tbl_booking',array('tbl_booking.booking_id'=>$id))->row_array();
		
		  $this->db->order_by('user_id','asc');
        $data['user_data'] = $this->db->get('tbl_user')->result_array();
    
        $this->db->order_by('user_id','asc');
        $data['washer_data'] = $this->db->get('tbl_washer')->result_array();
    
        $this->db->order_by('vhl_id','asc');
        $data['vehicle_data'] = $this->db->get('tbl_vehicle')->result_array();
       
        $this->db->order_by('package_id','asc');
        $data['package_data'] = $this->db->get('tbl_package')->result_array();
      
      
        $this->load->view('booking/view_data',$data);
    }
     public function update()
    {
        $booking_id = $this->input->post('booking_id');
        $data = array(
             'washer_id' => $this->input->post('washer_id'),
            );
         $this->db->where('booking_id',$booking_id);
        $result= $data['booking_data'] = $this->db->update('tbl_booking',$data);
       
            if($result)
            {
                   $messge = array('message_del' => 'Data updated successfully.','message_type' => 'success');
                   $this->session->set_flashdata('item', $messge);
            }
            else{
                   $messge = array('message_del' => 'data not updated.Please try again.','message_type' => 'error');
                   $this->session->set_flashdata('item', $messge);
             
            }
         redirect('Booking');
    }
    
    public function send_notification($to,$message,$title){
                $message=$user_message;
                $msg = urlencode($message);
                $data = array(
                    'title'=>$title,
                    'sound' => "default",
                    'msg'=>$msg,
                    'data'=>$datapayload,
                    'body'=>$message,
                    'color' => "#79bc64"
                );
                $fields = array(
                    'to'=>$to,
                    'notification'=>$data,
                    'data'=>$datapayload,
                    "priority" => "high",
                );
                $headers = array(
                    'Authorization: key='.FCM_KEY,
                    'Content-Type: application/json'
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                curl_close( $ch );
                
                 
    }
    
    public function Edit_c()
    {
        $booking_id = $this->input->post('booking_id');
        $data = array(
            'user_id' => $this->input->post('user_id'),
            'washer_id' => $this->input->post('washer_id'),
            'vehicle_id' => $this->input->post('vehicle_id'),
            'total_amount' => $this->input->post('total_amount'),
            'service_center_id' => $this->input->post('service_center_id'),
            'service_date' => $this->input->post('service_date'),
            'service_time' => $this->input->post('service_time'),
            'booking_desc' => $this->input->post('booking_desc'),
            'booking_status' => $this->input->post('booking_status'),
            'payment_mode' => $this->input->post('payment_mode'),
           
            );
             $this->db->where('booking_id',$booking_id);
        $result= $data['booking_data'] = $this->db->update('tbl_booking',$data);
       
            if($result)
            {
                $user_data=$this->db->get_where('tbl_user',array('user_id'=>$this->input->post('user_id')))->row_array();
                
                $washer_data=$this->db->get_where('tbl_washer',array('user_id'=>$this->input->post('washer_id')))->row_array();


                if($this->input->post('booking_status')=='Accepted')
                {
                    $user_message="your booking ".$this->input->post('booking_id')." is accepted.";
                    $user_washer_message="your booking assigned to ".$washer_data['user_fname'].' '.$washer_data['user_lname'] ;
                    $washer_message="New order -".$this->input->post('booking_id')."  is assigned to you.";
                    $washer_message_1="You have received new order";
                    $this->send_notification($user_data['device_token'],'Booking status updated',$user_message);
                    $this->send_notification($user_data['device_token'],'Washer Assigned',$user_washer_message);
                    $this->send_notification($washer_data['device_token'],'Booking status updated',$washer_message);
                    $this->send_notification($washer_data['device_token'],'New Order',$washer_message_1);
                }
                
                if($this->input->post('booking_status')=='On Way')
                {
                    $user_message="Our washer is on his way. Start tracking.";
                    $this->send_notification($user_data['device_token'],'Booking status updated',$user_message);
                }

                if($this->input->post('booking_status')=='Washing')
                {
                    $user_message="Our washer reached your location to start washing.";
                    $this->send_notification($user_data['device_token'],'Booking status updated',$user_message);
                }
                if($this->input->post('booking_status')=='Completed')
                {
                    $user_message="Your car washing request successfully completed.";
                    $this->send_notification($user_data['device_token'],'Booking status updated',$user_message);
                }
                else{
                   $user_message="Status of your booking is changed to ".$this->input->post('booking_status');
                   $this->send_notification($user_data['device_token'],'Booking status updated',$user_message);
                }
           
           
                   $messge = array('message_del' => 'Data updated successfully.','message_type' => 'success');
                   $this->session->set_flashdata('item', $messge);
            }
            else{
                   $messge = array('message_del' => 'data not updated.Please try again.','message_type' => 'error');
                   $this->session->set_flashdata('item', $messge);
             
            }
         redirect('Booking');
    }

    public function Delete($user_id)
    {
        $result = $this->Bookingmodel->Delete($user_id);
        $messge = array('message_del' => 'Data Deleted Successfully!','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
        redirect('UserDetail');
    }
}
