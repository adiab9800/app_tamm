<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model('servicesmodel');
	    $this->load->library('upload');
	
		
	}
	
	public function index()
	{
     
	     $data['cat_data'] = $this->servicesmodel->get_cat_data();
         $this->load->view('services/view',$data);
       
    }
    public function Add()
	{
	    $data['vhl_data'] = $this->servicesmodel->vehicle_data();
         $this->load->view('services/add',$data);
    } 
    
    public function Add_c()
	{  
       
       if($this->input->post('add_category') == '10')
       {  
           if(!empty($_FILES['cat_image']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
               $currentTime = date( 'd-m-Y h:i:s A', time () );
               $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/categoryimage/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['cat_image']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('cat_image'))
                {
                    $uploadData = $this->upload->data();
                    $cat_image = base_url().'upload/categoryimage/'.$uploadData['file_name'];
                    
                }
                else
                {
                    $cat_image = '';
                }
            }
      
      
      
           $cat_slu = $this->common->slug_generate($this->input->post('cat_name'));
       
          $data_ct = array(
            'cat_name' => $this->input->post('cat_name'),
            'service_price' => $this->input->post('service_price'),
            'cat_arabic'=>$this->input->post('cat_arabic'),
            'cat_slug' => $cat_slu.$key,
            'cat_img'=>$cat_image,
            );
       
        $this->servicesmodel->Add_c($data_ct);
     
      }
      
      
      
        redirect('services');
    } 
    
     public function Edit($cat_slu)
	{
	    $data['vhl_data'] = $this->servicesmodel->vehicle_data();
	    $data['cat_data'] = $this->servicesmodel->selected_cat_data($cat_slu);
        $this->load->view('services/edit',$data);
    } 
    
    public function View($cat_slu)
	{
	    $data['vhl_data'] = $this->servicesmodel->vehicle_data();
	    $data['cat_data'] = $this->servicesmodel->selected_cat_data($cat_slu);
        $this->load->view('services/view_data',$data);
    } 
    
    public function Edit_c()
	{
	    $cat_id= $this->input->post('cat_id');
         $old_img = $this->input->post('old_img');

        if(!empty($_FILES['cat_image']['name']))
            {
                $config['upload_path'] = 'upload/categoryimage/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['cat_image']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('cat_image'))
                {
                    $uploadData = $this->upload->data();
                    $cat_image =  base_url().'upload/categoryimage/'.$uploadData['file_name'];
                    //unlink("upload/categoryimage/$old_img");
                }
                else
                {
                    $cat_image = $old_img;
                }
            }
        else
            {
                $cat_image = $old_img;
            }
        $cat_slu = $this->common->slug_generate($this->input->post('cat_name'));
        
        $data_ct = array(
            'cat_name' => $this->input->post('cat_name'),
            'service_price' => $this->input->post('service_price'),
            'cat_arabic'=>$this->input->post('cat_arabic'),
            'cat_slug' => $cat_slu,
            'cat_img'=>$cat_image,
            );
       
        $this->servicesmodel->Edit_c($cat_id,$data_ct);
        redirect('services');
    } 
    
    public function Delete($cat_slug)
    {
        $this->servicesmodel->Delete($cat_slug);
        redirect('services');
    }
}
?>