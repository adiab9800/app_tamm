<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Load the Rest Controller library
require APPPATH . '/libraries/RestController.php';
class Api extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
       $this->load->model('ApiModel');
        
    }
   
    public function add_feedback()
    {   
        $user_id=$this->input->post('user_id');    
       
        $msg=$this->input->post('msg');    
        $area=$this->db->insert('tbl_feedback',array('msg'=>$msg,'user_id'=>$user_id));
        if($area>0){
            echo json_encode(array("success"=>"1") );
        }
        else{
             echo json_encode(array("success"=>"0") );
        }
    }
 
    public function get_area()
    {   
        $locality=$this->input->post('locality');    
        $area=$this->db->get_where('tbl_allowed_area',array('area'=>$locality))->num_rows();
        if($area>0){
            echo json_encode(array("success"=>"1") );
        }
        else{
             echo json_encode(array("success"=>"0") );
        }
    }
    public function get_map()
    {
        $lat=$this->input->get('lat');
        $lng=$this->input->get('lng');
        if($lat && $lng)
        {
            try{
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&language=ar&key=AIzaSyDQ69wZR1GPEeLAxyu-vkSSo_dzpZTOV2c");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result =curl_exec($ch);
                curl_close($ch);
                $jsonData = json_decode($result,true);
                $addresses =$jsonData['results'][0]['address_components'] ;
                
                foreach($addresses as $address)
                {
                    $area=$this->db->get_where('tbl_new_map',array('short_name'=>$address['short_name']))->num_rows();
                    if($area>0){
                        echo json_encode(array("success"=>"1") );
                        die();
                    }
                }
                echo json_encode(array("success"=>"0") );
                die();
            }catch(Exception $e)
            {
                echo json_encode(array("success"=>"0") );
                die();
            }
        }
        else{
            echo json_encode(array("success"=>"0") );
            die();  
        }
    }
    public function get_dropdown()
    {   
        $this->db->order_by('brand_name','asc');
        $brand=$this->db->get('tbl_brand')->result_array();

        $this->db->order_by('type_name','asc');
        $type=$this->db->get('tbl_type')->result_array();
        
        echo json_encode(array("success"=>"1","brand" => $brand,"type" => $type) );
        
        
    }
    public function get_slot_day()
    {   
       // $this->db->distinct('slot_day');
       // $this->db->order_by('slot_day','asc');
       // $this->db->group_by('slot_day');
       // $brand=$this->db->get('tbl_slots')->result_array();
 $brand=$this->db->query('SELECT DISTINCT(slot_day) FROM tbl_slots  group by slot_day')->result_array();
       
        echo json_encode(array("success"=>"1","data" => $brand) );
    }
    public function get_slot_time()
    {   
        $slot_day=$_POST['day'];
        $this->db->where('slot_day',$slot_day);
        $this->db->order_by('slot_day','asc');
        $brand=$this->db->get('tbl_slots')->result_array();
        
        echo json_encode(array("success"=>"1","data" => $brand) );
    }
   
    public function add_card(){
        $check_card=$this->db->get_where('tbl_card',array('card_number'=>$this->input->post('card_number'),'user_id'=>$this->input->post('user_id')))->num_rows();
         if($check_card=='0')
         {
         $user_array = array(
            'user_id'=> $this->input->post('user_id'),
           // 'card_number'=> base64_encode($this->input->post('card_number')),
            'card_number'=> $this->input->post('card_number'),
            'expiry_date'=> $this->input->post('expiry_date'),
            'card_holder_name'=> $this->input->post('card_holder_name'),
            'cvv'=> $this->input->post('cvv'),
            );
        $res = $this->db->insert('tbl_card',$user_array);
        }
        else{
            $obj = array();
            echo json_encode(array("success"=>"0","message" => "Card already saved.") );
            exit;
        }
        $obj = array();   
        if($res)
        {
            $obj = array();
            echo json_encode(array("success"=>"1","message" => "Card added successfully.") );
            
        }
        else
        {
           echo json_encode(array("success"=>"0","message" => "Card not Added.") );
            
        }
    }
     public function get_card()
    {   
        extract($_POST);
        $cards=$this->db->get_where('tbl_card',array('user_id'=>$user_id))->result_array();
        if($cards)
        {
            
          
           
         echo json_encode(array("success"=>"1","data" => $cards) );
       
        }
        else{
                  echo json_encode(array("success"=>"2","message" => 'No data found.') );
            
        }
    }
    

     public function get_model()
    {   
        extract($_POST);
        $this->db->order_by('model_name','asc');
        $model=$this->db->get_where('tbl_model',array('brand_id'=>$brand_id))->result_array();
        if($model)
        {
                   echo json_encode(array("success"=>"1","model" => $model) );
        }
        else{
                  echo json_encode(array("success"=>"2","message" => 'No data found.') );
            
        }
    }
    
// ------------------- Manage Login / Register --------------
    
    public function login()
    {
        $user_phno = $this->input->post('phone');
        
        $query = $this->ApiModel->login($user_phno);
        $obj = array();
        if($query == '0')
        {
         	$random_no = rand(000000,999999);
            
         	array_push($obj,array("message"=>"Mobile Number Not Found."));
        
         echo json_encode(array("success"=>"2","data" => $obj));             //"data" => array($query_d)));
            
        }
        else
        {
            $user_id = $query['user_id'];
        	$verify_code = $query['verify_code'];
        	array_push($obj,$query);
            echo json_encode(array("success"=>"1","data" => $obj) );
        }        
    }
    public function washer_login()
    {
        $user_phno = $this->input->post('phone');
        
        $query = $this->ApiModel->washer_login($user_phno);
        $obj = array();
        if($query == '0')
        {
         	$random_no = rand(000000,999999);
            
         	array_push($obj,array("message"=>"Mobile Number Not Found."));
        
         echo json_encode(array("success"=>"2","data" => $obj));             //"data" => array($query_d)));
            
        }
        else
        {
            $user_id = $query['user_id'];
        	$verify_code = $query['verify_code'];
        	array_push($obj,$query);
            echo json_encode(array("success"=>"1","data" => $obj) );
        }        
    }
    
    public function get_country()
    {    $this->db->order_by('phonecode','asc');
          $result=$this->db->get('tbl_countries')->result_array();
         if(!$result)
        {
         
         echo json_encode(array("success"=>"2","message" => "Something went wrong.Please try again."));             //"data" => array($query_d)));
            
        }
        else
        { 
            echo json_encode(array("success"=>"1","data" => $result) );
        }       
        
    }
   
    public function services()
    {
        $obj=$res=array();
        $this->db->where('cat_delete','0');
        $this->db->select('cat_name,cat_id,cat_img,service_price');
        $res = $this->db->get('tbl_category')->result_array();
        array_push($obj,$res);
        
        if($res)
        {
            echo json_encode(array("success"=>"1","data" => $res));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
    
    public function PrivacyPolicy()
    {
        $res = $this->db->get_where('tbl_policy')->result();
        
        $obj = array();
        foreach ($res as $r) 
        {
        	
        	$msg = $r->policy_msg;
        
        	array_push($obj,array("policy"=>"$msg"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
     public function contact()
    {     $this->db->select('contact_us_phone_no,contact_us_email,contact_us_address,contact_us_longitude,contact_us_latitude');
          $result[]=$this->db->get_where('generalsetting')->row_array();
       
        if(!$result)
        {
         
         echo json_encode(array("success"=>"2","message" => "Something went wrong.Please try again."));             //"data" => array($query_d)));
            
        }
        else
        { 
            
            echo json_encode(array("success"=>"1","data" => $result) );
        }        
       
    }
    
    
     public function terms()
    {
        $res = $this->db->get_where('tbl_terms')->row_array();
        
        $obj = array();
        foreach ($res as $r) 
        {
        	array_push($obj,$res);
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
    
     public function About()
    {
        $res = $this->db->get_where('tbl_about')->result();
        
        $obj = array();
        foreach ($res as $r) 
        {
        	
        	$msg = $r->about_desc;
        
        	array_push($obj,array("About"=>"$msg"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
    
    public function Tax()
    {
        $res = $this->db->get_where('tbl_tax')->result();
        
        $obj = array();
        foreach ($res as $r) 
        {
        	
        	$msg = $r->tax_rate;
        	$discount = $r->tax_discount;
        
        	array_push($obj,array("Tax_Rate"=>"$msg",'Discount_Tax'=>"$discount"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
    
    
    
    
 
    public function Location()
    {
        $res = $this->ApiModel->Get_location()->result();
        
        $obj = array();
        foreach ($res as $r) 
        {
        	$loc_id = $r->location_id;
        	$loc_name = $r->location_state;
        	$loc_city = $r->location_city;
        	array_push($obj,array("location_id"=>"$loc_id","location_city"=>"$loc_city"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
    
    public function State_wise_location()
    {
        $state_name = $this->input->post('state_name');
        $res = $this->ApiModel->Get_location_bystate($state_name)->result();
        
        $obj = array();
        foreach ($res as $r) 
        {
        	$loc_id = $r->location_id;
        	$loc_city = $r->location_city;
        	array_push($obj,array("location_id"=>"$loc_id","location_city"=>"$loc_city"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
    
    public function manage_wallet()
    {
        $op_id = $this->input->post('operation_id');
        $user_id = $this->input->post('user_id');
        $amount = $this->input->post('amount');
        $msg_del = $this->input->post('msg');
        
        $res = $this->ApiModel->manage_wallet($op_id,$user_id,$amount,$msg_del);
        
        if($res == '1')
        {
            echo json_encode(array("success"=>"1","message" => "Money Added to Wallet."));
        }
        else
        {
            echo json_encode(array("success"=>"1","message" => "Money Deducted  from Wallet."));
        }
        
    }
    
    
    public function driver_data()
    {   
           $obj=$result=array();
           $completed=$this->db->get_where('tbl_booking',array('washer_id'=>$this->input->post('user_id'),'booking_status'=>'Completed'))->num_rows();
           $cancelled=$this->db->get_where('tbl_booking',array('washer_id'=>$this->input->post('user_id'),'booking_status'=>'Cancelled'))->num_rows();
           $requested=$this->db->get_where('tbl_booking',array('washer_id'=>$this->input->post('user_id'),'booking_status'=>'Pending'))->num_rows();
           $this->db->select('profile_pic,user_phno,country_id,user_fname,user_lname');
           $user_data=$this->db->get_where('tbl_washer',array('user_id'=>$this->input->post('user_id')))->row_array();
            $result['profilePic']=$user_data['profile_pic'];
            $result['name']=$user_data['user_fname'].' '.$user_data['user_lname'];
            $result['phone']=$user_data['country_id'].' '.$user_data['user_phno'];
            $result['completed']=$completed;
            $result['cancelled']=$cancelled;
            $result['pending']=$requested;
            array_push($obj,$result);
          if(!$result)
            {
             
             echo json_encode(array("success"=>"2","message" => "Something went wrong.Please try again."));             //"data" => array($query_d)));
                
            }
            else
            { 
                
                echo json_encode(array("success"=>"1","data" => $obj) );
            }    
    }
  
    public function driver_wallet()
    {   
        extract($_POST);
        $date=$end_date=date('Y-m-d');
        
        if($type=='0')
         {
            $start_date=$date;
         }
        if($type=='1')
         {
            $start_date= date('Y-m-d', strtotime($date. ' -7 days'));
         }
         if($type=='2')
         {
            $start_date= date('Y-m-d', strtotime($date. ' -30 days'));
         }
         $end_date=date('Y-m-d');
        $this->db->where('service_date >=', $start_date);
        $this->db->where('service_date <=', $end_date);
        $this->db->select('service_date,service_time,booking_id,total_amount');
        $result=$this->db->get_where('tbl_booking',array('washer_id'=>$this->input->post('user_id')))->result_array();
        $wallet=$this->db->get_where('tbl_washer',array('user_id'=>$this->input->post('user_id')))->row_array();
        if(!$result)
        {
             
             echo json_encode(array("success"=>"2","message" => "Something went wrong.Please try again."));             //"data" => array($query_d)));
                
        }
        else
        { 
                $obj=$row=array();
                $old_date='';
                foreach($result as $key=> $result)
                {
                    
                $date=date('d-M-Y',strtotime($result['service_date']));
                if($old_date=='')
                {
                 
                        $row['order_date']=date('d-M-Y',strtotime($result['service_date']));
                        $row['order_id']='';
                        $row['totalDeliveryCharge']=''; 
                        $old_date=$date;
                     	array_push($obj,$row);
            
                }
                 if($old_date!='')
                 {
                 
                
                 if($date!=$old_date)
                    {
                            $row['order_date']=date('d-M-Y',strtotime($result['service_date']));
                            $row['order_id']='';
                            $row['totalDeliveryCharge']='';
                           
                    }
                    else{
                      
                    
                    $row['order_date']=date('d-M-Y',strtotime($result['service_date']));
                    $row['order_id']='TAMM'.$result['booking_id'];
                    $row['totalDeliveryCharge']=$result['total_amount'];
                    }
                $old_date=$date;
            	array_push($obj,$row);
            	
                }
                
                }
            	
                echo json_encode(array("success"=>"1","earning" => $wallet['user_wallet'],"data" => $obj) );
        }    
    }
    public function notification()
    {   
        if($_POST['user_type']=='0')
        {
            $type="Customer";
        }
        if($_POST['user_type']=='1')
        {
            $type="Washer";
        }
        $result=$this->db->query('select * FROM tbl_notification WHERE (user_type="'.$type.'" OR user_type="All")')->result_array();
       
        if(!$result)
        {
         
         echo json_encode(array("success"=>"2","message" => "Something went wrong.Please try again."));             //"data" => array($query_d)));
            
        }
        else
        { 
            
            echo json_encode(array("success"=>"1","data" => $result) );
        }        
       
    }
     public function get_washer_profile()
    {
        $user_id=$this->input->post('user_id');  
        $user_data=$this->db->get_where('tbl_washer',array('user_id'=>$user_id))->row_array();
        $obj = array();
        if(!$user_data)
        {
         
         echo json_encode(array("success"=>"0","message" => "Oops..!Something  went wrong."));
            
        }
        else
        {   array_push($obj,$user_data);
            echo json_encode(array("success"=>"1","data" => $obj) );
        }        
    }
    public function edit_washer_profile()
    {
          $user_id=$this->input->post('user_id');  
          $user_data=$this->db->get_where('tbl_washer',array('user_id'=>$user_id))->row_array();
          $profile_pic=$user_data['profile_pic'];
        
           if(!empty($_FILES['profile_pic']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
                $currentTime = date( 'd-m-Y h:i:s A', time () );
                $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['profile_pic']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('profile_pic'))
                {
                    $uploadData = $this->upload->data();
                    $profile_pic= base_url().'upload/'.$uploadData['file_name'];
                    
                }
            }

            
        $data = array(
            'user_fname'=>$this->input->post('first_name'),
            'user_lname'=>$this->input->post('last_name'),
           // 'user_email'=>$this->input->post('email'),
            'profile_pic'=>$profile_pic,
           // 'country_id'=>$this->input->post('country_code'),
               );
         $this->db->where('user_id',$this->input->post('user_id'));
         $query = $this->db->update('tbl_washer',$data);
        $obj = array();
        if(!$query)
        {
         
         echo json_encode(array("success"=>"0","message" => "something went wrong. Profile not Updated."));
            
        }
        else
        {    $user_id = $_POST['user_id'];
            $user_data=$this->db->get_where('tbl_washer',array('user_id'=>$user_id))->row_array();
       
            array_push($obj,$user_data);
            echo json_encode(array("success"=>"1","data" => $obj,"message" => " Profile Update Successfully.") );
        }        
    }
   
     public function update_status()
    {   $status=$this->input->post('status');
        if($status=='Reject')
        {
            $status='Requested';
            $user_array = array(
            'washer_id'=>'0',
            'booking_status'=>$status,
            );
            
         $this->db->where('booking_id',$this->input->post('order_id'));
         $query = $this->db->update('tbl_booking',$user_array);

        }
        else{
            $status=$this->input->post('status');
            $user_array = array(
            'booking_status'=>$status,
            );
            
         $this->db->where('booking_id',$this->input->post('order_id'));
         $query = $this->db->update('tbl_booking',$user_array);
     
        }
       
        $obj = array();
        if(!$query)
        {
         
         echo json_encode(array("success"=>"2","message" => "Something went wrong.Please try again. Order not Updated."));             //"data" => array($query_d)));
            
        }
        else
        { 
            $result[]=$this->db->get_where('tbl_booking',array('booking_id'=>$this->input->post('order_id')))->row_array();
            
            $user_data=$this->db->get_where('tbl_user',array('user_id'=>$result[0]['user_id']))->row_array();
               
            $washer_data=$this->db->get_where('tbl_washer',array('user_id'=>$result[0]['washer_id']))->row_array();


                if($this->input->post('status')=='Accepted')
                {
                    $user_message="your booking ".$this->input->post('order_id')." is accepted.";
                    $user_washer_message="your booking assigned to ".$washer_data['user_fname'].' '.$washer_data['user_lname'];
                    $washer_message="New order -".$this->input->post('order_id')."  is assigned to you.";
                    $washer_message_1="You have received new order";
                    $this->send_notification($user_data['device_token'],'Booking status updated',$user_message);
                    $this->send_notification($user_data['device_token'],'Washer Assigned',$user_washer_message);
                    $this->send_notification($washer_data['device_token'],'Booking status updated',$washer_message);
                    $this->send_notification($washer_data['device_token'],'New Order',$washer_message_1);
                }
                
                if($this->input->post('status')=='On Way')
                {
                    $user_message="Our washer is on his way. Start tracking.";
                    $this->send_notification($user_data['device_token'],'Booking status updated',$user_message);
                }

                if($this->input->post('status')=='Washing')
                {
                    $user_message="Our washer reached your location to start washing.";
                    $this->send_notification($user_data['device_token'],'Booking status updated',$user_message);
                }
                if($this->input->post('status')=='Completed')
                {
                    $user_message="Your car washing request successfully completed.";
                    $this->send_notification($user_data['device_token'],'Booking status updated',$user_message);
                }
                else{
                   $user_message="Status of your booking is changed to ".$this->input->post('status');
                   $this->send_notification($user_data['device_token'],'Booking status updated',$user_message);
                }
           
            
            echo json_encode(array("success"=>"1","data" => $result,"message" => " Order Updated Successfully.") );
        }        
    }
    
    public function send_notification($to,$message,$title){
                $message=$user_message;
                $msg = urlencode($message);
                $data = array(
                    'title'=>$title,
                    'sound' => "default",
                    'msg'=>$msg,
                    'data'=>$datapayload,
                    'body'=>$message,
                    'color' => "#79bc64"
                );
                $fields = array(
                    'to'=>$to,
                    'notification'=>$data,
                    'data'=>$datapayload,
                    "priority" => "high",
                );
                $headers = array(
                    'Authorization: key='.FCM_KEY,
                    'Content-Type: application/json'
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                curl_close( $ch );
                
                 
    }
    
    public function update_lat_long()
    {   $booking_id=$this->input->post('order_id');
        $lat=$this->input->post('lat');
        $long=$this->input->post('long');
        $lat_long_time=$this->input->post('lat_long_time');
        $user_array = array(
            'driver_lat'=>$lat,
            'driver_long'=>$long,
            'lat_long_time'=>$lat_long_time
            );
            
         $this->db->where('booking_id',$this->input->post('order_id'));
         $query = $this->db->update('tbl_booking',$user_array);
     
        $obj = array();
        if(!$query)
        {
         
         echo json_encode(array("success"=>"2","message" => "Something went wrong.Please try again. Order not Updated."));             //"data" => array($query_d)));
            
        }
        else
        { 
             $result[]=$this->db->get_where('tbl_booking',array('booking_id'=>$this->input->post('order_id')))->row_array();
         
            echo json_encode(array("success"=>"1","data" => $result,"message" => "Location Updated.") );
        }        
    }
      
    public function booking_status()
    {
            $result=$this->db->get('tbl_booking_status')->result_array();
            if($result)
            {
            echo json_encode(array("success"=>"1","data" => $result) );
            }
            else{
               echo json_encode(array("success"=>"2","message" => "Something went wrong.Please try again."));             //"data" => array($query_d)));
          
            }
    }
    
    public function wallet_detail() {
       $user_id=$this->input->post('user_id');
    
       $result=$this->db->query("Select * from tbl_user WHERE user_id='$user_id'");
       $data= array();
       $count=$result->num_rows();
       
        if($count>0)
        {
            
         $data['staus']=0;
         $i=0;
         $row=$result->result_array();
         
            foreach($row as $row)
            { 
              $i++;
              $obj=$data2=array();
              $detail= array();
           
             $id=$row['user_wallet']; 
            $wallet=$row['user_wallet'];
            
            $result_img=$this->db->query("Select * from tbl_transaction WHERE trans_userid='$user_id'");
            $res_order=$result_img->result();
             
            $data['wallet'][$i]['wall_amount']=$id;
              foreach ($res_order as $r_order) 
                      
            {
                       $trans_msg = $r_order->trans_msg;
                       $trans_db_cr = $r_order->trans_db_cr;
                	   $trans_time = $r_order->trans_time;
                	   
                      
                       $detail[]=array("Detail"=>"$trans_msg","Operation"=>"$trans_db_cr","Timing"=>"$trans_time");
             
            
            }
             $data1[]=array(
              'wallet_amount'=>$wallet,
              'transaction_detail'=>$detail);
           
          }
          echo json_encode(array("success"=>"1","wallet" =>$data1));         
          
     }
        else{
            $data['staus']=1;
            $data['msg'] = 'No data found.';
                   
        	echo json_encode(array("success"=>"2","message" => "No Record found."));
        
        }
          
           
    }
 
    public function City_wise_service()
    {
        $location_id = $this->input->post('location_id');
        $vehicle_type = $this->input->post('vehicle_type');
        $res = $this->ApiModel->Get_service_bycity($location_id)->row();
        $obj = array();
        
        
        	$loc_city = explode(",",$res->location_wiseservice);
        	
        	$result = $this->ApiModel->Get_service($vehicle_type)->result();
        	
        	foreach ($result as $cat) 
            {
               if(in_array($cat->cat_id,$loc_city))
               {
                   	$loc_id = $res->location_id;
                   	$serv_id = $cat->cat_id;
                   	$serv_name = $cat->cat_name;
                   	$ser_img = $cat->cat_img;
                   	$price=$cat->service_price;
                   	array_push($obj,array("location_id"=>"$loc_id","service_id"=>"$serv_id","service_name"=>"$serv_name","service_image"=>"$ser_img",'price'=>$price));
               }
            }    
        	
        	
        
        
        
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
 
 
  public function Services_by_location()
    {
        $location_name=$this->input->post('city');
        $res = $this->ApiModel->Get_category_by_location($location_name)->result();
        
        $obj = array();
        foreach ($res as $r) 
        {
        	$cat_id = $r->cat_id;
        	$cat_name = $r->cat_name;
        	$cat_img = $r->cat_img;
        	array_push($obj,array("cat_id"=>"$cat_id","cat_name"=>"$cat_name","cat_img"=>"$cat_img"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
    
    public function get_addons()
    {
        $package_id=$this->input->post('package_id');
        $res = $this->ApiModel->get_addons($package_id)->result();
        
        $obj = array();
        foreach ($res as $r) 
        {
        	$addon_id = $r->addon_id;
        	$addon_name = $r->addon_name;
        	$addon_price = $r->addon_price;
        	$addon_desc = $r->addon_desc;
        	array_push($obj,array("addon_id"=>"$addon_id","addon_name"=>"$addon_name","addon_price"=>"$addon_price","addon_desc"=>"$addon_desc"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
    
    public function get_subservices()
    {
        $package_id=$this->input->post('package_id');
        $res = $this->ApiModel->get_subservices($package_id)->result();
        
        $obj = array();
        foreach ($res as $r) 
        {   
        	$subserv_id = $r->sub_servid;
        	$sub_name = $r->sub_name;
        	$sub_mainamount = $r->sub_mainamount;
        	$sub_discamount = $r->sub_discamount;
        	$sub_include = $r->sub_include;    
        	$sub_detail = $r->sub_detail;
        	array_push($obj,array("subserv_id"=>"$subserv_id","sub_name"=>"$sub_name","sub_mainamount"=>"$sub_mainamount","sub_discamount"=>"$sub_discamount","sub_include"=>"$sub_include","sub_detail"=>"$sub_detail"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }


    public function get_vehicle_detail()
    {
       
        $res = $this->ApiModel->GetAllVehicledetail()->result();
        
        $obj = array();
        foreach ($res as $r) 
        {
        	$vhl_id = $r->vhl_type_id;
        	$vhl_type = $r->vhl_type_name;
        	
        	array_push($obj,array("vehicle_id"=>"$vhl_id","vehicle_type"=>"$vhl_type"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
    
    
    public function get_package_detail()
    {
       
         $vhl_id = $this->input->post('vehicle_type');
         $srv_id = $this->input->post('service_id');
        
        $res = $this->ApiModel->GetPackagedetail($vhl_id,$srv_id)->result();
        
        $obj = array();
        foreach ($res as $r) 
        {
            if($r->package_type == '1')
            {
                $package_type = "Single Packages";
            }
            if($r->package_type == '2')
            {
                $package_type = "Monthly Packages";
            }
            if($r->package_type == '3')
            {
                $package_type = "Combo Packages";
            }
            
            
        	$package_id = $r->package_id;
        	$package_name = $r->package_name;
        	$package_about = $r->package_about;
        	$package_price = $r->package_price;
        	$package_img = $r->package_image;    
        	$package_service = $r->cat_name;
        	
        	array_push($obj,array("vehicle_id"=>"$package_id","package_service"=>"$package_service","package_type"=>"$package_type","package_name"=>"$package_name","package_about"=>"$package_about","package_price"=>"$package_price","package_img"=>"$package_img"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
    

    public function get_vehicle()
    {
        $user_id = $this->input->post('user_id');
        $this->db->select('tbl_vehicle.*,tbl_model.model_name,tbl_brand.brand_name,tbl_type.type_name');
        $this->db->join('tbl_brand','tbl_brand.brand_id=tbl_vehicle.brand_id','LEFT');
        $this->db->join('tbl_model','tbl_model.model_id=tbl_vehicle.model_id','LEFT');
        $this->db->join('tbl_type','tbl_type.type_id=tbl_vehicle.type_id','LEFT');
        $res = $this->db->get_where('tbl_vehicle',array('user_id'=>$user_id))->result_array();
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $res));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }

    public function Category()
    {
        $cat_id = $_REQUEST['cat_id'];
        $res = $this->ApiModel->GetAllSubcategory($cat_id)->result();
        
        $obj = array();
        foreach ($res as $r) 
        {
        	$subcat_id = $r->subcat_id;
        	$subcat_name = $r->subcat_name;
        	array_push($obj,array("subcat_id"=>"$subcat_id","subcat_name"=>"$subcat_name"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }
    }
    
    
    public function add_vehicle()
    {  
        $subcat_slug = $this->common->slug_generate($this->input->post('vhl_no'));
        if(!empty($_FILES['vehicle_image']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
                $currentTime = date( 'd-m-Y h:i:s A', time () );
                $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['vehicle_image']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('vehicle_image'))
                {
                    $uploadData = $this->upload->data();
                    $vehicle_image = base_url().'upload/'.$uploadData['file_name'];
                    
                }
                else
                {
                    $vehicle_image = base_url().'upload/no_image.jpeg';
                }
            } 
        else{
                  $vehicle_image = base_url().'upload/no_image.jpeg';
        }
         $user_array = array(
            'brand_id'=> $this->input->post('brand_id'),
            'model_id'=> $this->input->post('model_id'),
            'type_id'=> $this->input->post('type_id'),
            'vhl_slug'=>$subcat_slug,
            'vhl_no'=> $this->input->post('vhl_no'),
            'user_id'=> $this->input->post('user_id'),
            'vehicle_image'=>$vehicle_image,
            'vhl_delete'=>'1'
            );
        $res = $this->db->insert('tbl_vehicle',$user_array);
        $obj = array();   
        if($res)
        {
             $obj = array();
            echo json_encode(array("success"=>"1","message" => "Vehicle added successfully.") );
            
        }
        else
        {
           echo json_encode(array("success"=>"0","message" => "Vehicle not Added.") );
            
        }
    }
    
    
     public function edit_vehicle()
    {  
        $subcat_slug = $this->common->slug_generate($this->input->post('vhl_no'));
        $vehicle_data=$this->db->get_where('tbl_vehicle',array('vhl_id'=>$this->input->post('vhl_id')))->row_array();
            if(!empty($_FILES['profile_pic']['name']))
            {
             
                date_default_timezone_set('Asia/Kolkata');
                $currentTime = date( 'd-m-Y h:i:s A', time () );
                $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['profile_pic']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('profile_pic'))
                {
                    $uploadData = $this->upload->data();
                    $vehicle_image = base_url().'upload/'.$uploadData['file_name'];
                    
                }
                else
                {
		     $vehicle_image=$vehicle_data['vehicle_image'];
                }
		
                
    }
    else
                {
                    $vehicle_image=$vehicle_data['vehicle_image'];
                }
         $user_array = array(
            'vehicle_image'=>$vehicle_image,
            'brand_id'=> $this->input->post('brand_id'),
            'model_id'=> $this->input->post('model_id'),
            'type_id'=> $this->input->post('type_id'),
            'vhl_no'=> $this->input->post('vhl_no'),
            'user_id'=> $this->input->post('user_id'),
            );
        $this->db->where('vhl_id',$this->input->post('vhl_id'));
        $res = $this->db->update('tbl_vehicle',$user_array);
	$obj = array();   
        if($res)
        {
             $obj = array();
            echo json_encode(array("success"=>"1","message" => "Vehicle updated successfully.") );
            
        }
        else
        {
           echo json_encode(array("success"=>"0","message" => "Vehicle not updated.") );
            
        }
    } 
    public function add_user_vehicle()
    {  
        
        
        $user_array = array(
            'vhl_detl_vhlid'=> $this->input->post('vhl_id'),
            'vhl_detl_userid'=> $this->input->post('user_id'),
            'vhl_detl_vhlno'=> $this->input->post('vhl_no'),
            'vhl_detl_vhlcolor'=> $this->input->post('vhl_color'),
            'vhl_detl_fueltype'=> $this->input->post('vhl_fuel'),
            'vhl_detl_parkingarea'=> $this->input->post('parkingarea'),
            'vhl_detl_parkinglot'=> $this->input->post('parkinglot'),
          );
            
        $res = $this->ApiModel->add_user_vehicle($user_array);
        $obj = array();   
        if($res == '1')
        {
             $obj = array();
            echo json_encode(array("success"=>"1","message" => "Your Vehicle Added.") );
            
        }
        else
        {
           echo json_encode(array("success"=>"0","message" => "Your Vehicle not Added, Vehicle No Already Exist.") );
            
        }
    }
    
    public function get_user_data()
    {
         $userid = $this->input->post('user_id');
         $res = $this->db->get_where('tbl_user',array('user_id'=>$userid))->result_array();
        
        
       if($res)
        {
            echo json_encode(array("success"=>"1","data" => $res));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.") );
        }

    }
    
    
     public function get_user_detail()
    {
       
         $vhl_detl_userid = $this->input->post('user_id');
        
        
        $res = $this->ApiModel->Getvehiclebyuser($vhl_detl_userid)->result();
        
        $obj = array();
        foreach ($res as $r) 
        {
            if($r->vhl_detl_parkingarea == '1')
            {
                $vhl_area = "Basement";
            }
            if($r->vhl_detl_parkingarea == '2')
            {
                $vhl_area = "Basement 1";
            }
            if($r->vhl_detl_parkingarea == '3')
            {
                $vhl_area = "Ground Floor";
            }
            if($r->vhl_detl_parkingarea == '4')
            {
                $vhl_area = "First Floor";
            }
            
            $vah_detail_id = $r->vhl_detl_id;
        	$vah_id = $r->vhl_detl_vhlid;
        	$vah_type = $r->vhl_type_name;
        	$vah_type_id = $r->vhl_type_id;
        	$vhl_detl_vhlno = $r->vhl_detl_vhlno;
        	$vhl_detl_vhlcolor = $r->vhl_detl_vhlcolor;
        	$vhl_detl_fueltype = $r->vhl_detl_fueltype;
        	$vhl_detl_parkinglot = $r->vhl_detl_parkinglot;       
          
        	
        	array_push($obj,array("vahicle_detail_id"=>"$vah_detail_id","vehicle_id"=>"$vah_id","Vehicle_no"=>"$vhl_detl_vhlno","Vehicle_color"=>"$vhl_detl_vhlcolor","Vehicle_fueltype"=>"$vhl_detl_fueltype",
        	                      "parking_area"=>"$vhl_area","parking_plot"=>"$vhl_detl_parkinglot","vehicle_type"=>"$vah_type","vah_type_id"=>"$vah_type_id"));
        }
          $address = $this->ApiModel->Get_Address_ByUser($vhl_detl_userid)->result_array();
       
        
        if($res == true && $res==true)
        {
            echo json_encode(array("success"=>"1","data" => $obj,"success_address"=>"1","address"=>$address));
        }
       else  if($address == true && $res!=true)
        {
            echo json_encode(array("success"=>"0","data" => $obj,"success_address"=>"1","address"=>$address));
        }
       else if($address != true && $res==true)
        {
            echo json_encode(array("success"=>"1","data" => $obj,"success_address"=>"0","address"=>$address));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "No Record found.","success_address"=>"0") );
        }
    }
    
    public function signup()
    {  
           if(!empty($_FILES['profile_pic']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
                $currentTime = date( 'd-m-Y h:i:s A', time () );
                $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['profile_pic']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('profile_pic'))
                {
                       $uploadedFile =$date_string.$_FILES['profile_pic']['name'];
        $sourceProperties = getimagesize($uploadedFile);
        $newFileName = 'User_'.time();
        $dirPath =  base_url().'upload/';
        $ext = pathinfo($_FILES['profile_pic']['name'], PATHINFO_EXTENSION);
        $imageType = $sourceProperties[2];


        switch ($imageType) {


            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = $this->imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1]);
                 imagepng($tmp,$dirPath. $newFileName. "_thump.". $ext);
                $profile_pic= $dirPath. $newFileName. "_thump.". $ext;
                
                break;           

            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = $this->imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1]);
                 imagejpeg($tmp,$dirPath. $newFileName. "_thump.". $ext);
                $profile_pic= $dirPath. $newFileName. "_thump.". $ext;
                
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = $this->imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1]);
                imagegif($tmp,$dirPath. $newFileName. "_thump.". $ext);
                $profile_pic= $dirPath. $newFileName. "_thump.". $ext;
                
                break;

            default:
                echo "Invalid Image type.";
                exit;
                break;
        }


                    
                    $uploadData = $this->upload->data();
                    $profile_pic = base_url().'upload/'.$uploadData['file_name'];
                    
                }
                else
                {
                    
                    $profile_pic =  base_url().'upload/no_image.jpeg';
                }
            } 
            else{
                    $profile_pic =  base_url().'upload/no_image.jpeg';
                
            }
     
       // echo "Image Resize Successfully.";



function imageResize($imageSrc,$imageWidth,$imageHeight) {

    $newImageWidth =1500;
    $newImageHeight =1200;

    $newImageLayer=imagecreatetruecolor($newImageWidth,$newImageHeight);
    imagecopyresampled($newImageLayer,$imageSrc,0,0,0,0,$newImageWidth,$newImageHeight,$imageWidth,$imageHeight);

    return $newImageLayer;
}
            
            
        $user_array = array(
            'user_fname'=>$this->input->post('first_name'),
            'user_lname'=>$this->input->post('last_name'),
            'user_email'=>$this->input->post('email'),
            'user_phno'=>$this->input->post('phone'),
            'user_creattime'=>date('Y-m-d'),
            'profile_pic'=>$profile_pic,
            'country_id'=>$this->input->post('country_code'),
            'user_delete'=>'0',
            );
            
        $query = $this->db->get_where('tbl_user',array('user_phno'=>$this->input->post('phone'),'user_delete'=>'0'));
        if($query->num_rows() == '0')
        {
            $ins = $this->db->insert('tbl_user',$user_array);
        if(!$ins)
        {
            echo json_encode(array("success"=>"0","message" => "User Account not Created. Mobile Number already Exist.") );
        }
        else
        {
            echo json_encode(array("success"=>"1","data" => $user_array));
        }
        }
        else{
              echo json_encode(array("success"=>"0","message" => "User Account not Created. Mobile Number already Exist.") );
          
        }
    }
    
    public function get_profile()
    {
        $user_id=$this->input->post('user_id');  
        $token=$this->input->post('token');  
        $user_data=$this->db->get_where('tbl_user',array('user_id'=>$user_id))->row_array();
        $obj = array();
        if(!$user_data)
        {
         
         echo json_encode(array("success"=>"0","message" => "Oops..!Something  went wrong."));
            
        }
        else
        {   
            $user_array=array('device_token'=>$token);
            $this->db->where('user_id',$user_id);
            $this->db->update('tbl_user',$user_array);
            array_push($obj,$user_data);
            echo json_encode(array("success"=>"1","data" => $obj) );
        }        
    }
    public function edit_profile()
    {
          $user_id=$this->input->post('user_id');  
          $user_data=$this->db->get_where('tbl_user',array('user_id'=>$user_id))->row_array();
          $profile_pic=$user_data['profile_pic'];
        
           if(!empty($_FILES['profile_pic']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
                $currentTime = date( 'd-m-Y h:i:s A', time () );
                $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['profile_pic']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('profile_pic'))
                {
                    $uploadData = $this->upload->data();
                    $profile_pic= base_url().'upload/'.$uploadData['file_name'];
                    
                }
            }

            
        $data = array(
            'user_fname'=>$this->input->post('first_name'),
            'user_lname'=>$this->input->post('last_name'),
           // 'user_email'=>$this->input->post('email'),
            'profile_pic'=>$profile_pic,
           // 'country_id'=>$this->input->post('country_code'),
               );
         $this->db->where('user_id',$this->input->post('user_id'));
         $query = $this->db->update('tbl_user',$data);
        $obj = array();
        if(!$query)
        {
         
         echo json_encode(array("success"=>"0","message" => "something went wrong. Profile not Updated."));
            
        }
        else
        {    $user_id = $_POST['user_id'];
            $user_data=$this->db->get_where('tbl_user',array('user_id'=>$user_id))->row_array();
       
            array_push($obj,$user_data);
            echo json_encode(array("success"=>"1","data" => $obj,"message" => " Profile Update Successfully.") );
        }        
    }
   
   
    public function booking_data()
    { 
        extract($_POST);
        $obj=array();
         $query=$this->db->select('tbl_rating.rating_detail,tbl_rating.rating,tbl_booking.booking_status,tbl_booking.booking_id,tbl_vehicle.vhl_no,booking_lat,booking_long,tbl_booking.address,service_date,service_time,CONCAT(brand_name," ",model_name) as vehicle_name,driver_lat,driver_long');
         $this->db->join('tbl_vehicle','tbl_vehicle.vhl_id=tbl_booking.vehicle_id','LEFT');
         $this->db->join('tbl_brand','tbl_vehicle.brand_id=tbl_brand.brand_id','LEFT');
         $this->db->join('tbl_model','tbl_vehicle.model_id=tbl_model.model_id','LEFT');
         $this->db->join('tbl_user_address','tbl_user_address.addr_id=tbl_booking.address_id','LEFT');
         $this->db->join('tbl_rating','tbl_rating.order_id=tbl_booking.booking_id','LEFT');
         $query=$this->db->get_where('tbl_booking',array('tbl_booking.booking_id'=>$this->input->post('booking_id')))->result_array();
        
         foreach($query as $row)
        {
            $booking_id=$row['booking_id'];
            $this->db->select('cat_name as service_name,service_amount');
            $this->db->join('tbl_category','tbl_category.cat_id=tbl_booking_service.service_id','LEFT');
            $row['service_data']=$this->db->get_where('tbl_booking_service',array('booking_id'=>$booking_id))->result_array();
            $arrival=$this->get_arrival_time($row['booking_lat'],$row['booking_long'],$row['driver_lat'],$row['driver_long']);
            $row['arrival_time']=$arrival;
           
            array_push($obj,$row);
        }   

        if(!$query)
        {
         
         echo json_encode(array("success"=>"0","message" => "something went wrong."));
            
        }
        else
        {    echo json_encode(array("success"=>"1","data" => $obj) );
        }        
    }
    
     public function driver_booking_data()
    { 
        extract($_POST);
        $obj=array();
         $query=$this->db->select('tbl_rating.rating_detail,tbl_rating.rating,tbl_booking.booking_status,tbl_user.user_fname,tbl_user.user_lname,tbl_user.country_id,tbl_user.user_phno,tbl_user.profile_pic,booking_lat,booking_long,tbl_booking.booking_id,tbl_vehicle.vhl_no,tbl_booking.address,service_date,service_time,CONCAT(brand_name," ",model_name) as vehicle_name,driver_lat,driver_long');
         $this->db->join('tbl_vehicle','tbl_vehicle.vhl_id=tbl_booking.vehicle_id','LEFT');
         $this->db->join('tbl_brand','tbl_vehicle.brand_id=tbl_brand.brand_id','LEFT');
         $this->db->join('tbl_model','tbl_vehicle.model_id=tbl_model.model_id','LEFT');
         $this->db->join('tbl_user','tbl_user.user_id=tbl_booking.user_id','LEFT');
         $this->db->join('tbl_user_address','tbl_user_address.addr_id=tbl_booking.address_id','LEFT');
         $this->db->join('tbl_rating','tbl_rating.order_id=tbl_booking.booking_id','LEFT');
        
         $query=$this->db->get_where('tbl_booking',array('tbl_booking.booking_id'=>$this->input->post('booking_id')))->result_array();
        
         foreach($query as $row)
        {
             $booking_id=$row['booking_id'];
             $this->db->select('cat_name as service_name,service_amount');
             $this->db->join('tbl_category','tbl_category.cat_id=tbl_booking_service.service_id','LEFT');
             $row['service_data']=$this->db->get_where('tbl_booking_service',array('booking_id'=>$booking_id))->result_array();
             $arrival=$this->get_arrival_time($row['booking_lat'],$row['booking_long'],$row['driver_lat'],$row['driver_long']);
             $row['arrival_time']=$arrival;
           
            array_push($obj,$row);
        }   

        if(!$query)
        {
         
         echo json_encode(array("success"=>"0","message" => "something went wrong."));
            
        }
        else
        {    echo json_encode(array("success"=>"1","data" => $obj) );
        }        
    }
    
    
     public function payment_method()
    { 
        extract($_POST);
           
        $query=$this->db->get('tbl_payment_mode')->result_array();
            

        if(!$query)
        {
         
         echo json_encode(array("success"=>"0","message" => "something went wrong."));
            
        }
        else
        {    echo json_encode(array("success"=>"1","data" => $query) );
        }        
    }
    public function driver_rating()
    { 
        extract($_POST);
         $query=$this->db->get_where('tbl_booking',array('tbl_booking.user_id'=>$this->input->post('user_id'),'booking_status!='=>'Pending'))->result_array();
       
        
    }
    public function my_booking()
    { 
        extract($_POST);
        $obj=array();
        $user_id = $this->input->post('user_id');
        $type    = $this->input->post('type');
        $this->db->query('SET SESSION sql_mode = ""');
        $query=$this->db->select('tbl_booking.booking_status,tbl_rating.rating_detail,tbl_rating.rating,tbl_booking.booking_id,tbl_vehicle.vhl_no,tbl_booking.address,service_date,service_time,CONCAT(brand_name," ",model_name) as vehicle_name');
        $this->db->join('tbl_vehicle','tbl_vehicle.vhl_id=tbl_booking.vehicle_id','LEFT');
        $this->db->join('tbl_brand','tbl_vehicle.brand_id=tbl_brand.brand_id','LEFT');
        $this->db->join('tbl_rating','tbl_rating.order_id=tbl_booking.booking_id','LEFT');
        $this->db->join('tbl_model','tbl_vehicle.model_id=tbl_model.model_id','LEFT');
        $this->db->group_by('tbl_booking.booking_id');
        $this->db->order_by('tbl_booking.booking_id','desc');
        
        if($type=='0')
        {
        $this->db->group_start();
        $this->db->where('booking_status!=', 'Delivered');
        $this->db->or_where('booking_status!=', "Completed");
        $this->db->group_end();
        
        $query=$this->db->get_where('tbl_booking',array('tbl_booking.user_id'=>$this->input->post('user_id')))->result_array();
        }
        else{
            $this->db->group_start();
        $this->db->where('booking_status', 'Delivered');
        $this->db->or_where('booking_status', "Completed");
        $this->db->group_end();
        
        $query=$this->db->get_where('tbl_booking',array('tbl_booking.user_id'=>$this->input->post('user_id')))->result_array();
        }
        foreach($query as $row)
        {
            $booking_id=$row['booking_id'];
            $this->db->select('cat_name as service_name,service_amount,cat_img');
            $this->db->join('tbl_category','tbl_category.cat_id=tbl_booking_service.service_id','LEFT');
            $row['service_data']=$this->db->get_where('tbl_booking_service',array('booking_id'=>$booking_id))->result_array();
               
            
            array_push($obj,$row);
        }
        if(!$query)
        {
         
         echo json_encode(array("success"=>"0","message" => "something went wrong."));
            
        }
        else
        {    echo json_encode(array("success"=>"1","data" => $obj) );
        }        
    }
    public function my_booking_agent()
    { 
        extract($_POST);
        $obj=array();
        $token =$this->input->post('token');
        $user_id =$this->input->post('user_id');
        $type =$this->input->post('type');
            $user_array=array('device_token'=>$token);
            $this->db->where('user_id',$user_id);
            $this->db->update('tbl_washer',$user_array);
            $this->db->query('SET SESSION sql_mode = ""');
           
        $query=$this->db->select('tbl_booking.booking_status,tbl_rating.rating_detail,tbl_rating.rating,tbl_user.user_fname,tbl_user.user_lname,tbl_user.profile_pic,tbl_booking.booking_id,tbl_vehicle.vhl_no,tbl_booking.address,service_date,service_time,CONCAT(brand_name," ",model_name) as vehicle_name ');
       
        $this->db->join('tbl_vehicle','tbl_vehicle.vhl_id=tbl_booking.vehicle_id','LEFT');
        $this->db->join('tbl_brand','tbl_vehicle.brand_id=tbl_brand.brand_id','LEFT');
        $this->db->join('tbl_rating','tbl_rating.order_id=tbl_booking.booking_id','LEFT');
        $this->db->join('tbl_model','tbl_vehicle.model_id=tbl_model.model_id','LEFT');
        $this->db->join('tbl_user','tbl_booking.user_id=tbl_user.user_id','LEFT');
        $this->db->group_by('tbl_booking.booking_id');
         $this->db->order_by('booking_id','desc');
      
        if($type=='0')
        {
        $query=$this->db->get_where('tbl_booking',array('tbl_booking.washer_id'=>$this->input->post('user_id'),'booking_status!='=>'Completed'))->result_array();
        }
        else{
        $query=$this->db->get_where('tbl_booking',array('tbl_booking.washer_id'=>$this->input->post('user_id'),'booking_status='=>'Completed'))->result_array();
            
        }
        foreach($query as $row)
        {
            $booking_id=$row['booking_id'];
            $this->db->select('cat_name as service_name,service_amount,cat_img');
            $this->db->join('tbl_category','tbl_category.cat_id=tbl_booking_service.service_id','LEFT');
            $row['service_data']=$this->db->get_where('tbl_booking_service',array('booking_id'=>$booking_id))->result_array();
               
            
            array_push($obj,$row);
        }
        if(!$query)
        {
         
         echo json_encode(array("success"=>"0","message" => "something went wrong."));
            
        }
        else
        {    echo json_encode(array("success"=>"1","data" => $obj) );
        }        
    }
  
    public function driver_review()
    {   
        $query[]=$this->db->get_where('tbl_rating',array('order_id'=>$this->input->post('order_id')))->row_array();
        if(!$query)
        {
         
         echo json_encode(array("success"=>"0","message" => "something went wrong."));
            
        }
        else
        {    echo json_encode(array("success"=>"1","data" => $query) );
        }    
    }
    public function booking()
    {  
        $date = $this->input->post('service_date');
        $date = date('Y-m-d',strtotime($date));
        $user_array = array(
            'user_id'=>$this->input->post('user_id'),
            'vehicle_id'=>$this->input->post('vehicle_id'),
            'address_id'=>$this->input->post('address_id'),
            'address'=>$this->input->post('address'),
            //'subpackage_id'=>$this->input->post('service_id'),
            'service_date'=>$date,
            'service_time'=>$this->input->post('service_time'),
           // 'amount'=>$this->input->post('amount'),
           // 'transaction_id'=>$this->input->post('transaction_id'),
            'total_amount'=>$this->input->post('total_amount'),
            'payment_mode'=>$this->input->post('payment_mode'),
             'booking_lat'=>$this->input->post('addr_lat'),
            'booking_long'=>$this->input->post('addr_long'),
           
            //'cat_id'=>$this->input->post('service_id'),
            );
            
        $res = $this->db->insert('tbl_booking',$user_array);
        $booking_id=$this->db->insert_id();
        $service_data=json_decode($this->input->post('service_data'),true);
        foreach($service_data as $key=>$value)
        {
            $service_array=array('booking_id'=>$booking_id,
            'service_id'=>$service_data[$key]['service_id'],
            'service_amount'=>$service_data[$key]['amount']);
            $this->db->insert('tbl_booking_service',$service_array);
        }
        
        $obj = array();   
        
        if($res)
        {
                $user_data=$this->db->get_where('tbl_user',array('user_id'=>$this->input->post('user_id')))->row_array();
                $message='Thank you. Your booking successful for car wash.';
                $msg = urlencode($message);
                $data = array(
                    'title'=>'Booking Successful',
                    'sound' => "default",
                    'msg'=>$msg,
                    'data'=>$datapayload,
                    'body'=>$message,
                    'color' => "#79bc64"
                );
                $fields = array(
                    'to'=>$user_data["device_token"],
                    'notification'=>$data,
                    'data'=>$datapayload,
                    "priority" => "high",
                );
                $headers = array(
                    'Authorization: key='.FCM_KEY,
                    'Content-Type: application/json'
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                curl_close( $ch );
              
            echo json_encode(array("success"=>"0","message" => "Booking requested successfully.") );
            
        }
        else
        {
              
            echo json_encode(array("success"=>"1","message" => "Ooops..! Please try again."));
        }
    }
    
    
    public function Add_Address()
    {  
        $address_array = array(
            'addr_userid'=>$this->input->post('addr_userid'),
            'addr_address'=>$this->input->post('addr_address'),
            'addr_address_1'=>$this->input->post('addr_address_1'),
            'addr_city'=>$this->input->post('addr_city'),
           // 'addr_zip'=>$this->input->post('addr_zip'),
            'addr_type'=>$this->input->post('addr_type'),
            'addr_delete'=>'0',
            'addr_lat'=>$this->input->post('addr_lat'),
            'addr_long'=>$this->input->post('addr_long'),
            
            );
            
        $userid = $this->ApiModel->Add_Address($address_array);
        if($userid > 0)
        {
           // $seladdress = $this->ApiModel->Get_Address_ByUser($userid);
           echo json_encode(array("success"=>"1","message" => "Address Added Successfully."));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "something went wrong. Address not added.") );
        }
    }
    
    public function delete_Address()
    {
        $addr_id = $this->input->post('addr_id');
        $this->db->where('addr_id',$addr_id);
        $userid = $this->db->delete('tbl_user_address');
        if($userid)
        {
           // $seladdress = $this->ApiModel->Get_Address_ByUser($userid);
           echo json_encode(array("success"=>"1","message" => "Address Deleted Successfully."));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "something went wrong. Address not Deleted.") );
        }
    }
    
     public function delete_vehicle()
    {
        $vhl_id = $this->input->post('vhl_id');
        $this->db->where('vhl_id',$vhl_id);
        $userid = $this->db->delete('tbl_vehicle');
        if($userid)
        {
           echo json_encode(array("success"=>"1","message" => "Vehicle Deleted Successfully."));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "something went wrong. Vehicle not Deleted.") );
        }
    }
    
    
    public function edit_Address()
    {  
        $address_array = array(
            'addr_id'=>$this->input->post('addr_id'),
            'addr_userid'=>$this->input->post('addr_userid'),
            'addr_address'=>$this->input->post('addr_address'),
            'addr_address_1'=>$this->input->post('addr_address_1'),
            'addr_city'=>$this->input->post('addr_city'),
            'addr_zip'=>$this->input->post('addr_zip'),
            'addr_type'=>$this->input->post('addr_type'),
          );
            
        $userid = $this->ApiModel->edit_Address($address_array);
        if($userid > 0)
        {
           // $seladdress = $this->ApiModel->Get_Address_ByUser($userid);
           echo json_encode(array("success"=>"1","message" => "Address Edited Successfully."));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "something went wrong. Address not edited.") );
        }
    }
    

    public function Get_Address_ByUser() {
        $user_id = $this->input->post('user_id');
        $res = $this->db->get_where('tbl_user_address',array('addr_userid'=>$user_id))->result_array();
 
        // print_r($res);

        $obj = array();
        
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $res));         
            
        }
        else
        {            
        	echo json_encode(array("success"=>"2","message" => "No Record found."));
        	
        }   
    }
    
     public function Get_past_booking() {
        $user_id = $this->input->post('user_id');
        $today_date = date('Y-m-d');
               $this->db->join('tbl_subservice','tbl_subservice.sub_servid =tbl_booking.subpackage_id ','LEFT');
               $this->db->join('tbl_vhl_detail','tbl_vhl_detail.vhl_detl_id = tbl_booking.vehicle_id','LEFT');
               $this->db->join('tbl_vehicle','tbl_vehicle.vhl_id = tbl_vhl_detail.vhl_detl_vhlid','LEFT');
        $res = $this->db->get_where('tbl_booking',array('tbl_booking.user_id'=>$user_id,'tbl_booking.service_date <'=>$today_date))->result();

        $obj = array();
        foreach ($res as $r) 
        {
        
            $vhl_name = $r->vhl_name;
        	$vhl_no = $r->vhl_detl_vhlno;
        	$vhl_color = $r->vhl_detl_vhlcolor;
            $vhl_fuel = $r->vhl_detl_fueltype;
            $package = $r->sub_name;
            $vhl_servtime = $r->service_time;
            $vhl_servdate = $r->service_date;
            $total_amount = $r->total_amount;
        
        	
        	array_push($obj,array("Vehicle_name"=>"$vhl_name","Vehicle_no"=>"$vhl_no","Vehicle_color"=>"$vhl_color","Fuel_type"=>"$vhl_fuel","package"=>"$package","Service_date"=>"$vhl_servdate","Service_time"=>"$vhl_servtime","amount"=>"$total_amount"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));         
            
        }
        else
        {            
        	echo json_encode(array("success"=>"2","message" => "No Record found."));
        	
        }   
    }
    
     public function Get_current_booking() {
        $user_id = $this->input->post('user_id');
        $today_date = date('Y-m-d');
               $this->db->join('tbl_subservice','tbl_subservice.sub_servid =tbl_booking.subpackage_id ','LEFT');
               $this->db->join('tbl_vhl_detail','tbl_vhl_detail.vhl_detl_id = tbl_booking.vehicle_id','LEFT');
               $this->db->join('tbl_vehicle','tbl_vehicle.vhl_id = tbl_vhl_detail.vhl_detl_vhlid','LEFT');
        $res = $this->db->get_where('tbl_booking',array('tbl_booking.user_id'=>$user_id,'tbl_booking.service_date >='=>$today_date))->result();

        $obj = array();
        foreach ($res as $r) 
        {
        
            $vhl_name = $r->vhl_name;
        	$vhl_no = $r->vhl_detl_vhlno;
        	$vhl_color = $r->vhl_detl_vhlcolor;
            $vhl_fuel = $r->vhl_detl_fueltype;
            $package = $r->sub_name;
            $vhl_servtime = $r->service_time;
            $vhl_servdate = $r->service_date;
            $total_amount = $r->total_amount;
        
        	
        	array_push($obj,array("Vehicle_name"=>"$vhl_name","Vehicle_no"=>"$vhl_no","Vehicle_color"=>"$vhl_color","Fuel_type"=>"$vhl_fuel","package"=>"$package","Service_date"=>"$vhl_servdate","Service_time"=>"$vhl_servtime","amount"=>"$total_amount"));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));         
            
        }
        else
        {            
        	echo json_encode(array("success"=>"2","message" => "No Record found."));
        	
        }   
    }
    
    public function get_offer_detail()
    {
        $today = date('Y-m-d');
        $res = $this->db->get_where('tbl_offer',array('offer_st_date <='=>$today,'offer_end_date >='=>$today))->result();  
        $obj = array();
        foreach ($res as $r) 
        {
        
            $offer_id= $r->offer_name;
            $offer_name= $r->offer_name;
        	$offer_code = $r->offer_code;
        	$offer_val = $r->offer_val;
           
            if($r->offer_type == '1')
            {
                 $off_type = "% Wise Discount";
            }
            else
            {
                 $off_type = "Fix Amount Wise Discount";
            }
            
            if($r->offer_allowed == '1')
            {
                 $off_allow = "Single User";
            }
            else
            {
                 $off_allow = "Multiple Users";
            }
        
        	
        	array_push($obj,array("offer_id"=>$offer_id,"offer_name"=>"$offer_name","offer_code"=>"$offer_code","offer_amount"=>"$offer_val","Type"=>"$off_type","allow_user"=>$off_allow));
        }
        
        if($res == true)
        {
            echo json_encode(array("success"=>"1","data" => $obj));         
            
        }
        else
        {            
        	echo json_encode(array("success"=>"2","message" => "No Record found."));
        	
        }   
    }
   
    public function get_coupon()
    {
        $coupon=$this->input->post('coupon_code');
        $res = $this->db->get_where('tbl_coupon',array('coupon_code '=>$coupon));
        $row=$res->row_array();  
        $count=$res->num_rows();  
        if($count>0)
        {
            echo json_encode(array("success"=>"1","data" => $row));         
            
        }
        else
        {            
        	echo json_encode(array("success"=>"2","message" => "No Record found."));
        	
        }   
    }
    public function delete_card()
    {
        $card_id = $this->input->post('card_id');
        $this->db->where('card_id',$card_id);
        $userid = $this->db->delete('tbl_card');
        if($userid)
        {
           echo json_encode(array("success"=>"1","message" => "Card Deleted Successfully."));
        }
        else
        {
            echo json_encode(array("success"=>"0","message" => "something went wrong. Card not Deleted.") );
        }
    }
    public function check_promocode()
    {
        $promocode = $this->input->post('promocode');
        $offerid = $this->input->post('offer_id');
        
        $today = date('Y-m-d');
        $res = $this->db->get_where('tbl_offer',array('offer_id'=>$offerid,'offer_code'=>$promocode,'offer_st_date <='=>$today,'offer_end_date >='=>$today));  
        
        if($res->num_rows() > '0')
        {
            $res1 = $res->result();
            $obj = array();
            foreach ($res1 as $r) 
            {
        
           
        	$offer_code = $r->offer_code;
        	$offer_val = $r->offer_val;
           
            if($r->offer_type == '1')
            {
                 $off_type = "% Wise Discount";
            }
            else
            {
                 $off_type = "Fix Amount Wise Discount";
            }
            
            if($r->offer_allowed == '1')
            {
                 $off_allow = "Single User";
            }
            else
            {
                 $off_allow = "Multiple Users";
            }
        
        	
        	array_push($obj,array("offer_code"=>"$offer_code","offer_amount"=>"$offer_val","Type"=>"$off_type","allow_user"=>$off_allow));
        }
            
            
            
            
            echo json_encode(array("success"=>"1","message" => "valid Promo Code.","data" => $obj));        
            
        }
        else
        {            
        	echo json_encode(array("success"=>"2","message" => "No Valid Promo Code."));
        	
        }  
        
    }
    
    public function arrival_time()
    {
        $row=$this->db->get_where('tbl_booking',array('booking_id'=>'54'))->row_array();
         $arrival_time=$this->get_arrival_time($row['booking_lat'],$row['booking_long'],$row['driver_lat'],$row['driver_long']);
           echo $arrival_time;
    } 
    public function get_arrival_time($from_lat,$from_long,$to_lat,$to_long)
    {
        
        $origin=$from_lat.','.$from_long;
        $destination=$to_lat.','.$to_long;

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&key=AIzaSyDQ69wZR1GPEeLAxyu-vkSSo_dzpZTOV2c";
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        return $response_a['rows'][0]['elements'][0]['duration']['text']; 
    }
}
?>
