
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model('UserDetailmodel');
	}
	
	public function index()
	{
        if ($this->session->userdata('is_admin_login')) {

            $data['user_data'] = $this->UserDetailmodel->get_user_data();
            $this->load->view('user/user_detail',$data);
        } 
        else
        {
            $messge = array('message_del' => 'Login Please...','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
            redirect('Home');
       	}
    }

    public function Add()
    {
        $this->load->view('user/add_detail');
    }

    public function Add_c()
    {
            if(!empty($_FILES['profile_pic']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
               $currentTime = date( 'd-m-Y h:i:s A', time () );
               $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['profile_pic']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('profile_pic'))
                {
                    $uploadData = $this->upload->data();
                    $profile_pic = base_url().'upload/'.$uploadData['file_name'];
                    
                }
                else
                {
                    $profile_pic = '';
                }
            }
            else{
                $profile_pic='-';
            }
          $query = $this->db->get_where('tbl_user',array('user_phno'=>$this->input->post('user_phoneno'),'user_delete'=>'0'));
        if($query->num_rows() == '0')
        {
          
        
        $data = array(
            'user_fname' => $this->input->post('user_fname'),
            'user_lname' => $this->input->post('user_lname'),
           // 'user_password'=> md5($this->input->post('user_password')),
            'user_email' => $this->input->post('user_email'),
            'user_phno' => $this->input->post('user_phoneno'),
            'country_id' => $this->input->post('country_id'),
            'profile_pic' => $profile_pic,
            'user_creattime' => date('Y-m-d'),
            'user_password' => md5($this->input->post('user_password')),
           
            'user_delete' => '0',
            );
       $r= $this->db->insert('tbl_user',$data);
       if($r)
       {
            $messge = array('message_del' => 'User Added Successfully!','message_type' => 'success');
          $this->session->set_flashdata('item', $messge);
    
       }
       else{
            $messge = array('message_del' => 'User Not Added Successfully!','message_type' => 'error');
          $this->session->set_flashdata('item', $messge);
         
       }
        }
        else{
            $messge = array('message_del' => 'Mobile No already exist!','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
     
        }
     redirect('User');
 
    }
   
    public function Edit($user_id)
    {
        $data['user_data'] = $this->UserDetailmodel->selected_user_data($user_id);
        $this->load->view('user/edit_detail',$data);
    }
    public function View($user_id)
    {
        $data['user_data'] = $this->UserDetailmodel->selected_user_data($user_id);
        $this->load->view('user/view_data',$data);
    }
    
    public function Edit_c()
    {
          $user_data = $this->UserDetailmodel->selected_user_data($user_id);
      
             if(!empty($_FILES['profile_pic']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
               $currentTime = date( 'd-m-Y h:i:s A', time () );
               $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['profile_pic']['name'];
                
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('profile_pic'))
                {
                    $uploadData = $this->upload->data();
                    $profile_pic = base_url().'upload/'.$uploadData['file_name'];
                    
                }
                else
                {
                    $profile_pic = $user_data['profile_pic'];
                }
            }
            else
                {
                    $profile_pic = $user_data['profile_pic'];
                }
        $user_id = $this->input->post('user_id');
        $data = array(
            'user_fname' => $this->input->post('user_fname'),
            'user_lname' => $this->input->post('user_lname'),
            'country_id' => $this->input->post('country_id'),
            'profile_pic' => $profile_pic,
            'user_email' => $this->input->post('user_email'),
            'user_phno' => $this->input->post('user_phoneno'),
            'user_status' => $this->input->post('status'),
            
            );
        $data['user_data'] = $this->UserDetailmodel->Edit_c($user_id,$data);
          $messge = array('message_del' => 'User Detail Updated Successfully!','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
        redirect('User');
    }

    public function Delete($user_id)
    {
        $result = $this->UserDetailmodel->Delete($user_id);
        $messge = array('message_del' => 'User Detail Deleted Successfully!','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
        redirect('User');
    }
}
