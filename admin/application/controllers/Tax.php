<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tax extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		
	}
	
	public function index()
	{
     
		if ($this->session->userdata('is_admin_login')) {
           
            $data['tax_data'] = $this->db->get('tbl_tax')->row_array();
            $this->load->view('tax',$data);
        } 
        else
        {
            $messge = array('message_del' => 'Login Please...','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
               redirect('Home');
       	}
    }
    
    
    public function Edit_c()
	{
	      $data = array(
            'tax_rate' => $this->input->post('tax_rate'),
            'tax_discount' => $this->input->post('gst_disc')
            );
            $this->db->where('tax_id','1');
        $this->db->update('tbl_tax',$data);
        redirect('Tax');
    } 
    
    
}
?>