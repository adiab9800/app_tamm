<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_center extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		
	    $this->load->library('upload');
	}
	
	public function index()
	{
         $data['service_center_data'] = $this->db->get('tbl_service_center')->result_array();
         $this->load->view('service_center/view',$data);
    }
    public function Add()
	{
	     $this->load->view('service_center/add',$data);
    } 
    
    public function Add_c()
	{  
       
       
          $array = array(
            'service_center_name' => $this->input->post('service_center_name'),
            'service_center_lat' => $this->input->post('service_center_lat'),
            'service_center_long' => $this->input->post('service_center_long'),
            'service_center_address' => $this->input->post('service_center_address'),
            'service_center_about' => $this->input->post('service_center_about'),
            );
            $result=$this->db->insert('tbl_service_center',$array);
             if($result)
            {
                   $messge = array('message_del' => 'Data inserted successfully.','message_type' => 'success');
                   $this->session->set_flashdata('item', $messge);
            }
            else{
                   $messge = array('message_del' => 'data not inserted.Please try again.','message_type' => 'error');
                   $this->session->set_flashdata('item', $messge);
             
            }
       
      
      
      
        redirect('service_center');
    } 
    
     public function Edit($id)
	{
    	 $this->db->where('service_center_id',$id);
	 	$data['service_center_data'] = $this->db->get('tbl_service_center')->row_array();
       
           $this->load->view('service_center/edit',$data);
    } 
    
    public function View($id)
	{
    	 $this->db->where('service_center_id',$id);
	 	$data['service_center_data'] = $this->db->get('tbl_service_center')->row_array();
       
           $this->load->view('service_center/view_data',$data);
    } 
    
    public function Edit_c()
	{
	    $service_center_id= $this->input->post('service_center_id');
        
         
         
          $array = array(
            'service_center_name' => $this->input->post('service_center_name'),
            'service_center_lat' => $this->input->post('service_center_lat'),
            'service_center_long' => $this->input->post('service_center_long'),
            'service_center_address' => $this->input->post('service_center_address'),
            'service_center_about' => $this->input->post('service_center_about'),
            );
            $this->db->where('service_center_id',$service_center_id);
            $result=$this->db->update('tbl_service_center',$array);
            if($result)
            {
                   $messge = array('message_del' => 'Data updated successfully.','message_type' => 'success');
                   $this->session->set_flashdata('item', $messge);
            }
            else{
                   $messge = array('message_del' => 'data not updated.Please try again.','message_type' => 'error');
                   $this->session->set_flashdata('item', $messge);
             
            }
       
    redirect('service_center');
    } 
    
    public function Delete($id)
    {   
        
        $this->db->where('service_center_id',$id);
        $result=$this->db->delete('tbl_service_center');
        if($result)
        {
               $messge = array('message_del' => 'Data deleted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not deleted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('service_center');
      
    
    }
}
?>