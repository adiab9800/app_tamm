<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subservice extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model('Subservicemodel');
	}
	
	public function index()
	{
        $data['subserv_data'] = $this->Subservicemodel->get_subserv_data();
        $this->load->view('subservice/view',$data);
        
    }
    public function Add()
	{
        $data['pack_data'] = $this->Subservicemodel->get_packing_data();
        $this->load->view('subservice/add',$data);
    } 
    
    public function Add_c()
	{
       if($this->input->post('sub_ca') == '8')
       {
         
        $data_ca = array(
            'sub_packid'=> $this->input->post('sub_packid'),
            'sub_name'=> $this->input->post('sub_name'),
            'sub_mainamount'=> $this->input->post('sub_mainamount'),
            'sub_discamount'=> $this->input->post('sub_discamount'),
            'sub_include'=> $this->input->post('sub_include'),
           
            'sub_detail'=>$this->input->post('sub_detail'),
            'sub_delete '=>'1',
            );
          
       $this->Subservicemodel->Add_loc($data_ca);
       }
       
        redirect('Subservice');
    } 
    
     public function Edit($serv_id)
	{
       
	    $data['serv_data'] = $this->Subservicemodel->selected_service_data($serv_id);
        $data['pack_data'] = $this->Subservicemodel->get_packing_data();
        $this->load->view('subservice/edit',$data);
    } 
    
    public function Edit_c($sub_servid)
	{
	    if($this->input->post('sub_ca') == '8')
       {
        $data_ca = array(
            'sub_packid'=> $this->input->post('sub_packid'),
            'sub_name'=> $this->input->post('sub_name'),
            'sub_mainamount'=> $this->input->post('sub_mainamount'),
            'sub_discamount'=> $this->input->post('sub_discamount'),
            'sub_include'=> $this->input->post('sub_include'),
           
            'sub_detail'=>$this->input->post('sub_detail'),
            'sub_delete '=>'1',
            );
         $this->Subservicemodel->Edit_loc($data_ca,$sub_servid);
       }     
         redirect('Subservice');
    } 
    
    public function Delete($sub_servid)
    {
        $this->Subservicemodel->Delete($sub_servid);
        redirect('Subservice');
    }
    
    public function get_bycategory() {
        $categoryid= $this->input->post('categoryid');
        $data = $this->Locationmodel->subcat_bycat($categoryid);
        echo json_encode($data); 
    }
}
?>