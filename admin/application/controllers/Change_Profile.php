<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_Profile extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model('Changeprofilemodel');
	
	
		
	}
	
	public function index()
	{
        
	     $data['pf_data'] = $this->Changeprofilemodel->get_pf_data($this->session->userdata('admin_id'));
         $this->load->view('change_profile',$data);
       
    }
    
    public function edit_prf()
    {
        
        if($this->input->post('add_cpf') == '10')
        {
             $admin_id = $this->session->userdata('admin_id');
             $data_array = array(
               'admin_name' => $this->input->post('admin_name'),
               'admin_email' => $this->input->post('admin_email'),
                );
             $result= $this->Changeprofilemodel->edit_prf($admin_id,$data_array); 
             redirect(base_url('Change_Profile'));
        }
        
       
    }
    
    public function edit_pass()
    {
        if($this->input->post('add_caty') == '14')
        {
           $old_pass = $this->input->post('old_pass');
           $ol_password = md5($this->input->post('ol_password'));
           if($old_pass == $ol_password)
           {
               $admin_id = $this->session->userdata('admin_id');
               $new_password = md5($this->input->post('new_password'));
               $result= $this->Changeprofilemodel->edit_pass($admin_id,$new_password); 
               if($result == '1') {redirect(base_url('Home/logout'));}
             else{redirect(base_url('Change_Profile'));}
           }
           else
           {
              $messge = array('message_del' => 'Old Password not match with system','message_type' => 'Success');
              $this->session->set_flashdata('item', $messge);
              redirect(base_url('Change_Profile'));
           }
        }
    }
   
}
?>