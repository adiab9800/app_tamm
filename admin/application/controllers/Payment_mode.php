<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_mode extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Payment_modemodel');
		
	}
	
	public function index()
	{
     
		$data['booking_data'] = $this->Payment_modemodel->get_mode_data();
        $this->load->view('payment_mode/view',$data);
        
    }
    public function Add()
	{
        $this->load->view('payment_mode/add',$data);
    } 
    
    public function add_c()
	{
     
		  $data = array(
            'status' => $this->input->post('status'),
            
            );
         $this->db->insert('tbl_payment_mode',$data);
        $messge = array('message_del' => 'Data Added Successfully!','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
     
      redirect('payment_mode');
         
    }
    public function edit($id)
	{
        $data['mode_data'] = $this->db->get_where('tbl_payment_mode',array('payment_mode_id'=>$id))->row_array();
        $this->load->view('payment_mode/edit',$data);
    } 
    public function view($id)
	{
        $data['mode_data'] = $this->db->get_where('tbl_payment_mode',array('payment_mode_id'=>$id))->row_array();
        $this->load->view('payment_mode/view_data',$data);
    } 
    
    public function edit_c()
	{  
	      $payment_mode_id=$this->input->post('payment_mode_id');
	      $data = array(
            'status' => $this->input->post('status'),
            );
        $this->db->where('payment_mode_id',$payment_mode_id);
        $this->db->update('tbl_payment_mode',$data);
        $messge = array('message_del' => 'Data Updated Successfully!','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
     
      redirect('payment_mode');
         
    }
     public function delete($id)
	{    
         $this->db->where('payment_mode_id',$id);
        $result=$this->db->delete('tbl_payment_mode');
        if($result)
        {
               $messge = array('message_del' => 'Data deleted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not deleted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('payment_mode');
      
    
    }
}
?>