<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotions extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model('Couponcodemodel');
	}
	
	public function index()
	{
     
		if ($this->session->userdata('is_admin_login')) {
           
            $data['coupon_data'] = $this->Couponcodemodel->get_coupon_data();
            $this->load->view('promotions/view',$data);
        } 
        else
        {
            $messge = array('message_del' => 'Login Please...','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
               redirect('Home');
       	}
    }
    public function Add()
	{
	    $this->load->view('promotions/add',$data);
    } 
    
    public function Add_c()
	{
        $coupontype= $this->input->post('coupontype');
       
        
       
        
        $data_prdt = array(
             'coupon_addby'=> '0',
             'coupon_code' => $this->input->post('coupon_code'),
             'coupon_amount' => $this->input->post('coupon_amount'),
             'coupon_desc' => $this->input->post('coupon_desc'),
             
             'coupon_per' => $this->input->post('coupon_per'),
             'coupon_expiry' => $this->input->post('coupon_expiry'),
             'coupon_delete'=>'0'
            );
        $this->Couponcodemodel->Add_c($data_prdt);
        redirect('promotions');
    } 
    
    public function View_data($coupon_id)
    {
        $data['cat_data'] = $this->Couponcodemodel->get_cat_data();
	    $data['prdt_data'] =$this->Couponcodemodel->get_product_data();
	    $data['coupon_data'] =$this->Couponcodemodel->selected_coupon_data($coupon_id);
        $this->load->view('promotions/view_detail',$data);
    }
    
    
     public function Edit($coupon_id)
	{
	    $data['coupon_data'] = $this->Couponcodemodel->selected_coupon_data($coupon_id);
        $this->load->view('promotions/edit',$data);
    }   
    
    public function View($coupon_id)
	{
	    $data['coupon_data'] = $this->Couponcodemodel->selected_coupon_data($coupon_id);
        $this->load->view('promotions/edit',$data);
    } 
    
    
   public function Pending_Request()
    { 
        $data['pen_data'] = $this->Couponcodemodel->Pending_Request();
        $this->load->view('promotions/pending_request',$data);
    }
    
    public function Accept($coupon_id)
    {
        $this->Couponcodemodel->Accept($coupon_id);
        $data['pen_data'] = $this->Couponcodemodel->Pending_Request();
        $this->load->view('promotions/pending_request',$data);
    }
    
    public function Edit_c()
	{
	   $coupon_id= $this->input->post('coupon_id');
	    $data_prdt = array(
             'coupon_addby'=> '0',
             'coupon_code' => $this->input->post('coupon_code'),
              'coupon_desc' => $this->input->post('coupon_desc'),
             'coupon_per' => $this->input->post('coupon_per'),
             'coupon_amount' => $this->input->post('coupon_amount'),
             'coupon_expiry' => $this->input->post('coupon_expiry'),
             'coupon_status' => $this->input->post('coupon_status'),
              'coupon_delete'=>'0'
            );
            $this->db->where('coupon_id',$coupon_id);
        $this->db->update('tbl_coupon',$data_prdt);
        
        redirect('promotions/Edit/'.$coupon_id);
    } 
    
    public function Delete($coupon_id)
    {  
        $this->db->delete('tbl_coupon',$coupon_id);
        redirect('promotions');
    }
    
    public function Delete_data($coupon_id,$coupon_type,$data_id)
    {
        $result = $this->Couponcodemodel->Get_Data($coupon_id,$coupon_type);
        $prdt_data = explode(',',$result['coupon_prdt']);
       
        foreach($prdt_data as $val_data)
        {
            if($val_data != $data_id)
            {
                $data_prdt[] = $val_data;
            }
        }
       $this->Couponcodemodel->Delete_data($data_prdt,$coupon_id);
       redirect('promotions/View_data/'.$coupon_id);
    }
}
?>