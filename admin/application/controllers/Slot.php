<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slot extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{    $this->db->order_by('slot_id','desc');
	      $data['booking_data'] = $this->db->get('tbl_slots')->result_array();
      
        $this->load->view('slot/view',$data);
        
    }
    public function Add()
	{
        $this->load->view('slot/add',$data);
    } 
    
    public function add_c()
	{
       foreach($_POST['slot_time'] as $key =>$value)
       {
		  $data = array(
            'slot_date' => $this->input->post('slot_date'),
            'slot_month' => $this->input->post('slot_month'),
            'slot_year' => $this->input->post('slot_year'),
            'slot_time' => $value,
            'slot_day' => $this->input->post('slot_date').'-'.$this->input->post('slot_month').'-'.$this->input->post('slot_year'),
            );
         $this->db->insert('tbl_slots',$data);
       }
        $messge = array('message_del' => 'Data Added Successfully!','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
     
      redirect('slot');
         
    }
    public function edit($id)
	{
        $data['booking_data'] = $this->db->get_where('tbl_slots',array('slot_id'=>$id))->row_array();
        $this->load->view('slot/edit',$data);
    } 
    public function view($id)
	{
        $data['booking_data'] = $this->db->get_where('tbl_slots',array('slot_id'=>$id))->row_array();
        $this->load->view('slot/view_data',$data);
    } 
    
    public function edit_c()
	{  
	      $slot_id=$this->input->post('slot_id');
	      $data = array(
            'status' => $this->input->post('status'),
            'arabic_status' => $this->input->post('arabic_status'),
            
            );
        $this->db->where('slot_id',$slot_id);
        $this->db->update('tbl_slots',$data);
        $messge = array('message_del' => 'Data Updated Successfully!','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
     
      redirect('slot');
         
    }
     public function delete($id)
	{    
         $this->db->where('slot_id',$id);
        $result=$this->db->delete('tbl_slots');
        if($result)
        {
               $messge = array('message_del' => 'Data deleted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not deleted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('slot');
      
    
    }
}
?>