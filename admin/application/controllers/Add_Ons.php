
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_Ons extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Addonsmodel');
		
	}
	
	public function index()
	{
        $data['addons_data'] = $this->Addonsmodel->get_addon_data();
        $this->load->view('addon/view',$data);
        
    }
    public function Add()
	{
        $data['pack_data'] = $this->Addonsmodel->get_packing_data();
        $this->load->view('addon/add',$data);
    } 
    
    public function Add_c()
	{
       if($this->input->post('sub_ca') == '8')
       {
         
        $data_ca = array(
            'addon_packid'=> $this->input->post('addon_packid'),
            'addon_name'=> $this->input->post('addon_name'),
            'addon_price'=> $this->input->post('addon_price'),
           
            'addon_desc'=>$this->input->post('addon_desc'),
            'addon_delete '=>'1',
            );
          
       $this->Addonsmodel->Add_loc($data_ca);
       }
       
        redirect('Add_Ons');
    } 
    
     public function Edit($addon_id)
	{
       
	    $data['addon_data'] = $this->Addonsmodel->selected_addon_data($addon_id);
        $data['pack_data'] = $this->Addonsmodel->get_packing_data();
        $this->load->view('addon/edit',$data);
    } 
    
    public function Edit_c()
	{
	    if($this->input->post('sub_ca') == '8')
       {
         $data_ca = array(
            'addon_id'=> $this->input->post('addon_id'),
            'addon_packid'=> $this->input->post('addon_packid'),
            'addon_name'=> $this->input->post('addon_name'),
            'addon_price'=> $this->input->post('addon_price'),
           
            'addon_desc'=>$this->input->post('addon_desc'),
            'addon_delete '=>'1',
            );
         $this->Addonsmodel->Edit_loc($data_ca);
       }     
         redirect('Add_Ons');
    } 
    
    public function Delete($addon_id)
    {
        $this->Addonsmodel->Delete($addon_id);
        redirect('Add_Ons');
    }
    
    public function get_bycategory() {
        $categoryid= $this->input->post('categoryid');
        $data = $this->Locationmodel->subcat_bycat($categoryid);
        echo json_encode($data); 
    }
}
?>