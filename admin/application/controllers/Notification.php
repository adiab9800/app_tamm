<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
     
		if ($this->session->userdata('is_admin_login')) {
            $data['notification_data'] = $this->db->get('tbl_notification')->result_array();
            $this->load->view('notification',$data);
        } 
        else
        {
            $messge = array('message_del' => 'Login Please...','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
            redirect('Home');
       	}
    }
    
    public function Add()
    {
        $this->load->view('add_notification');
    }
    
    public function Add_c()
    {  
         $data = array(
             'user_type'=> $this->input->post('user_type'),
             'title'=> $this->input->post('title'),
             'message' => $this->input->post('message'),
             'notification_datetime' => date('d-m-Y H:i:s'),
            );
        $this->db->insert('tbl_notification',$data);  
                if($this->input->post('user_type')=='All')
                {
                   
                $user_data=$this->db->get_where('tbl_user',array('user_status'=>'0'))->result_array();
                foreach($user_data as $user_data)
                {
                          $datapayload=array(  'title'=>'New Notification',
                            'sound' => "default",
                            'msg'=>$msg,
                            'data'=>$datapayload,
                            'body'=>$message,
                            'color' => "#79bc64"
                   );
              
                        $message=$this->input->post('message');
                        $msg = urlencode($message);
                        $data = array(
                            'title'=>'New Notification',
                            'sound' => "default",
                            'msg'=>$msg,
                            'data'=>$datapayload,
                            'body'=>$message,
                            'color' => "#79bc64"
                        );
                        $fields = array(
                            'to'=>$user_data["device_token"],
                            'notification'=>$data,
                            'data'=>$datapayload,
                            "priority" => "high",
                        );
                        $headers = array(
                            'Authorization: key='.FCM_KEY,
                            'Content-Type: application/json'
                        );
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                        $result = curl_exec($ch);
                        curl_close( $ch );
                }
               
                $user_data=$this->db->get_where('tbl_washer',array('user_status'=>'0'))->result_array();
                
                foreach($user_data as $user_data)
                {
                          $datapayload=array('msgBody'=>'Notification',
                                        "msgTitle"=> "Title of your Notification",
                                        "msgId"=> "000001",
                                        "data" => "This is sample payloadData");
              
                        $message=$this->input->post('message');
                        $msg = urlencode($message);
                        $data = array(
                            'title'=>'New Notification',
                            'sound' => "default",
                            'msg'=>$msg,
                            'data'=>$datapayload,
                            'body'=>$message,
                            'color' => "#79bc64"
                        );
                        $fields = array(
                            'to'=>$user_data["device_token"],
                            //'notification'=>$data,
                            'data'=>$data,
                            "priority" => "high",
                        );
                        $headers = array(
                            'Authorization: key='.AGENT_FCM_KEY,
                            'Content-Type: application/json'
                        );
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                        $result = curl_exec($ch);
                        curl_close( $ch );
                }
                    
                }
                else{
                if($this->input->post('user_type')=='User')
                {
                $user_data=$this->db->get_where('tbl_user',array('user_status'=>'0'))->result_array();
                $fcm_key=FCM_KEY;
              
                }
                else if($this->input->post('user_type')=='User')
                {
                $user_data=$this->db->get_where('tbl_washer',array('user_status'=>'0'))->result_array();
                $fcm_key=AGENT_FCM_KEY;
                    
                }
                foreach($user_data as $user_data)
                {
                    
 
                    $datapayload=array('msgBody'=>'Notification',
                                        "msgTitle"=> "Title of your Notification",
                                        "msgId"=> "000001",
                                        "data" => "This is sample payloadData");
                        $message=$this->input->post('message');
                        $msg = urlencode($message);
                        $data = array(
                            'title'=>'New Notification',
                            'sound' => "default",
                            'msg'=>$msg,
                            'data'=>$datapayload,
                            'body'=>$message,
                            'color' => "#79bc64"
                        );
                        $fields = array(
                            'to'=>$user_data["device_token"],
                           // 'notification'=>$data,
                            'data'=>$data,
                            "priority" => "high",
                        );
                        $headers = array(
                            'Authorization: key='.$fcm_key,
                            'Content-Type: application/json'
                        );
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                        $result = curl_exec($ch);
                        curl_close( $ch );
                }
                }
        
        redirect('notification');
    }
    
   
   
    
}
?>