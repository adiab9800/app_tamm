   <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="<?=base_url(); ?>"> <img alt="image" src="<?=base_url();?>upload/logo.png" class="header-logo" /> <span
                class="logo-name"></span>
            </a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Hello, <?= $this->session->userdata('admin_username');?></li>
            <li class="dropdown">
              <a href="<?=base_url();?>" class="nav-link"><i data-feather="monitor"></i><span>Dashboard</span></a>
            </li>
             <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="briefcase"></i><span>Vehicle</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?=base_url(); ?>vehicle/brand">Vehicle Brands</a></li>
                <li><a class="nav-link" href="<?=base_url(); ?>vehicle/model">Vehicle Models</a></li>
                <li><a class="nav-link" href="<?=base_url(); ?>vehicle/type">Vehicle Type</a></li>
                <li><a class="nav-link" href="<?= base_url();?>Vehicle">Vehicle List</a></li>
              </ul>
            </li>
       
           
            <li><a class="nav-link" href="<?= base_url();?>User"><i data-feather="file"></i><span>User</span></a></li>
            <li><a class="nav-link" href="<?= base_url();?>Washer"><i data-feather="file"></i><span>Washer</span></a></li>
            <!--<li><a class="nav-link" href="<?= base_url();?>Package"><i data-feather="file"></i><span>Packages</span></a></li>-->
            <li><a class="nav-link" href="<?= base_url();?>Services"><i data-feather="file"></i><span>Services</span></a></li>
           <!-- <li><a class="nav-link" href="<?= base_url();?>Service_center"><i data-feather="file"></i><span>Service Center</span></a></li>-->
            <li><a class="nav-link" href="<?= base_url();?>promotions"><i data-feather="file"></i><span>Coupons</span></a></li>
            <li><a class="nav-link" href="<?= base_url();?>rating"><i data-feather="file"></i><span>Ratings</span></a></li>
           
            <li><a class="nav-link" href="<?= base_url();?>Booking"><i data-feather="file"></i><span>Booking</span></a></li>
            <li><a class="nav-link" href="<?= base_url();?>Booking_status"><i data-feather="file"></i><span>Booking status</span></a></li>
           <!-- <li><a class="nav-link" href="<?= base_url();?>Payment_mode"><i data-feather="file"></i><span>Payment Mode</span></a></li>-->
            <li><a class="nav-link" href="<?= base_url();?>Country_code"><i data-feather="file"></i><span>Country code</span></a></li>
            <li><a class="nav-link" href="<?= base_url();?>home/new_map"><i data-feather="file"></i><span>New Map</span></a></li>
           
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="briefcase"></i><span>Setup</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?=base_url(); ?>home/select_location">Allowed Locations</a></li>
                <li><a class="nav-link" href="<?=base_url(); ?>About">About us</a></li>
                <li><a class="nav-link" href="<?=base_url(); ?>Term_Condition" active>Terms & Conditions</a></li>
                <li><a class="nav-link" href="<?=base_url(); ?>Privacy_Policy">Privacy Policy</a></li>
                <li><a class="nav-link" href="<?=base_url(); ?>Change_Profile">Edit Profile</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="briefcase"></i><span>Reports</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?=base_url(); ?>Report">Today's Bookings</a></li>
                <li><a class="nav-link" href="<?=base_url(); ?>Report/total">Total Bookings</a></li>
                <li><a class="nav-link" href="<?=base_url(); ?>Report/status/Pending">Pending Bookings</a></li>
                <li><a class="nav-link" href="<?=base_url(); ?>Report/status/Completed">Completed Bookings</a></li>
                <li><a class="nav-link" href="<?=base_url(); ?>Report/washer">Booking by washer</a></li>
              </ul>
            </li>
          
             <li><a class="nav-link" href="<?= base_url();?>notification"><i data-feather="file"></i><span>Notification</span></a></li>
             <li><a class="nav-link" href="<?= base_url();?>slot"><i data-feather="file"></i><span>Slots</span></a></li>
          
         <li><a class="nav-link" href="<?=base_url(); ?>Home/logout" class="dropdown-item has-icon text-danger"> <i class="fas fa-sign-out-alt"></i> Logout</a></li>
          </ul>
        </aside>
      </div>
      <!-- Main Content -->
     <div class="main-content">
    
