<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>

      <script type="text/javascript" src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
      <link rel="stylesheet" type="text/css" href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css">
  <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4>Add Notification</h4>
              </div>
              
              <form role="form" action="<?=base_url();?>Notification/Add_c" method="POST">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">User Type</label>
                            <select class="form-control" id="user_type" name="user_type">
                                <option value="All">All</option>
                                <option value="User">User</option>
                                <option value="Washer">Washer</option>
                            </select>
                        </div>
                         <div class="form-group col-md-12" id="user_div" style="display:none">
                            <label for="exampleInputEmail1">Select User</label>
                            <select class="form-control select2" id="user_id" name="user_id" multiple>
                              <option value="All">All</option>
                                <?php $user_data=$this->db->get_where('tbl_user',array('user_delete'=>'0'))->result_array();
                               foreach($user_data as $user_data)
                                { ?>
                                 <option value="<?=$user_data['user_id']; ?>"><?=$user_data['user_fname']; ?> <?=$user_data['user_lname']; ?></option>
                               
                                <?php } ?>
                            </select>
                        </div>
                      
                         <div class="form-group col-md-12" id="washer_div" style="display:none">
                            <label for="exampleInputEmail1">Select Washer</label>
                            <select class="form-control select2"  id="user_id" name="user_id" multiple>
                              <option value="All">All</option>
                                <?php $user_data=$this->db->get_where('tbl_washer',array('user_delete'=>'0'))->result_array();
                               foreach($user_data as $user_data)
                                { ?>
                                 <option value="<?=$user_data['user_id']; ?>"><?=$user_data['user_fname']; ?> <?=$user_data['user_lname']; ?></option>
                               
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Title</label>
                            <input  name="title" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Title" >
                        </div>
                       
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Message</label>
                            <input name="message" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Message" >
                        </div>
                       
                    </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
           
              </form>
          

          </div>
          
        </div>
        <!-- /.row -->
        </div>
     </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
?>

<script type="text/javascript">
$('#user_type').on('change', function () {
    // if (this.value == '1'); { No semicolon and I used === instead of ==
    if (this.value === 'User'){
        $("#user_div").show();
       $("#washer_div").hide();
    } else if (this.value === 'Washer') {
        $("#washer_div").show();
        $("#user_div").hide();
    }else{
        $("#washer_div").hide();
        $("#user_div").hide();
    }
});

$('#select2').select2({
    placeholder: 'Select data'
});
</script>