<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
        
          <div class="card">
            <div class="card-header">
                  <div class="col-md-9">
                     <h4 >All Payment Mode List</h4>
                  </div>
                 <div class="col-md-3">
                       <a href="<?=base_url();?>payment_mode/add"><button type="button" class="btn btn-block btn-primary ">Add Payment Mode</button></a>
                  </div>   
             
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                  <table id="tableExport" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Mode</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($booking_data as $val_data){?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><?=$val_data['status'];?></td>
                   <td>
                       <a href="<?= base_url();?>payment_mode/view/<?= $val_data['payment_mode_id'];?>"><i class="nav-icon fas fa-eye"></i></a>
                       <a href="<?= base_url();?>payment_mode/edit/<?= $val_data['payment_mode_id'];?>"><i class="nav-icon fas fa-edit"></i></a>
                       <a onclick="return confirm('Are You Sure Delete Data?');" href="<?= base_url();?>payment_mode/delete/<?= $val_data['payment_mode_id'];?>"><i class="nav-icon fas fa-trash"></i></a>
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
               <tr>
                  <th>#</th>
                  <th>Mode</th>
                  <th>Action</th>
                </tr>
               </tfoot>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php
  $this->load->view('include/footer');
?> 