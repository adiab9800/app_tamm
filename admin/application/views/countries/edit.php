<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">Edit Country</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Country_code/edit_c" method="POST" enctype='multipart/form-data'>
                     <input name="id" type="hidden" class="form-control" id="exampleInputEmail1" placeholder="Enter Id" value="<?=$countries_data['id']; ?>">
                         
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Name</label>
                            <input name="name" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Name" value="<?=$countries_data['name']; ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Code</label>
                            <input name="phonecode" type="text" class="form-control"  placeholder="Enter Latitude" value="<?=$countries_data['phonecode']; ?>">
                        </div>
                        
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="sub_ca"  class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
  
?>