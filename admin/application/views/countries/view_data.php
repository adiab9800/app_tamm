<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">View Country</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Country_code/edit_c" method="POST" enctype='multipart/form-data'>
                     <input name="service_center_id" type="hidden" class="form-control" id="exampleInputEmail1" placeholder="Enter Id" value="<?=$countries_data['service_center_id']; ?>">
                         
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Name:</label><br>
                            <?=$countries_data['name']; ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Code:</label><br>
                            <?=$countries_data['sortname']; ?>
                        </div>
                        
                    </div>
                    
                 
                
                </div>
               
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
  
?>