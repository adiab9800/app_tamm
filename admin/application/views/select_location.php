<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>

  <style id="compiled-css" type="text/css">
      body {         
    font:14px sans-serif;           
} 
input {
    margin: 0.6em 0.6em 0; 
    width:398px;
}
#map_canvas {         
    height: 200px;         
    width: 400px;         
    margin: 0.6em;       
}
    /* EOS */
  </style>
  
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
               <div class="col-md-9">
                <h4>Select Location to allow</h4>
                <script src="https://maps.google.com/maps/api/js?libraries=places&region=uk&language=en&sensor=false&key=AIzaSyDQ69wZR1GPEeLAxyu-vkSSo_dzpZTOV2c"></script>
               <form action="<?=base_url(); ?>home/select_location_c" method="post">
                         <div class="row">
                             <div class="col-md-10">
                                   <input id="searchTextField" name="area" class="form-control" type="text" size="500">                  
                             </div>
                          <div class="col-md-10">
                              <center>Or</center>
                          </div>
                            <div class="col-md-10">
                                   <input id="searchTextField" name="area_name" class="form-control" placeholder='Enter location Name manually' type="text" size="500">                  
                             </div>
                            
                             <input type="submit" name="submit" class="btn btn-primary col-md-1" value="Submit">                  
                             
                            
                         </div>
               </form>
                <div id="map_canvas"></div>    
                </div>
             </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Area</th>
                  <th>Action</th>
                 </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 $notification_data=$this->db->get('tbl_allowed_area')->result_array();
                 foreach($notification_data as $val_data){?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><?=$val_data['area'];?></td>
                    <td><a href="<?=base_url();?>home/delete_allowed_area/<?=$val_data['allowed_area_id']; ?>" ><button class="btn btn-danger">Delete</button></a></td>
                  
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Area</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
?>
<script type="text/javascript">


 $(function(){     
    var lat = -33.8688,
        lng = 151.2195,
        latlng = new google.maps.LatLng(lat, lng),
        image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png'; 
         
    var mapOptions = {           
            center: new google.maps.LatLng(lat, lng),           
            zoom: 13,           
            mapTypeId: google.maps.MapTypeId.ROADMAP         
        },
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
        marker = new google.maps.Marker({
            position: latlng,
            map: map,
            icon: image
         });
     
    var input = document.getElementById('searchTextField');         
    var autocomplete = new google.maps.places.Autocomplete(input, {
        types: ["geocode"]
 //     types: ['(cities)']
      });          
    
    autocomplete.bindTo('bounds', map); 
    var infowindow = new google.maps.InfoWindow(); 
 
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        infowindow.close();
        var place = autocomplete.getPlace();
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  
        }
        
        moveMarker(place.name, place.geometry.location);
    });  
    
    $("input").focusin(function () {
        $(document).keypress(function (e) {
            if (e.which == 13) {
                infowindow.close();
                var firstResult = $(".pac-container .pac-item:first").text();
                
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({"address":firstResult }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var lat = results[0].geometry.location.lat(),
                            lng = results[0].geometry.location.lng(),
                            placeName = results[0].address_components[0].long_name,
                            latlng = new google.maps.LatLng(lat, lng);
                        
                        moveMarker(placeName, latlng);
                        $("input").val(firstResult);
                    }
                });
            }
        });
    });
     
     function moveMarker(placeName, latlng){
        marker.setIcon(image);
        marker.setPosition(latlng);
        infowindow.setContent(placeName);
        infowindow.open(map, marker);
     }
});

</script>

  
             

