<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>

    <section class="content">
      <div class="row">
        <div class="col-12">
          

          <div class="card">
            <div class="card-header">
               <div class="col-md-9">
                     <h4 >All Notification List</h4>
                  </div>
                 <div class="col-md-3">
                       <a href="<?=base_url();?>Notification/add"><button type="button" class="btn btn-block btn-primary ">Add Notification</button></a>
                  </div>   
             
             </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>User Type</th>
                  <th>Title</th>
                  <th>Message</th>
                  <th>Date Time</th>
                 </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($notification_data as $val_data){?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   
                   <td><?=$val_data['user_type'];?></td>
                   <td><?=$val_data['title'];?></td>
                   <td><?=$val_data['message'];?></td>
                   <td><?php echo date("d-m-Y", strtotime($val_data['notification_datetime']));?></td>
                  
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>User Type</th>
                  <th>Title</th>
                  <th>Message</th>
                  <th>Date Time</th>
                 </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
?>
