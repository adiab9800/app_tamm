<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">Edit User</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>User/Edit_c" method="POST" enctype='multipart/form-data'>
                  <input type="hidden" value="<?= $user_data['user_id'];?>" name="user_id">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">First Name</label>
                            <input name="user_fname" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter First Name" value="<?= $user_data['user_fname'];?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Last Name</label>
                            <input  name="user_lname" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Last Name" value="<?= $user_data['user_lname'];?>">
                        </div>
                       
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Email</label>
                            <input name="user_email" type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="<?= $user_data['user_email'];?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Phone No</label>
                            <input name="user_phoneno" type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter Phone No" value="<?= $user_data['user_phno'];?>">
                        </div>
                         
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Profile picture</label>
                            <input name="profile_pic" type="file" class="form-control">
                            <img src="<?= $user_data['profile_pic'];?>" width="300px">
                       
                        </div> 
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Country</label>
                            <select class="form-control" name="country_id">
                                <?php $country_list=$this->db->get("tbl_countries")->result_array();
                                foreach($country_list as $country_list)
                                { ?>
                                <option value="<?=$country_list['phonecode']; ?>" <?php if($country_list['phonecode']==$user_data['country_id']) {echo 'selected';} ?>><?=$country_list['phonecode']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                         <div class="form-group col-md-12"> 
                            <label for="exampleInputEmail1">Status</label>
                             <select name="status" class="form-control">
                                 <option value="0" <?php if($user_data['user_status']=='0') {echo 'selected';} ?>>Active</option>
                                 <option value="1" <?php if($user_data['user_status']=='1') {echo 'selected';} ?>>Inactive</option>
                          </select>
                          </div>
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 