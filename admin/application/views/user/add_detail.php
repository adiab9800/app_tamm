<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4>Add User</h4>
              </div>
              
              <form role="form" action="<?=base_url();?>User/Add_c" method="POST" enctype='multipart/form-data'>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">First Name</label>
                            <input name="user_fname" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter First Name" >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Last Name</label>
                            <input  name="user_lname" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Last Name" >
                        </div>
                       
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Email</label>
                            <input name="user_email" type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Phone No</label>
                            <input name="user_phoneno" type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter Phone No" >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Password</label>
                            <input name="user_password" type="password" class="form-control" id="exampleInputEmail1" placeholder="Enter Password" >
                        </div>
                         <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Profile picture</label>
                            <input name="profile_pic" type="file" class="form-control">
                        </div> 
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Country</label>
                            <select class="form-control" name="country_id">
                                <?php $country_list=$this->db->get("tbl_countries")->result_array();
                                foreach($country_list as $country_list)
                                { ?>
                                <option value="<?=$country_list['phonecode']; ?>"><?=$country_list['phonecode']; ?></option>
                                <?php } ?>
                            </select>
                            
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
           
              </form>
          

          </div>
          
        </div>
        <!-- /.row -->
        </div>
     </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 