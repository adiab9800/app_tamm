<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
    <section class="content">
      <div class="row">
        <div class="col-12">
          

          <div class="card">
            <div class="card-header">
               <div class="col-md-9">
                     <h4 >All User List</h4>
                  </div>
                 <div class="col-md-3">
                       <a href="<?=base_url();?>User/Add"><button type="button" class="btn btn-block btn-primary ">Add User</button></a>
                  </div>   
             
             </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone No</th>
                  <th>Create Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($user_data as $val_data){?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><?=$val_data['user_fname'];?>&nbsp;<?=$val_data['user_lname'];?></td>
                   
                   <td><?=$val_data['user_email'];?></td>
                   <td><?=$val_data['user_phno'];?></td>
                   <td><?php echo date("d-m-Y", strtotime($val_data['user_creattime']));?></td>
                   
                   <td>
                       <a href="<?= base_url();?>User/View/<?= $val_data['user_id'];?>"><i class="nav-icon fas fa-eye"></i></a>
                       <a href="<?= base_url();?>User/Edit/<?= $val_data['user_id'];?>"><i class="nav-icon fas fa-edit"></i></a>
                       <a onclick="return confirm('Are You Sure Delete Data?');" href="<?= base_url();?>User/Delete/<?= $val_data['user_id'];?>"><i class="nav-icon fas fa-trash"></i></a>
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone No</th>
                  <th>Create Date</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 