<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4>View Package</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Package/Edit_pack" method="POST" enctype='multipart/form-data'>
                <input type="hidden" value="<?= $pack_data['package_id'];?>" name="package_id">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Package Type</label><br>
                                <?php if($pack_data['package_type'] == '1'){echo "Single Packages";}?> 
                                <?php if($pack_data['package_type'] == '2'){echo "Monthly Packages";}?> 
                                <?php if($pack_data['package_type'] == '3'){echo "Combo Packages ";}?>
                           
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                        <label for="exampleInputEmail1"> Vehicle Type</label><br>
                              <?php foreach($vhl_data as $val_data){?>
                              <?php if($pack_data['package_vehicle'] == $val_data['type_id']){echo $val_data['type_name'];}?> 
                                <?php } ?>
                        </div>  
                        <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Service</label><br>
                           
                                <?php foreach($cat_data as $val_cat){?>
                                <?php if($pack_data['package_service'] == $val_cat['cat_id']){echo $val_cat['cat_name'];}?> 
                                <?php } ?>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Package Name</label><br>
                           <?=$pack_data['package_name'];?>
                        </div> 
                        
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Package Description</label><br>
                            <?=$pack_data['package_about'];?>
                        </div> 
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Package Price</label><br>
                            <?=$pack_data['package_price'];?>
                        </div> 
                        
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Package Image</label> <br>
                            <img src="<?=base_url();?>upload/packageimage/<?=$pack_data['package_image'];?>" style="height:50px;width:50px;">
                        </div> 
                         
                    </div>
                
                </div>
                <!-- /.card-body -->

              </form>
            </div>         

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 
<!-- Script -->
<script type='text/javascript'>
// baseURL variable
var baseURL= "<?php echo base_url();?>";

$(document).ready(function(){
$('select.veh_type').on('change',function(){
    var veh_type = $(this).val(); 
    $.ajax({
       type : "ajax",
       url  : "<?=base_url()?>Package/get_service_bytype/"+veh_type,
       dataType : "JSON",
       data : {veh_type:veh_type},
       success : function(data){
            var html = '';
            var i;
            for(i=0; i<data.length; i++)
            {
              html += '<option value=' +data[i].cat_id+'>'+data[i].cat_name+'</option>';
            }
            $('.service').html(html); 
            
       }
    });
});




  // Get Sub Category on change of category
 
});
</script>