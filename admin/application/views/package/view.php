<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
 <section class="content">
      <div class="row">
        <div class="col-12">
          

          <div class="card">
            <div class="card-header">
            
                 <div class="col-md-9">
                     <h4>All Package List</h4>
                  </div>
                 <div class="col-md-3">
                       <a href="<?=base_url();?>Package/Add"><button type="button" class="btn btn-block btn-primary ">Add Package</button></a>
                  </div>   
             </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                   
                    
                  <th>#</th>
                  <th>Image</th>
                  <th>Vehicle Type</th>
                  <th>Package Type</th>
                  <th>Service Name</th>
                  <th>Package Name</th>
                  <th>Package Description</th>
                  <th>Package Price</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($pack_data as $val_data){
                 
                     if($val_data['package_type'] == '1')
                     {
                         $package = "Single Packages";
                     }
                     if($val_data['package_type'] == '2')
                     {
                         $package = "Monthly Packages";
                     }
                     if($val_data['package_type'] == '3')
                     {
                         $package = "Combo Packages";
                     }
                 ?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><img src="<?=base_url();?>upload/packageimage/<?=$val_data['package_image'];?>" style="width:50px;height:50px;"></td>
                   <td><?=$val_data['type_name'];?></td>
                   <td><?=$package;?></td>
                   <td><?=$val_data['cat_name'];?></td>
                   <td><?=$val_data['package_name'];?></td>
                   <td><?=$val_data['package_about'];?></td>
                   <td>₹ <?=$val_data['package_price'];?></td>
                   <td>
                       <a href="<?= base_url();?>Package/View/<?= $val_data['package_slug'];?>"><i class="nav-icon fas fa-eye"></i></a>
                       <a href="<?= base_url();?>Package/Edit/<?= $val_data['package_slug'];?>"><i class="nav-icon fas fa-edit"></i></a>
                       <a onclick="return confirm('Are You Sure Delete Data?');" href="<?= base_url();?>Package/Delete/<?= $val_data['package_slug'];?>"><i class="nav-icon fas fa-trash"></i></a>
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Image</th>
                  <th>Vehicle Type</th>
                  <th>Package Type</th>
                  <th>Service Name</th>
                  <th>Package Name</th>
                  <th>Package Description</th>
                  <th>Package Price</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 