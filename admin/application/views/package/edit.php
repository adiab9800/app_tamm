<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 >Edit Package</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Package/Edit_pack" method="POST" enctype='multipart/form-data'>
                <input type="hidden" value="<?= $pack_data['package_id'];?>" name="package_id">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Select Package Type</label>
                            <select name="package_type" class="form-control" id="exampleInputEmail1" required>
                                 <option value="0" <?php if($pack_data['package_type'] == '0'){echo "selected";}?> >Select Package Type</option>
                                 <option value="1" <?php if($pack_data['package_type'] == '1'){echo "selected";}?> >Single Packages</option>
                                 <option value="2" <?php if($pack_data['package_type'] == '2'){echo "selected";}?> >Monthly Packages </option>
                                 <option value="3" <?php if($pack_data['package_type'] == '3'){echo "selected";}?> >Combo Packages </option>
                           
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Select Vehicle Type</label>
                            <select name="package_vehicle" class="form-control veh_type" id="exampleInputEmail1" required>
                                <option value="0">Select Vehicle Type</option>
                                <?php foreach($vhl_data as $val_data){?>
                                <option value="<?=$val_data['type_id'];?>" <?php if($pack_data['package_vehicle'] == $val_data['type_id']){echo "selected";}?> ><?=$val_data['type_name'];?></option>
                                <?php } ?>
                            </select>
                        </div>  
                        <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Select service</label>
                            <select name="package_service" class="form-control services" id="exampleInputEmail1" multiple required>
                                <option value="0">Select service</option>
                                <?php foreach($cat_data as $val_cat){?>
                                  <option value="<?=$val_cat['cat_id'];?>" <?php if($pack_data['package_service'] == $val_cat['cat_id']){echo "selected";}?> ><?=$val_cat['cat_name'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Package Name</label>
                            <input name="package_name" type="text" class="form-control" id="exampleInputEmail1" value="<?=$pack_data['package_name'];?>" placeholder="Enter Package Name" required>
                        </div> 
                        
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Package Description</label>
                            <input name="package_about" type="text" class="form-control" id="exampleInputEmail1" value="<?=$pack_data['package_about'];?>"  placeholder="Enter Package Description" required>
                        </div> 
                        <input type="hidden" name="old_img" value="<?=$pack_data['package_image'];?>"/>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Package Price</label>
                            <input name="package_price" type="number" class="form-control" id="exampleInputEmail1" value="<?=$pack_data['package_price'];?>"  placeholder="Enter Package Price" required>
                        </div> 
                        
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Package Image</label> <br>
                            <img src="<?=base_url();?>upload/packageimage/<?=$pack_data['package_image'];?>" style="height:50px;width:50px;">
                            <input name="cat_image" type="file" class="form-control" id="exampleInputEmail1" >
                        </div> 
                         
                    </div>
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="su_sub_pack" value="50" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>         

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 
<!-- Script -->
<script type='text/javascript'>
// baseURL variable
var baseURL= "<?php echo base_url();?>";

$(document).ready(function(){
$('select.veh_type').on('change',function(){
    var veh_type = $(this).val(); 
    $.ajax({
       type : "ajax",
       url  : "<?=base_url()?>Package/get_service_bytype/"+veh_type,
       dataType : "JSON",
       data : {veh_type:veh_type},
       success : function(data){
            var html = '';
            var i;
            for(i=0; i<data.length; i++)
            {
              html += '<option value=' +data[i].cat_id+'>'+data[i].cat_name+'</option>';
            }
            $('.service').html(html); 
            
       }
    });
});




  // Get Sub Category on change of category
 
});
</script>