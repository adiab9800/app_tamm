<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
        
          <div class="card">
            <div class="card-header">
                  <div class="col-md-9">
                     <h4 >All Service Center</h4>
                  </div>
                 <div class="col-md-3">
                       <a href="<?=base_url();?>service_center/add"><button type="button" class="btn btn-block btn-primary ">Add Service Center</button></a>
                  </div>   
             
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                  <table id="tableExport" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Lat</th>
                  <th>Long</th>
                  <th>Address</th>
                  <th>About</th>
                 <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($service_center_data as $val_data){?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><?=$val_data['service_center_name'];?></td>
                   <td><?=$val_data['service_center_lat'];?></td>
                   <td><?=$val_data['service_center_long'];?></td>
                   <td><?=$val_data['service_center_address'];?></td>
                   <td><?=$val_data['service_center_about'];?></td>
                   <td>
                       <a href="<?= base_url();?>service_center/view/<?= $val_data['service_center_id'];?>"><i class="nav-icon fas fa-eye"></i></a>
                       <a href="<?= base_url();?>service_center/edit/<?= $val_data['service_center_id'];?>"><i class="nav-icon fas fa-edit"></i></a>
                       <a onclick="return confirm('Are You Sure Delete Data?');" href="<?= base_url();?>service_center/delete/<?= $val_data['service_center_id'];?>"><i class="nav-icon fas fa-trash"></i></a>
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
               <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Lat</th>
                  <th>Long</th>
                  <th>Address</th>
                  <th>About</th>
                  <th>Action</th>
                </tr>
               
                </tfoot>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php
  $this->load->view('include/footer');
?> 