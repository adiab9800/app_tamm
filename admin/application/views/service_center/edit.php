<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">Edit Service Center</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Service_center/edit_c" method="POST" enctype='multipart/form-data'>
                     <input name="service_center_id" type="hidden" class="form-control" id="exampleInputEmail1" placeholder="Enter Id" value="<?=$service_center_data['service_center_id']; ?>">
                         
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Name</label>
                            <input name="service_center_name" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Name" value="<?=$service_center_data['service_center_name']; ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Lat</label>
                            <input name="service_center_lat" type="text" class="form-control"  placeholder="Enter Latitude" value="<?=$service_center_data['service_center_lat']; ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Long</label>
                            <input name="service_center_long" type="text" class="form-control"  placeholder="Enter Longitude" value="<?=$service_center_data['service_center_long']; ?>">
                        </div>
                         <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Address</label>
                            <input name="service_center_address" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Address" value="<?=$service_center_data['service_center_address']; ?>">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">About</label>
                            <input name="service_center_about" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter About" value="<?=$service_center_data['service_center_about']; ?>">
                        </div>
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="sub_ca"  class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
  
?>