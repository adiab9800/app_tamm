<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">Add Vehicle</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Vehicle/Add_vhl" method="POST">
              <div class="card-body">
                    <div class="row">
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Select Brand</label>
                            <select name="brand_id" class="form-control" id="exampleInputEmail1">
                            <?php 
                            
                            $brand_data=$this->db->get_where('tbl_brand',array('brand_status'=>0))->result_array();
                            foreach($brand_data as $val_data){?>
                                <option value="<?=$val_data['brand_id'];?>"  ><?=$val_data['brand_name'];?></option>
                            <?php } ?>
                            </select>
                        </div>
                       <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Select Model</label>
                              <select name="model_id" class="form-control" id="exampleInputEmail1">
                            <?php 
                            
                            $model_data=$this->db->get_where('tbl_model',array('model_status'=>0))->result_array();
                            foreach($model_data as $val_data){?>
                                 <option value="<?=$val_data['model_id'];?>" ><?=$val_data['model_name'];?></option>
                           <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Select Type</label>
                           <select name="type_id" class="form-control" id="exampleInputEmail1">
                            <?php 
                            
                            $type_data=$this->db->get_where('tbl_type',array('type_status'=>0))->result_array();
                            foreach($type_data as $val_data){?>
                                 <option value="<?=$val_data['type_id'];?>" ><?=$val_data['type_name'];?></option>
                           <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Vehicle No</label> &nbsp;<span style="color:red;"></span>
                            <input name="vhl_no" type="text" class="form-control" value="" id="exampleInputEmail1" placeholder="Enter Vehicle No">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Select user</label>
                            <select name="user_id" class="form-control" id="exampleInputEmail1">
                             <?php 
                            
                            $user_data=$this->db->get_where('tbl_user',array('user_status'=>0))->result_array();
                            foreach($user_data as $val_data){?>
                                 <option value="<?=$val_data['user_id'];?>" ><?=$val_data['user_fname'];?> <?=$val_data['user_lname'];?></option>
                           <?php } ?>
                           </select>
                        </div>
                       
                         
                    </div>
                    
                 
                
                </div>
                  <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" value="8" name="sub_ca"  class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 