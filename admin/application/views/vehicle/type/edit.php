<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">Edit Vehicle Type</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Vehicle/edit_type_c" method="POST" enctype='multipart/form-data'>
                <input type="hidden" name="type_id" value="<?=$type_data['type_id']; ?>">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Vehicle Type</label>
                            <input name="type_name" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Vehicle Type"  value="<?=$type_data['type_name']; ?>">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Icon</label>
                            <input name="type_logo" type="file" class="form-control">
                            <img src="<?=$type_data['type_logo']; ?>" width="300px"> 
                        </div>
                         <div class="form-group col-md-12"> 
                            <label for="exampleInputEmail1">Status</label>
                             <select name="status" class="form-control">
                                 <option value="0" <?php if($model_data['type_status']=='0'){echo 'selected';}?>>Active</option>
                                 <option value="1" <?php if($model_data['type_status']=='1'){echo 'selected';}?>>Inactive</option>
                             </select>
                          </div>
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="sub_ca"  class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
  
?>