<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
        
          <div class="card">
            <div class="card-header">
                  <div class="col-md-9">
                     <h4 >All Vehicle List</h4>
                  </div>
                 <div class="col-md-3">
                       <a href="<?=base_url();?>Vehicle/Add"><button type="button" class="btn btn-block btn-primary ">Add Vehicle</button></a>
                  </div>   
             
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                  <table id="tableExport" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>User</th>
                  <th>Brand</th>
                  <th>Model</th>
                  <th>Type</th>
                  <th>Vehicle No</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($subcat_data as $val_data){?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><?=$val_data['user_fname'];?> <?=$val_data['user_lname'];?></td>
                   <td><?=$val_data['brand_name'];?></td>
                   <td><?=$val_data['model_name'];?></td>
                   <td><?=$val_data['type_name'];?></td>
                   <td><?=$val_data['vhl_no'];?></td>
                   <td>
                       <a href="<?= base_url();?>Vehicle/View/<?= $val_data['vhl_id'];?>"><i class="nav-icon fas fa-eye"></i></a>
                       <a href="<?= base_url();?>Vehicle/Edit/<?= $val_data['vhl_id'];?>"><i class="nav-icon fas fa-edit"></i></a>
                       <a onclick="return confirm('Are You Sure Delete Data?');" href="<?= base_url();?>Vehicle/Delete/<?= $val_data['vhl_id'];?>"><i class="nav-icon fas fa-trash"></i></a>
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
                <tr>
                 <th>#</th>
                   <th>User</th>
                   <th>Brand</th>
                  <th>Model</th>
                  <th>Type</th>
                  <th>Vehicle No</th>
                  <th>Action</th>
                 </tr>
                </tfoot>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 