<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">View Vehicle</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Vehicle/Edit_subcat" method="POST">
                  <input type="hidden" value="<?= $vhl_data['vhl_id'];?>" name="vhl_id">
                <div class="card-body">
                    <div class="row">
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Brand:</label><br>
                             <?php echo $vhl_data['brand_name'];?>
                           
                        </div>
                       <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Model:</label><br>
                            <?php echo $vhl_data['model_name'];?>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Type:</label><br>
                            <?php echo $vhl_data['type_name'];?>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Vehicle No:</label> <br>
                           <?php echo $vhl_data['vhl_no'];?>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">User:</label><br>
                            <?=$vhl_data['user_fname'] ; ?>  <?=$vhl_data['user_lname'] ; ?>
                        </div>
                    </div>
                </div>
              
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 