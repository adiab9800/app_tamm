<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">Edit Vehicle Model</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Vehicle/edit_model_c" method="POST" enctype='multipart/form-data'>
                <input type="hidden" name="model_id" value="<?=$model_data['model_id']; ?>">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Brand:</label><br>
                                <?php 
                                $brand_data=$this->db->get('tbl_brand')->result_array();
                                foreach($brand_data as $brand_data){ ?>
                               <?php if($brand_data['brand_id']==$model_data['brand_id']) {echo $brand_data['brand_name'];} ?>
                                <?php } ?>
                          
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Vehicle model:</label><br>
                           <?=$model_data['model_name']; ?>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Icon:</label>
                            <img src="<?=$model_data['model_logo']; ?>" width="300px"> 
                        </div>
                         <div class="form-group col-md-12"> 
                            <label for="exampleInputEmail1">Status:</label><br>
                             <?php if($model_data['model_status']=='0'){echo 'Active';}?>
                             <?php if($model_data['model_status']=='1'){echo 'Inctive';}?>
                            
                          </div>
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

               
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
  
?>