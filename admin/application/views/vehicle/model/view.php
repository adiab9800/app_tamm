<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
        
          <div class="card">
            <div class="card-header">
                  <div class="col-md-9">
                     <h4 >All Vehicle Model List</h4>
                  </div>
                 <div class="col-md-3">
                       <a href="<?=base_url();?>Vehicle/add_model"><button type="button" class="btn btn-block btn-primary ">Add Vehicle Model</button></a>
                  </div>   
             
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                  <table id="tableExport" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Model</th>
                  <th>Brand</th>
                  <th>Icon</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($model_data as $val_data){
                 if($val_data['model_status']=='0')
                 {
                    $status='Active'; 
                 }
                 else{
                    $status='Inactive'; 
                     
                 }
                 ?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><?=$val_data['model_name'];?></td>
                   <td><?=$val_data['brand_name'];?></td>
                   <td><img src="<?=$val_data['model_logo'];?>" width="250px" ></td>
                   <td><?=$status;?></td>
                   <td>
                       <a href="<?= base_url();?>Vehicle/view_model/<?= $val_data['model_id'];?>"><i class="nav-icon fas fa-eye"></i></a>
                       <a href="<?= base_url();?>Vehicle/edit_model/<?= $val_data['model_id'];?>"><i class="nav-icon fas fa-edit"></i></a>
                       <a onclick="return confirm('Are You Sure Delete Data?');" href="<?= base_url();?>Vehicle/delete_model/<?= $val_data['model_id'];?>"><i class="nav-icon fas fa-trash"></i></a>
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
               <tr>
                  <th>#</th>
                  <th>Model</th>
                  <th>Brand</th>
                  <th>Icon</th>
                  <th>Status</th>
                <th>Action</th>
                </tr>
               
                </tfoot>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php
  $this->load->view('include/footer');
?> 