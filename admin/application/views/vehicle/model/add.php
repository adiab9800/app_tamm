<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">Add Vehicle Model</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Vehicle/add_model_c" method="POST" enctype='multipart/form-data'>
                  
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Brand</label>
                            <select class="form-control" name="brand_id">
                                <?php 
                                $brand_data=$this->db->get('tbl_brand')->result_array();
                                foreach($brand_data as $brand_data){ ?>
                                <option value="<?=$brand_data['brand_id']; ?>"><?=$brand_data['brand_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Vehicle model</label>
                            <input name="model_name" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Vehicle model">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Icon</label>
                            <input name="model_logo" type="file" class="form-control">
                        </div>
                        <div class="form-group col-md-12"> 
                            <label for="exampleInputEmail1">Status</label>
                             <select name="status" class="form-control">
                                 <option value="0" >Active</option>
                                 <option value="1" >Inactive</option>
                             </select>
                          </div>
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="sub_ca"  class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
  
?>