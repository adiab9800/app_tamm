<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">Edit Vehicle Brand</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Vehicle/edit_brand_c" method="POST" enctype='multipart/form-data'>
                <input type="hidden" name="brand_id" value="<?=$brand_data['brand_id']; ?>">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Vehicle Brand</label>
                            <input name="brand_name" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Vehicle Brand"  value="<?=$brand_data['brand_name']; ?>">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Icon</label>
                            <input name="brand_logo" type="file" class="form-control">
                            <img src="<?=$brand_data['brand_logo']; ?>" width="300px"> 
                        </div>
                         <div class="form-group col-md-12"> 
                            <label for="exampleInputEmail1">Status</label>
                             <select name="brand_status" class="form-control">
                                 <option value="0"   <?php if($brand_data['brand_status']=='0'){echo 'selected';}?>>Active</option>
                                 <option value="1"   <?php if($brand_data['brand_status']=='1'){echo 'selected';}?>>Inactive</option>
                             </select>
                          </div>
                    </div>
                    
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="sub_ca"  class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
  
?>