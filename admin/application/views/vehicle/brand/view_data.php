<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">View Vehicle Brand</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Vehicle/edit_brand_c" method="POST" enctype='multipart/form-data'>
                <input type="hidden" name="brand_id" value="<?=$brand_data['brand_id']; ?>">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Vehicle Brand</label><br>
                            <?=$brand_data['brand_name']; ?>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Icon</label><br>
                            <img src="<?=$brand_data['brand_logo']; ?>" width="300px"> 
                        </div>
                         <div class="form-group col-md-12"> 
                            <label for="exampleInputEmail1">Status</label><br>
                             <?php if($brand_data['brand_status']=='0'){echo 'Active';}?>
                             <?php if($brand_data['brand_status']=='1'){echo 'Inctive';}?>
                            
                          </div>
                    </div>
                    
                    
                 
                
                </div>
                <!-- /.card-body -->

                
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
  
?>