<?php 
$this->load->view('include/header.php');
$this->load->view('include/sidebar.php');
?>
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
         <div class="content-wrapper">
           <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Promocode</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Update
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
            <section id="floating-label-layouts simple-validation">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <!-- <div class="card-header">
                    <h4 class="card-title">promocode</h4>
                </div> -->
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" action="<?=base_url();?>promotions/edit_c" method="post" enctype="multipart/form-data" >
                                <input type="hidden" name="coupon_id" value="<?=$coupon_data['coupon_id'];?>">
                                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="exampleInputEmail1">Coupon Code</label>
                            <input name="coupon_code" type="text" class="form-control" id="exampleInputEmail1" value="<?=$coupon_data['coupon_code'];?>" placeholder="Enter Coupon Code">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleInputEmail1"> Expiry Date</label>
                            <input name="coupon_expiry" type="date" class="form-control" id="exampleInputEmail1" value="<?=$coupon_data['coupon_expiry'];?>" placeholder="Enter Coupon Expiry Date">
                        </div>    
                        
                    
                        <div class="form-group col-md-4" >
                            <label for="exampleInputEmail1"> Discount Amount</label>
                            <input name="coupon_amount" type="text" class="form-control"  id="amount_value" value="<?=$coupon_data['coupon_amount'];?>" placeholder="Enter Coupon Discount Amount">
                        </div>
                 
                       <div class="form-group col-md-4" >
                            <label for="exampleInputEmail1"> Discount (%)</label>
                            <input name="coupon_per" type="text" class="form-control"  id="per_value" value="<?=$coupon_data['coupon_per'];?>" placeholder="Enter Coupon Discount (%)">
                        </div>
                         <div class="form-group col-md-12" >
                            <label for="exampleInputEmail1"> Description</label>
                            <textarea name="coupon_desc" class="form-control"  placeholder="Enter Coupon Description"><?=$coupon_data['coupon_desc'];?></textarea>
                        </div>
                         <div class="form-group col-md-12" >
                            <label for="exampleInputEmail1">Status</label>
                            <select class="form-control" name="coupon_status">
                                <option value="0">Pending</option>
                                <option value="1">Expired</option>
                            </select>
                        
                        </div>
                    </div>    
                   </div>
                                    <div class="col-12">
                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Update</button>
                                    <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                    <a href="<?=base_url(); ?>/promocode" class="btn btn-outline-danger mr-1 mb-1">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- // Basic promocode Form section end -->
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->

 <?php
 $this->load->view('include/footer.php');
?> 