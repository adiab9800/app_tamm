<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
    <section class="content">
      <div class="row">
        <div class="col-12">
          

          <div class="card">
            <div class="card-header">
               <div class="col-md-9">
                     <h4 >All Promocode List</h4>
                  </div>
                 <div class="col-md-3">
                       <a href="<?=base_url();?>Promotions/Add"><button type="button" class="btn btn-block btn-primary ">Add Promocode</button></a>
                  </div>   
             
             </div>
            <!-- /.card-header -->
            <div class="card-body">
            <div class="table-responsive">
              
 <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Couponcode</th>
                  <th>Discount percentage</th>
                  <th>Expiry Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($coupon_data as $val_data){
                 
                 if($val_data['coupon_status'] == '1') {$status = "Pending";}else{$status = "Expired";}
                 
                 
                 if($val_data['coupon_for'] == '1') {$cou_for = "Less Amount on Product Price"; $cou_val = $val_data['coupon_amount'];}
                 elseif($val_data['coupon_for'] == '2') {$cou_for = "Less Amount on Shipping Price"; $cou_val = $val_data['coupon_amount'];}
                 elseif($val_data['coupon_for'] == '2') {$cou_for = "Free Shipping"; $cou_val = '0';}
                 else {$cou_for = "Buy 1 Get 1"; $cou_val = '0';}
                 
                 ?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><?=$val_data['coupon_code'];?></td>
                   <td><?=$val_data['coupon_per']?></td>
                   <td><?=$val_data['coupon_expiry'];?></td>
                   <td><?=$status;?></td>
                   <td>
                       <a href="<?= base_url();?>promotions/View/<?= $val_data['coupon_id'];?>"><i class="nav-icon fas fa-eye"></i></a>
                       <a href="<?= base_url();?>promotions/Edit/<?= $val_data['coupon_id'];?>"><i class="nav-icon fas fa-edit"></i></a>
                       <a onclick="return confirm('Are You Sure Delete Data?');" href="<?= base_url();?>promotions/Delete/<?= $val_data['coupon_id'];?>"><i class="nav-icon fas fa-trash"></i></a>
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Couponcode</th>
                  <th>Discount percentage</th>
                  <th>Expiry Date</th>
                   <th>Status</th>
                 
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
   
 <?php
 include __DIR__ . "./../include/footer.php";
?>      
<script>
 $(".delete-confirm").on("click", function (event) {
     var url = $(this).attr("href");
    
        event.preventDefault();
        swal({
        title: "Are you sure?",
        text: "The record will be deleted permanantly after 15 days. You can retrive it within 15 days.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
      }).then((result) => {
                if (result.value) {
                    window.location.href = url;
                }
            });
 });
 </script>