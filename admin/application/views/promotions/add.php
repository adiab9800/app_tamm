
<?php 
$this->load->view('include/header.php');
$this->load->view('include/sidebar.php');
?>
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
         <div class="content-wrapper">
           <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Promotions</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Add
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
            <section id="floating-label-layouts simple-validation">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <!-- <div class="card-header">
                    <h4 class="card-title">Customer</h4>
                </div> -->
                <div class="card-content">
                    <div class="card-body">
                       <form role="form" action="<?=base_url();?>promotions/Add_c" method="POST">
                  
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="exampleInputEmail1">Coupon Code</label>
                            <input name="coupon_code" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Coupon Code">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleInputEmail1">Coupon Expiry Date</label>
                            <input name="coupon_expiry" type="date" class="form-control" id="exampleInputEmail1" placeholder="Enter Coupon Expiry Date">
                        </div>    
                        
                    
                        <div class="form-group col-md-4" >
                            <label for="exampleInputEmail1">Coupon Discount Amount</label>
                            <input name="coupon_amount" type="text" class="form-control"  id="amount_value" placeholder="Enter Coupon Discount Amount">
                        </div>
                 
                       <div class="form-group col-md-4" >
                            <label for="exampleInputEmail1">Coupon Discount (%)</label>
                            <input name="coupon_per" type="text" class="form-control"  id="per_value" placeholder="Enter Coupon Discount (%)">
                        </div>
                        <div class="form-group col-md-12" >
                            <label for="exampleInputEmail1">Coupon Description</label>
                            <textarea name="coupon_desc" class="form-control"  placeholder="Enter Coupon Description"></textarea>
                        </div>
                    </div>    
                   </div>
                   
                
                    </div>
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Add</button>
                </div>
              </form>
           </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- // Basic Customer Form section end -->
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->

 <?php
 $this->load->view('include/footer.php');
?> 