<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">Add Service</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Services/Add_c" method="POST" enctype='multipart/form-data'>
                  
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Name</label>
                            <input name="cat_name" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Services Name" required>
                        </div>
                         <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Name In Arabic</label>
                            <input name="cat_arabic" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Services Name in Arabic" required>
                        </div>
                         <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Price</label>
                            <input name="service_price" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Services price" required>
                        </div>
                        
                        
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Image</label>
                            <input name="cat_image" type="file" class="form-control" id="exampleInputEmail1" placeholder="Choose Services Image" required>
                        </div>
                       
                         
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="add_category"  value="10" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 
