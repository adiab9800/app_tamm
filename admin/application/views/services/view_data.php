<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">View Serivce</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Services/Edit_c" method="POST" enctype='multipart/form-data'>
                  <input type="hidden" value="<?= $cat_data['cat_id'];?>" name="cat_id">
                <div class="card-body">
                    <div class="row">
                       
                        
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Name</label><br>
                           <?= $cat_data['cat_name'];?>
                        </div>
                         <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Name In Arabic</label><br>
                           <?= $cat_data['cat_arabic'];?>
                        </div>
                       
                       
                         <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Price</label><br>
                            <?= $cat_data['service_price'];?>
                        </div>
                        <input type="hidden" name="old_img" value="<?=$cat_data['cat_img'];?>"/>
                       
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Image</label><br>
                            <img src="<?=$cat_data['cat_img'];?>" style="height:100px;width:100px;">
                        </div>
                         
                    </div>
                    
                 
                
                </div>
               
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 