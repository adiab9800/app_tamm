<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">Edit Serivce</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Services/Edit_c" method="POST" enctype='multipart/form-data'>
                  <input type="hidden" value="<?= $cat_data['cat_id'];?>" name="cat_id">
                <div class="card-body">
                    <div class="row">
                        
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Name</label>
                            <input name="cat_name" type="text" class="form-control" id="exampleInputEmail1" value="<?= $cat_data['cat_name'];?>" placeholder="Enter Services Name">
                        </div>
                       
                       <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Name In Arabic</label>
                            <input name="cat_arabic" type="text" class="form-control" id="exampleInputEmail1" value="<?= $cat_data['cat_arabic'];?>" placeholder="Enter Services Name in arabic">
                        </div>
                       
                         <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Price</label>
                            <input name="service_price" type="text" class="form-control" id="exampleInputEmail1" value="<?= $cat_data['service_price'];?>" placeholder="Enter Services price" required>
                        </div>
                        <input type="hidden" name="old_img" value="<?=$cat_data['cat_img'];?>"/>
                       
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Services Image</label><br>
                            <img src="<?=$cat_data['cat_img'];?>" style="height:100px;width:100px;">
                            <input name="cat_image" type="file" class="form-control" id="exampleInputEmail1" placeholder="Choose Image">
                        </div>
                         
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 