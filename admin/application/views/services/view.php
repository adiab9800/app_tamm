<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>   <section class="content">
      <div class="row">
        <div class="col-12">
          

          <div class="card">
            <div class="card-header">
                 <div class="col-md-9">
                     <h4>All Services List</h4>
                  </div>
                 <div class="col-md-3">
                       <a href="<?=base_url();?>Services/Add"><button type="button" class="btn btn-block btn-primary ">Add Services</button></a>
                  </div>   
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Image</th>
                  <th>Services Name</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($cat_data as $val_data){?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><img src="<?=$val_data['cat_img'];?>" style="height:50px;width:50px;"></td>
                   <td><?=$val_data['cat_name'];?></td>
                   <td>
                       <a href="<?= base_url();?>Services/View/<?= $val_data['cat_slug'];?>"><i class="nav-icon fas fa-eye"></i></a>
                       <a href="<?= base_url();?>Services/Edit/<?= $val_data['cat_slug'];?>"><i class="nav-icon fas fa-edit"></i></a>
                       <a onclick="return confirm('Are You Sure Delete Data?');" href="<?= base_url();?>Services/Delete/<?= $val_data['cat_slug'];?>"><i class="nav-icon fas fa-trash"></i></a>
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Image</th>
                  <th>Services Name</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 