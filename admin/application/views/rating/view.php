<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
    <section class="content">
      <div class="row">
        <div class="col-12">
          

          <div class="card">
            <div class="card-header">
               <div class="col-md-9">
                     <h4 >All Rating List</h4>
                  </div>
              
             </div>
            <!-- /.card-header -->
            <div class="card-body">
            <div class="table-responsive">
                  <table id="example1" class="table table-bordered table-striped">
                <thead>
                 <tr>
                                                <th>#</th>
                                                <th>User</th>
                                                <th>Washer</th>
                                                <th>Booking ID</th>
                                                <th>Review</th>
                                                <th>Rating</th>
                                                <th>Rating On</th>
                                              
                                            </tr>
                </thead>
                <tbody>
                 <?php $sr_no = 0;
                 foreach($rating_data as $val_data){
                 $sr_no++;?>
                 <tr>
                   <td><?=$sr_no; ?></td>
                   <td><?=$val_data['user_fname'];?> <?=$val_data['user_lname'];?></td>
                   <td><?=$val_data['washer_fname'];?> <?=$val_data['washer_lname'];?></td>
                   <td style="color:blue" ><a href="<?= base_url();?>booking/View/<?= $val_data['order_id'];?>" class="btn btn-primary"><?=$val_data['order_id'];?></a></td>
                   <td><?=$val_data['rating_detail'];?></td>
                   <td><?=$val_data['rating'];?> <i class="fa fa-star"></i></td>
                   <td><?php echo date("d-m-Y H:i:s", strtotime($val_data['rating_date']));?></td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
               
              </table>
                     
 
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
   
 <?php
 include __DIR__ . "./../include/footer.php";
?>      
<script>
 $(".delete-confirm").on("click", function (event) {
     var url = $(this).attr("href");
    
        event.preventDefault();
        swal({
        title: "Are you sure?",
        text: "The record will be deleted permanantly after 15 days. You can retrive it within 15 days.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
      }).then((result) => {
                if (result.value) {
                    window.location.href = url;
                }
            });
 });
 </script>