<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">Edit Booking Status</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>booking_status/edit_c" method="POST" enctype='multipart/form-data'>
                  <input type="hidden" value="<?=$booking_data['booking_status_id']; ?>" name="booking_status_id">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Booking Status</label>
                            <input name="status" type="text" class="form-control" id="exampleInputEmail1" value="<?=$booking_data['status']; ?>" placeholder="Enter Booking Status">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Booking Status</label>
                            <input name="arabic_status" type="text" class="form-control" id="exampleInputEmail1" value="<?=$booking_data['arabic_status']; ?>" placeholder="Enter booking status in arabic">
                        </div>
                    </div>
                    
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="sub_ca"  class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
  
?>