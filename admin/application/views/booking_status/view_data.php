<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">View Booking Status</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>booking_status/edit_c" method="POST" enctype='multipart/form-data'>
                  <input type="hidden" value="<?=$booking_data['booking_status_id']; ?>" name="booking_status_id">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Booking Status:</label><br>
                            <?=$booking_data['status']; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Booking Status in arabic:</label><br>
                            <?=$booking_data['arabic_status']; ?>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->

               
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php 
  $this->load->view('include/footer');
  
?>