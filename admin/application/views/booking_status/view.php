<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
        
          <div class="card">
            <div class="card-header">
                  <div class="col-md-9">
                     <h4 >All Booking Status List</h4>
                  </div>
                 <div class="col-md-3">
                       <a href="<?=base_url();?>Booking_status/add"><button type="button" class="btn btn-block btn-primary ">Add Booking status</button></a>
                  </div>   
             
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                  <table id="tableExport" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Status</th>
                  <th>Arabic status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($booking_data as $val_data){?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><?=$val_data['status'];?></td>
                   <td><?=$val_data['arabic_status'];?></td>
                   <td>
                      <a href="<?= base_url();?>Booking_status/View/<?= $val_data['booking_status_id'];?>"><i class="nav-icon fa fa-eye"></i></a>
                       <a href="<?= base_url();?>Booking_status/edit/<?= $val_data['booking_status_id'];?>"><i class="nav-icon fas fa-edit"></i></a>
                       <a onclick="return confirm('Are You Sure Delete Data?');" href="<?= base_url();?>Booking_status/delete/<?= $val_data['booking_status_id'];?>"><i class="nav-icon fas fa-trash"></i></a>
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
               <tr>
                  <th>#</th>
                  <th>Status</th>
                  <th>Arabic status</th>
                  <th>Action</th>
                </tr>
               </tfoot>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php
  $this->load->view('include/footer');
?> 