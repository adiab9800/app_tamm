<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
               <h4>Add Slots</h4> 
            </div>
            <div class="card-body">
                    <form action="<?=base_url(); ?>slot/add_c" Method="POST">
                 <div class="row">
                  <div class="col-md-2">
                      <label>Date:</label>
                      <select class="form-control" name="slot_date">
                         <?php
                         for($i=1;$i<=31;$i++)
                         { ?>
                          <option value="<?=$i; ?>"><?=$i; ?></option>
                          <?php } ?>
                      </select>
                  </div>  
                  <div class="col-md-2">
                      <label>Month:</label>
                      <select class="form-control" name="slot_month">
                         <option value="January">January</option>
                         <option value="February">February</option>
                         <option value="March">March</option>
                         <option value="April">April</option>
                         <option value="May">May</option>
                         <option value="June">June</option>
                         <option value="July">July</option>
                         <option value="Auguest">Auguest</option>
                         <option value="September">September</option>
                         <option value="October">October</option>
                         <option value="November">November</option>
                         <option value="December">December</option>
                        
                      </select>
                  </div>
                  <div class="col-md-2">
                      <label>Year:</label>
                       <select class="form-control" name="slot_year">
                        <?php
                         for($i=2021;$i<=2030;$i++)
                         { ?>
                          <option value="<?=$i; ?>"><?=$i; ?></option>
                          <?php } ?>
                        </select>  
                  </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                         <label>Slots:</label>
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="9 AM -10 AM"> 9 AM -10 AM
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="10 AM -11 AM"> 10 AM -11 AM
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="11 AM -12 PM"> 11 AM -12 PM
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="12 PM -1 PM"> 12 PM -1 PM
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="1 PM -2 PM"> 1 PM -2 PM
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="2 PM -3 AM"> 2 PM -3 PM
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="3 PM -4 PM"> 3 PM -4 PM
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="4 PM -5 PM"> 4 PM -5 PM
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="5 PM -6 PM"> 5 PM -6 AM
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="6 PM -7 PM"> 6 PM -7 PM
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="7 PM -8 PM"> 7 PM -8 PM
                      </div>
                      <div class="col-md-3">
                          <input type="checkbox" class="form-controll" name="slot_time[]" value="8 PM -9 PM"> 8 PM -9 PM
                      </div>
                          </div>
                  <div class="col-md-3">
                      <button class="btn btn-primary" style="margin-top:30px">Submit</button>
                  </div>
                  </form>
                </div>
            </div>
          </div>
        </div>
    </section>   
    <section class="content">
      <div class="row">
        <div class="col-12">
          
        
          <div class="card">
            <div class="card-header">
                  <div class="col-md-9">
                     <h4>All Slots</h4>
                  </div>
               
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                  <table id="tableExport" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Date</th>
                  <th>Slot</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($booking_data as $val_data){?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><?=$val_data['slot_date'];?>-<?=$val_data['slot_month'];?>-<?=$val_data['slot_year'];?></td>
                   <td><?=$val_data['slot_time'];?></td>
                   <td><a onclick="return confirm('Are You Sure?');" href="<?= base_url();?>Slot/delete/<?= $val_data['slot_id'];?>"><i class="nav-icon fas fa-trash"></i></a></td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
               <tr>
                   <th>#</th>
                  <th>Date</th>
                  <th>Slot</th>
                  <th>Action</th>
               </tr>
               </tfoot>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php
  $this->load->view('include/footer');
?> 