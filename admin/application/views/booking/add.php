<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Booking</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Booking</a></li>
              <li class="breadcrumb-item active">Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Data</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>UserDetail/Add_c" method="POST">
                 
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">First Name</label>
                            <input name="user_fname" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter First Name" >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Last Name</label>
                            <input  name="user_lname" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Last Name" >
                        </div>
                       
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Email</label>
                            <input name="user_email" type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Phone No</label>
                            <input name="user_phoneno" type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter Phone No" >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Password</label>
                            <input name="user_password" type="password" class="form-control" id="exampleInputEmail1" placeholder="Enter Password" >
                        </div>
                         
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 