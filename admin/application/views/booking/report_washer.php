<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
    <section class="content">
          <form action="#" method="POST">
      
      <div class="row">
            <div class="col-6">
            <label>Washer:</label>
            <select class="form-control" name="washer_id">
                <?php 
                    $this->db->order_by('user_fname','asc');
                    $wahser_list=$this->db->get("tbl_washer")->result_array();
                    foreach($wahser_list as $wahser_list)
                    {
                    ?>
                    <option value="<?=$wahser_list['user_id'] ?>"><?=$wahser_list['user_fname'] ?> - <?=$wahser_list['user_lname'] ?></option>
                <?php } ?>
            </select>      
            <input type="submit" class="btn btn-primary" value="submit">
        </div>
     
      </div>
          </form>
     
      <div class="row">
        <div class="col-12">
          

          <div class="card">
            <div class="card-header">
               <div class="col-md-9">
                     <h4><?=$report_name;?> Booking List</h4>
                  </div>
                 <div class="col-md-3">
                      <!-- <a href="<?=base_url();?>User/Add"><button type="button" class="btn btn-block btn-primary ">Add User</button></a>-->
                  </div>   
             
             </div>
            <!-- /.card-header -->
            <div class="card-body">
                 <div class="table-responsive">
                   <div class="card-body">
                      <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Customer</th>
                  <th>Package</th>
                  <th>Vehicle</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($booking as $val_data){
                 
                 if($val_data['booking_status']=='Pending')
                 {
                 $booking_status_div='danger';}
                 else{
                      $booking_status_div='primary'; } ?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><?=$val_data['user_fname'];?>&nbsp;<?=$val_data['user_lname'];?></td>
                   
                   <td><?=$val_data['package_name'];?></td>
                   <td><?=$val_data['vhl_name'];?></td>
                   <td>₹ <?=$val_data['total_amount'];?></td>
                   <td><button class="btn btn-<?=$booking_status_div; ?>"> <?=$val_data['booking_status'];?></button></td>
                   <td>
                       <a href="<?= base_url();?>Booking/Edit/<?= $val_data['booking_id'];?>"><i class="nav-icon fas fa-edit"></i></a>
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
               <tr>
                  <th>#</th>
                  <th>Customer</th>
                  <th>Package</th>
                  <th>Vehicle</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                 </tfoot>
              </table>
                 </div>
             </div>
        </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 