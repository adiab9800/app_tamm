<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
    <section class="content">
      <div class="row">
        <div class="col-12">
          

          <div class="card">
            <div class="card-header">
               <div class="col-md-9">
                     <h4 >All Booking List</h4>
                  </div>
                 <div class="col-md-3">
                      <!-- <a href="<?=base_url();?>User/Add"><button type="button" class="btn btn-block btn-primary ">Add User</button></a>-->
                  </div>   
             
             </div>
            <!-- /.card-header -->
            <div class="card-body">
                 
                  <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Customer</th>
                  <th>Vehicle</th>
                  <th>Amount</th>
                  <th>Payment Mode</th>
                  <th>Washer</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($booking as $val_data){
                 
                 if($val_data['booking_status']=='Pending')
                 {
                 $booking_status_div='danger';}
                 else{
                      $booking_status_div='primary'; } ?>
                      
                        <div class="modal fade text-left" id="inlineForm_<?=$sr_no?>" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModal">Washer</h4>
                            <button type="button"  onlcick="closemodel()" class="close close_model" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="<?=base_url(); ?>booking/update/<?=$val_data['booking_id'];?>">
                             <input type="hidden" name="booking_id" id="booking_id" value="<?=$val_data['booking_id'];?>">
                              
                            <div class="modal-body">
                               
                                <label>Assign To* </label>
                                <div class="form-group">
                                    <select name="washer_id" id="washer_id" placeholder="Assign To" class="form-control">
                                      <?php
                                        foreach($washer_data as $data)
                                        {
                                      ?>
                                        <option value="<?=$data['user_id']?>" <?php if($val_data['washer_id']==$data['user_id'])  {echo 'selected';} ?>><?=$data['user_fname']." ".$data['user_lname']?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                
                                
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" >Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
                 
                 <tr>
                   <td><?=$val_data['booking_id'];?></td>
                   <td><?=$val_data['user_fname'];?>&nbsp;<?=$val_data['user_lname'];?></td>
                   <td><?=$val_data['vhl_no'];?></td>
                   <td>₹ <?=$val_data['total_amount'];?></td>
                   <td><?=$val_data['payment_mode'];?></td>
                   <td><?=$val_data['washer_fname'];?> <?=$val_data['washer_lname'];?></td>
                   <td><button class="btn btn-<?=$booking_status_div; ?>"> <?=$val_data['booking_status'];?></button></td>
                   <td>
                       <a href="<?= base_url();?>Booking/View/<?= $val_data['booking_id'];?>"><i class="nav-icon fa fa-eye"></i></a>
                       <a href="<?= base_url();?>Booking/Edit/<?= $val_data['booking_id'];?>"><i class="nav-icon fas fa-edit"></i></a>
                       <a data-toggle="modal" data-target="#inlineForm_<?=$sr_no?>" ><i class="nav-icon fas fa-user" data-toggle="tooltip" data-placement="top"
                      title="Assign Washer"></i></a>
                  
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
               <tr>
                   <th>#</th>
                  <th>Customer</th>
                  <th>Vehicle</th>
                  <th>Amount</th>
                  <th>Payment Mode</th>
                  <th>Washer</th>
                  
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                 </tfoot>
              </table>
                 </div>
             </div>
        </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
    </section>
     <?php
 include __DIR__ . "./../include/footer.php";
?> 