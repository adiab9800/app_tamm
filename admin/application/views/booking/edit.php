<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4>Edit Booking</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Booking/Edit_c" method="POST">
                  <input type="hidden" value="<?= $booking_data['booking_id'];?>" name="booking_id">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Customer</label>
                            <select class="form-control" name="user_id">
                            <?php foreach($user_data as $value)
                            { ?>
                            <option value="<?=$value['user_id']; ?>" <?php if($value['user_id']==$booking_data['user_id']){ echo 'Selected'; } ?>><?=$value['user_fname']; ?> <?=$value['user_lname']; ?> (<?=$value['user_phno']; ?>)</option>
                            <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Washer</label>
                            <select class="form-control" name="washer_id">
                            <?php foreach($washer_data as $value)
                            { ?>
                            <option value="<?=$value['user_id']; ?>" <?php if($value['user_id']==$booking_data['washer_id']){ echo 'Selected'; } ?>><?=$value['user_fname']; ?> <?=$value['user_lname']; ?> (<?=$value['user_phno']; ?>)</option>
                            <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Vehicle</label>
                            <select class="form-control" name="vehicle_id">
                            <?php foreach($vehicle_data as $value)
                            { ?>
                            <option value="<?=$value['vhl_id']; ?>" <?php if($value['vhl_id']==$booking_data['vehicle_id']){ echo 'Selected'; } ?>><?=$value['vhl_no']; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                       
                       <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Service</label>
                            <select class="form-control" name="service_id">
                            <?php
                             $service_mode=$this->db->get_where('tbl_category',array('cat_delete'=>'0'))->result_array();
                            foreach($service_mode as $value)
                            { ?>
                            <option value="<?=$value['cat_id']; ?>" <?php if($value['cat_id']==$booking_data['cat_id']){ echo 'Selected'; } ?>><?=$value['cat_name']; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                     <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Service Center</label>
                            <select class="form-control" name="service_center_id">
                            <?php
                            
                            $payment_mode=$this->db->get_where('tbl_service_center',array('service_center_status'=>'0'))->result_array();
                            foreach($package_data as $value)
                            { ?>
                            <option value="<?=$value['package_id']; ?>" <?php if($value['package_id']==$booking_data['subpackage_id']){ echo 'Selected'; } ?>><?=$value['package_name']; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                     
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Service Date</label>
                            <input name="service_date" type="date" class="form-control" id="exampleInputEmail1" placeholder="Enter Date" value="<?= $booking_data['service_date'];?>">
                        </div>
                          <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Service Time</label>
                            <input name="service_time" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Time" value="<?= $booking_data['service_time'];?>">
                        </div>
                        
                          <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Total Amount</label>
                            <input name="total_amount" type="number" class="form-control" id="exampleInputEmail1" placeholder="Enter Amount" value="<?= $booking_data['total_amount'];?>">
                        </div>
                          <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Payment Mode</label>
                             <select class="form-control" name="payment_mode">
                               <?php
                               $payment_mode=$this->db->get('tbl_payment_mode')->result_array();
                                foreach($payment_mode as $value)
                               { ?>
                            <option value="<?=$value['status']; ?>" <?php if($value['status']==$booking_data['payment_mode']){ echo 'Selected'; } ?>><?=$value['status']; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                          <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Status</label>
                            <select class="form-control" name="booking_status">
                              <?php
                               $booking_status=$this->db->get('tbl_booking_status')->result_array();
                                foreach($booking_status as $value)
                               { ?>
                            <option value="<?=$value['status']; ?>" <?php if($value['status']==$booking_data['booking_status']){ echo 'Selected'; } ?>><?=$value['status']; ?></option>
                            <?php } ?>    </select>
                        </div>
                           <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Note</label>
                                <input name="booking_desc" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Note" value="<?= $booking_data['booking_desc'];?>">
                        </div>
                     
                         
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 