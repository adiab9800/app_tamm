<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4>View Booking</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Booking/Edit_c" method="POST">
                  <input type="hidden" value="<?= $booking_data['booking_id'];?>" name="booking_id">
                <div class="card-body">
                    <div class="row" >
                    <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Vehicle No</label><br>
                                  <?=$booking_data['vhl_no']; ?> 
                     
                            </select>
                        </div>
                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Customer</label><br>
                            <?=$booking_data['user_fname']; ?> <?=$booking_data['user_lname']; ?>
                        </div>
                    
                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Customer Number</label><br>
                            <?=$booking_data['user_phno']; ?> 
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Washer</label><br>
                                 <?=$booking_data['washer_fname']; ?> <?=$booking_data['washer_lname']; ?>
                       </div>
                          <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Washer Number</label><br>
                            <?=$booking_data['washer_phno']; ?> 
                        </div>
                        
                       
                       <!-- <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Service Center</label><br>
                           <?=$booking_data['service_center_name']; ?>
                        </div>-->
                     <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Service </label><br>
                           <?=$booking_data['cat_name']; ?>
                        </div>
                     
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Service Date</label><bR>
                           <?= $booking_data['service_date'];?>
                        </div>
                          <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Service Time</label><br>
                            <?= $booking_data['service_time'];?>
                        </div>
                        
                          <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Total Amount</label><br>
                           AED <?= $booking_data['total_amount'];?>
                        </div>
                          <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Payment Mode</label><br>
                              <?= $booking_data['payment_mode'];?>
                        
                        </div>
                          <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Status</label><br>
                              <?= $booking_data['booking_status'];?>
                        
                        </div>
                           <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Note</label><br>
                              <?= $booking_data['booking_desc'];?>
                        </div>
                     
                         
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

               
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 