<?php 
  $this->load->view('include/header');
  $this->load->view('include/sidebar');
?>

  <style id="compiled-css" type="text/css">
      body {         
        font:14px sans-serif;           
      } 
      input {
          margin: 0.6em 0.6em 0; 
          width:398px;
      }
      #map_canvas {         
          height: 200px;         
          width: 400px;         
          margin: 0.6em;       
      }
          /* EOS */
  </style>
  
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
              <div class="col-md-9">
              <h4>Select Location to allow</h4>
              <script src="https://maps.google.com/maps/api/js?libraries=places&region=uk&language=ar&sensor=false&key=AIzaSyDQ69wZR1GPEeLAxyu-vkSSo_dzpZTOV2c"></script>
              <form action="<?=base_url(); ?>home/new_map_c" method="post">
                        <div class="row">
                            <div class="col-md-10">
                                  <input id="searchTextField" name="area" class="form-control" type="text" size="500">
                                  <input id="short_name" name="name" class="form-control" type="hidden" size="500">                  
                            </div>
                          <div class="col-md-10">
                            </div>
                          
                            <input disabled id="submit_btn" type="submit" name="submit" class="btn btn-primary col-md-1" value="Submit">                  
                            
                          
                        </div>
              </form>
              <div id="map_canvas"></div>    
              </div>
            </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>#</th>
                <th>Area</th>
                <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $sr_no = "1";
                $notification_data=$this->db->get('tbl_new_map')->result_array();
                foreach($notification_data as $val_data){?>
                <tr>
                  <td><?=$sr_no;?></td>
                  <td><?=$val_data['area'];?></td>
                  <td><a href="<?=base_url();?>home/delete_new_map/<?=$val_data['new_map_id']; ?>" ><button class="btn btn-danger">Delete</button></a></td>
                
                </tr>
              <?php $sr_no++;}?>
              </tbody>
              <tfoot>
              <tr>
                <th>#</th>
                <th>Area</th>
                <th>Action</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
 <?php 
  $this->load->view('include/footer');
?>
<script type="text/javascript">
  $(function () {
      var e = new google.maps.LatLng(-33.8688, 151.2195),
          o = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png",
          n = { center: new google.maps.LatLng(-33.8688, 151.2195), zoom: 13, mapTypeId: google.maps.MapTypeId.ROADMAP },
          t = new google.maps.Map(document.getElementById("map_canvas"), n),
          a = new google.maps.Marker({ position: e, map: t, icon: o }),
          g = document.getElementById("searchTextField"),
          s = new google.maps.places.Autocomplete(g, { types: ["geocode"] });
      s.bindTo("bounds", t);
      var c = new google.maps.InfoWindow();
      function m(e, n) {
          a.setIcon(o), a.setPosition(n), c.setContent(e), c.open(t, a);
      }
      google.maps.event.addListener(s, "place_changed", function () {
        c.close();
        var e = s.getPlace();
        console.log(e);
        $("#short_name").val(e.name);
        e.geometry.viewport ? t.fitBounds(e.geometry.viewport) : (t.setCenter(e.geometry.location), t.setZoom(17)), m(e.name, e.geometry.location);
        $('#submit_btn').removeAttr('disabled');
      }),
          $("#searchTextField").focusin(function () {
              $(document).keypress(function (e) {
                  if (13 == e.which) {
                      c.close();
                      var o = $(".pac-container .pac-item:first").text();
                      new google.maps.Geocoder().geocode({ address: o }, function (e, n) {
                          if (n == google.maps.GeocoderStatus.OK) {
                              var t = e[0].geometry.location.lat(),
                                  a = e[0].geometry.location.lng();
                              m(e[0].address_components[0].long_name, new google.maps.LatLng(t, a)), $("#searchTextField").val(o);
                          }
                      });
                  }
              });
          });
  });

</script>

  
             

