<?php 
include "include/header.php";
include "include/sidebar.php";
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Banner</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Banner Detail</a></li>
           
        </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= base_url();?>Banner/add_c" method="POST" enctype="multipart/form-data">
                  
                <div class="form-row">

                                           
                                            
                                            
                                            <div class="form-group col-md-12">
                                                <label>Choose Banner Image</label>
                                                <input class="form-control" type="file" name="picture" />
                                            </div>
                                            
                                        </div>                                 
                                            <fieldset class="form-group">
                                                <div class="row">
                                                    <label class="col-form-label col-sm-2 pt-0">Status</label>
                                                    <div class="col-sm-10">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="slider_status" value="0" checked="checked">
                                                            <label class="form-check-label">Active</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="slider_status" value="1">
                                                            <label class="form-check-label">Inactive</label>
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                            </fieldset>
                                           
                                           
                                           
                                        <div class="col-sm-10">
                                                <button type="submit" class="btn btn-dark">Add Banner</button>
                                            </div>
                                    </form>

              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include "include/footer.php";
?> 


