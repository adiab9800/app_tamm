<?php 
include "include/header.php";
include "include/sidebar.php";
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Term & Condition</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Tax</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Tax/Edit_c" method="POST">
                  
                <div class="card-body">
                  
                    <div class="row"  >
                       
                       
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">GST Rate</label>
                            <input type="text" name="tax_rate" class="form-control" placeholder="Enter GST" required value="<?=$tax_data['tax_rate'];?>">
                        </div>
                       
                        <fieldset class="form-group"><label class="col-form-label ">Discount on GST</label><br>
                                                <div class="row">
                                                    
                                                    <div class="col-sm-10">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="gst_disc" value="1" <?php if($tax_data['tax_discount'] =='1' ){echo "checked";}?> >
                                                            <label class="form-check-label">Yes</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="gst_disc" value="2" <?php if($tax_data['tax_discount'] =='2' ){echo "checked";}?> >
                                                            <label class="form-check-label">No</label>
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                            </fieldset>
                       
                    </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" >Edit</button>
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include "include/footer.php";
?> 

<script>
  let addbutton = document.getElementById("addquestion");
  addbutton.addEventListener("click", function() {  
  let boxes = document.getElementById("boxes");
  let clone = boxes.firstElementChild.cloneNode(true);
  boxes.appendChild(clone);
});
</script>