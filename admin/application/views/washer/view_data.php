<?php 
include __DIR__ . "./../include/header.php";
include __DIR__ . "./../include/sidebar.php";
?>
  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h4 class="card-title">View User</h4>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>User/Edit_c" method="POST" enctype='multipart/form-data'>
                  <input type="hidden" value="<?= $user_data['user_id'];?>" name="user_id">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">First Name</label><br>
                           <?= $user_data['user_fname'];?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Last Name</label><br>
                            <?= $user_data['user_lname'];?>
                        </div>
                       
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Email</label><br>
                           <?= $user_data['user_email'];?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Phone No</label><br>
                            <?= $user_data['user_phno'];?>
                        </div>
                         
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Profile picture</label><br>
                            <img src="<?= $user_data['profile_pic'];?>" width="150px">
                        </div> 
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Country</label><br>
                                <?=$user_data['country_id'];?>
                            </select>
                        </div>
                         <div class="form-group col-md-12"> 
                            <label for="exampleInputEmail1">Status</label><br>
                                 <?php if($user_data['user_status']=='0') {echo 'Active';} ?>
                                 <?php if($user_data['user_status']=='1') {echo 'Inactive';} ?>
                          </select>
                          </div>
                    </div>
                    
                 
                
                </div>
              </form>
            </div>
           

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include __DIR__ . "./../include/footer.php";
?> 