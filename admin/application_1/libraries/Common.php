<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common {
	public $CI;
	function __construct() {
		$this ->CI =& get_instance();
        $this->CI->load->database();
	}
	
	public function web_setup()
	{
	    $qury_web = $this->CI->db->query("SELECT * FROM `tbl_websitesetup` WHERE `web_id`='1'");
        return $qury_web->row_array();
	}
	
	
	public function tag_data($tagid)
	{
	    $qury_cat = $this->CI->db->query("SELECT * FROM `tbl_tag` WHERE `tag_id`=$tagid");
        return $qury_cat->row_array();
	}
	
	
	public function user_login_data($user_id)
	{
	    $qury_cat = $this->CI->db->query("SELECT * FROM `tbl_user` WHERE `user_id`=$user_id");
        return $qury_cat->row_array();
	}
	
	public function category_show()
    {
        $qury_cat = $this->CI->db->query("SELECT * FROM `tbl_category` WHERE `cat_delete`='0'");
        return $qury_cat->result_array();
    }
	
	public function category_countshow($cat_id)
	{
	    $qury_cat = $this->CI->db->query("SELECT * FROM `tbl_subcategory` WHERE `cat_id`=$cat_id AND `subcat_delete`='0'");
        return $qury_cat->num_rows();
	}
	public function sub_category_show($cat_id)
    {
        
        
        $qury_subcat = $this->CI->db->query("SELECT * FROM `tbl_subcategory` WHERE `cat_id`=$cat_id AND `subcat_delete`='0'");
        return $qury_subcat->result_array();
    }
    
    
	public function sub_category_show_filter($cat_slug)
    {
        $query = $this->CI->db->query("SELECT * FROM `tbl_category` WHERE `cat_slug`='$cat_slug' AND `cat_delete`='0'");
        $result = $query->row_array();
        $cat_id = $result['cat_id'];
        
        $qury_subcat = $this->CI->db->query("SELECT * FROM `tbl_subcategory` WHERE `cat_id`=$cat_id AND `subcat_delete`='0'");
        return $qury_subcat->result_array();
    }
    
    public function sub_category_show_filter_2($cat_slug)
    {
        $query = $this->CI->db->query("SELECT * FROM `tbl_subcategory` WHERE `subcat_slug`='$cat_slug' AND `subcat_delete`='0'");
        $result = $query->row_array();
        $cat_id = $result['cat_id'];
        
        $qury_subcat = $this->CI->db->query("SELECT * FROM `tbl_subcategory` WHERE `cat_id`=$cat_id AND `subcat_delete`='0'");
        return $qury_subcat->result_array();
    }

    public function query_subcat_count($sub_catid)
    {
        $qury_subcat = $this->CI->db->query("SELECT * FROM `tbl_sub_subcategory` WHERE `ssubcat_subcatid`=$sub_catid AND `ssubcat_delete`='0'");
        return $qury_subcat->num_rows();
    }

	public function sub_sub_category_show($catid,$subcatid)
	{
	    $qury_sub_subcat = $this->CI->db->query("SELECT * FROM `tbl_sub_subcategory` WHERE `ssubcat_catid`=$catid AND `ssubcat_subcatid`=$subcatid AND `ssubcat_delete`='0'");
        return $qury_sub_subcat->result_array();
	}
	
	public function get_subcat_slug($ssubcat_id)
	{
	     $quryslug = $this->CI->db->query("SELECT * FROM `tbl_subcategory` WHERE `subcat_id`=$ssubcat_id");
         return $quryslug->row_array();
	}
	
		public function get_subsubcat_slug($ssubcat_id)
	{
	     $quryslug = $this->CI->db->query("SELECT * FROM `tbl_sub_subcategory` WHERE `ssubcat_id`=$ssubcat_id");
         return $quryslug->row_array();
	}
	
	public function sub_sub_category_show_filter($cat_data)
	{
	    $qury_sub_subcat = $this->CI->db->query("SELECT * FROM `tbl_subcategory` WHERE  `subcat_slug`='$cat_data' AND `subcat_delete`='0'")->row_array();
	    
	    $sub_cat = $qury_sub_subcat['subcat_id'];
	    $query = $this->CI->db->query("SELECT * FROM `tbl_sub_subcategory` WHERE  `ssubcat_subcatid`='$sub_cat' AND `ssubcat_delete`='0'");
        return $query->result_array();
	}
	
	
	public function sub_sub_category_show1($subcat_slug)
	{
	    $qury_sub_subcat = $this->CI->db->query("SELECT * FROM `tbl_sub_subcategory` WHERE  `ssubcat_subcatid`=$subcatid AND `ssubcat_delete`='0'");
        return $qury_sub_subcat->result_array();
	}
 
	public function brand_data()
    {
        $qury_cat = $this->CI->db->query("SELECT * FROM `tbl_brand` WHERE `brand_delete`='0'");
        return $qury_cat->result_array();
    }	
	
    public function merchant_data($mrnt_id)
    {
        $qury_mrnt = $this->CI->db->query("SELECT * FROM `tbl_merchant_register` WHERE `mrnt_id`=$mrnt_id");
        return $qury_mrnt->row_array();
    }
    
    public function get_discount($product_id,$product_qty)
    {
        $qury_mrnt = $this->CI->db->query("SELECT MAX(whole_qty) as whole_max,tbl_wholesell.* FROM `tbl_wholesell` WHERE `whole_prdtid`=$product_id AND `whole_qty`<=$product_qty");
        if($qury_mrnt->num_rows() >0)
        {
            $rslt = $qury_mrnt->row_array();
            return $rslt['whole_discount'];
        }
        else
        {
            return 0;
        }
        
        
    }
    
    public function cart_item_count($user_id)
    {
        $qury_mrnt = $this->CI->db->query("SELECT * FROM `tbl_cart` WHERE `cart_userid`=$user_id AND `cart_status`='0' ");
        return $qury_mrnt->num_rows();
    } 
	
	public function cart_item($user_id)
    {
        
        $qury_mrnt = $this->CI->db->query("SELECT * FROM tbl_cart LEFT JOIN tbl_product ON tbl_product.prod_id=tbl_cart.cart_prdtid WHERE `cart_userid`=$user_id AND `cart_status`='0' ");
        return $qury_mrnt->result_array();
    }
    
    public function wish_item_count($user_id)
    {
        $qury_mrnt = $this->CI->db->query("SELECT * FROM `tbl_wishlist` WHERE `wish_userid`=$user_id");
        return $qury_mrnt->num_rows();
    } 
	
	public function wish_item($user_id)
    {
        
        $qury_mrnt = $this->CI->db->query("SELECT * FROM tbl_wishlist LEFT  JOIN tbl_product ON tbl_product.prod_id=tbl_wishlist.wish_prdtid WHERE `wish_userid`=$user_id ");
        return $qury_mrnt->result_array();
    }

    public function get_mertn_total_order($mrnt_id)
    {
        $qury_mrnt = $this->CI->db->query("SELECT * FROM tbl_order LEFT JOIN tbl_product ON tbl_product.prod_id = tbl_order.product_id WHERE `prod_addby`=$mrnt_id");
        return $qury_mrnt->num_rows();
    }
    
    public function get_mertn_done_order($mrnt_id)
    {
        $qury_mrnt = $this->CI->db->query("SELECT * FROM tbl_order LEFT JOIN tbl_product ON tbl_product.prod_id = tbl_order.product_id WHERE `prod_addby`=$mrnt_id AND `order_status`='Done'");
        return $qury_mrnt->num_rows();
    }
    
    public function get_mertn_cancel_order($mrnt_id)
    {
        $qury_mrnt = $this->CI->db->query("SELECT * FROM tbl_order LEFT JOIN tbl_product ON tbl_product.prod_id = tbl_order.product_id WHERE `prod_addby`=$mrnt_id AND `order_delete`='1'");
        return $qury_mrnt->num_rows();
    }
    
    public function get_additional_charge($prod_id,$prod_size)
    {
        $qury_mrnt = $this->CI->db->query("SELECT * FROM tbl_product_attribute  WHERE `pattr_productid`=$prod_id AND `pattr_size`='$prod_size'");
        return $qury_mrnt->row_array();
    }
    
    public function slug_generate($title)
    {
        $qury_slug =  strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $title)));
        return $qury_slug;
    }
    
    
    public function check_availablity($add_by,$weight,$pincode)
    {
       $qury = $this->CI->db->query("SELECT * FROM tbl_pincode WHERE `pincode_zoneaddby`=$add_by AND `pincode_val`=$pincode"); 
      
       $query_rate = $this->CI->db->query("SELECT * FROM tbl_rate WHERE `rate_id`=$weight")->row_array(); 
       
        if($qury->num_rows() > 0)
	    {
	       if($add_by == '0')
	       {   
	         $query_admin = $this->CI->db->query("SELECT * FROM tbl_admin WHERE `admin_delete`='0'")->row_array();    
	           if($pincode == $query_admin['admin_pincode'])
	           {
	             return $query_rate['rate_local'];
	           }
	           else
	           {
	                $data_state = $qury->row_array();
	                if($data_state['pincode_zone'] == $query_admin['admin_zone'])
	                {
	                    return $query_rate['rate_zonal'];
	                }
	                else
	                {
	                    return $query_rate['rate_national'];
	                }
	           }
	       } 
	       else
	       {
	           $query_mrchnt = $this->CI->db->query("SELECT * FROM tbl_merchant_register WHERE `mrnt_id`=$add_by")->row_array();
	           if($pincode == $query_mrchnt['mrnt_strpincode'])
	           {
	               
	               return $query_rate['rate_local'];
	           }
	            else
	           { 
	                $data_state = $qury->row_array();
	                $mrchnt_data = $query_mrchnt['mrnt_strstate'];
	                $query_zone = $this->CI->db->query("SELECT * FROM tbl_zonestate WHERE `zonest_addby`=$add_by AND `zonest_state`= $mrchnt_data")->row_array();
	                
	                 if($data_state['pincode_zone'] == $query_zone['zone_id'])
	                {
	                    return $query_rate['rate_zonal'];
	                }
	                else
	                {
	                    return $query_rate['rate_national'];
	                }
	                
	           }     
	       }
	    }
	    else
	    {
	        return 0;
	    }
    }
		
}
?>