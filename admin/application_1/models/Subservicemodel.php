<?php

class Subservicemodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
     public function get_subserv_data()
    {
        $this->db->order_by('sub_servid','DESC');
                 $this->db->join('tbl_package','tbl_package.package_id = tbl_subservice.sub_packid','LEFT');
        $query = $this->db->get_where('tbl_subservice',array('sub_delete'=>'1'));
        return $query->result_array();
    }
  
    public function get_packing_data()
    {
        $this->db->order_by('package_id','ASC');
                 
        $query = $this->db->get_where('tbl_package',array('package_delete'=>'1'));
        return $query->result_array();
    }

    public function Add_loc($data_ca)
    {
        $query = $this->db->get_where('tbl_subservice',array('sub_name'=>$data_ca['sub_name'],'sub_packid'=>$data_ca['sub_packid'],'sub_delete '=>'1'));
        if($query->num_rows() == 0)
        {
           
            $this->db->insert('tbl_subservice',$data_ca);
            $messge = array('message_del' => 'Subservices Add Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Subservices Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    public function selected_subserv_data($sub_servid)
    {
        $query = $this->db->get_where('tbl_subservice',array('sub_servid'=>$sub_servid));
        return $query->row_array(); 
    }

    public function selected_service_data($serv_id)
    {
         $query = $this->db->get_where('tbl_subservice',array('sub_servid'=>$serv_id));
        return $query->row_array(); 
    }


    public function Edit_loc($data_ca,$sub_servid)
    {
        $this->db->where('sub_servid',$sub_servid);
        $query = $this->db->update('tbl_subservice',$data_ca);
        
        if($query)
        {
           
            
            $messge = array('message_del' => 'Subservices Edit Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Subservices Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    
    
    
    
  
    public function Delete($sub_servid)
    {
        $this->db->set('sub_delete','2');
        $this->db->where('sub_servid',$sub_servid);
        $this->db->update('tbl_subservice');
        $messge = array('message_del' => 'Subservices Delete Successfully','message_type' => 'Success');
        $this->session->set_flashdata('item', $messge);
        return 1;    
    }
    
    


    
}