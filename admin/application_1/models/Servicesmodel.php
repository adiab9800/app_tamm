<?php

class Servicesmodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
	
	public function vehicle_data()
	{
	    
        $query = $this->db->get('tbl_type');
        return $query->result_array();
	}
	
    public function get_cat_data()
    {
        $this->db->order_by('cat_id','DESC');
        $this->db->join('tbl_type','tbl_type.type_id = tbl_category.vehicle_type','LEFT');
        $query = $this->db->get_where('tbl_category',array('cat_delete'=>'0'));
        return $query->result_array();
    }

    public function selected_cat_data($cat_slu)
    {
        $query = $this->db->get_where('tbl_category',array('cat_slug'=>$cat_slu));
        return $query->row_array();
    }
    
    public function Add_c($data_ct)
    {
        $query = $this->db->get_where('tbl_category',array('vehicle_type'=>$data_ct['vehicle_type'],'cat_name'=>$data_ct['cat_name'],'cat_delete'=>'0'));
        if($query->num_rows() == 0)
        {
           
            $this->db->insert('tbl_category',$data_ct);
            $messge = array('message_del' => 'service Added successfully','message_type' => 'success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'data alredy Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    public function Edit_c($cat_id,$data_ct)
    {
       
            
            $this->db->where('cat_id',$cat_id);
          $query=  $this->db->update('tbl_category',$data_ct);
        if($query)
        {
            $messge = array('message_del' => 'Service Edited successfully','message_type' => 'success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'data already exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    
    
    
    
  
    public function Delete($cat_slug)
    {
        $this->db->set('cat_delete','1');
        $this->db->where('cat_slug',$cat_slug);
        $this->db->update('tbl_category');
        $messge = array('message_del' => 'Category Delete successfully','message_type' => 'success');
        $this->session->set_flashdata('item', $messge);
        return 1;    
    }

    
}