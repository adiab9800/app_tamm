<?php

class Washermodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
    public function get_user_data()
    {
        $this->db->order_by('user_id','DESC');
        $query = $this->db->get_where('tbl_washer',array('user_delete'=>'0'));
        return $query->result_array();
    }

    public function selected_user_data($user_id)
    {
        $query = $this->db->get_where('tbl_washer',array('user_id'=>$user_id,'user_delete'=>'0'));
        return $query->row_array();
    }
    
    public function Edit_c($user_id,$data)
    {
        $this->db->where('user_id',$user_id);
        $this->db->update('tbl_washer',$data);
    }
    
    public function Delete($user_id)
    {
        $this->db->set('user_delete','1');
        $this->db->where('user_id',$user_id);
        $this->db->update('tbl_washer');
        return;
    }

    
}