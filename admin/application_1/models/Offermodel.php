<?php

class Offermodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
	
	public function get_offer_data()
    {
        $this->db->order_by('offer_id','DESC');
        $query = $this->db->get_where('tbl_offer',array('offer_delete'=>'0'));
        return $query->result_array();
    }

	
    public function get_prdt_data()
    {
        $this->db->order_by('prod_id','DESC');
        $query = $this->db->get_where('tbl_product',array('prod_delete'=>'0'));
        return $query->result_array();
    }

   
    
    public function Add_c($data_ins)
    {
       
           
           $query = $this->db->insert('tbl_offer',$data_ins);
          
        if($query)
        {
            $messge = array('message_del' => 'Offer Add Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Offer Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    public function Edit_c($cat_id,$cat_name)
    {
        $query = $this->db->get_where('tbl_category',array('cat_name'=>$cat_name,'cat_delete'=>'0'));
        if($query->num_rows() == 0)
        {
            $this->db->set('cat_name',$cat_name);
            $this->db->where('cat_id',$cat_id);
            $this->db->update('tbl_category');
            $messge = array('message_del' => 'Category Edit Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Category Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    public function get_offerprdtdata($exp_date)
    {
        //$this->db->where('offer_st_date>=',$start_date);
        $this->db->where('offer_end_date<=',$exp_date);
        $query = $this->db->get('tbl_offer');
        if($query -> num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return 0;
        }
        
    }
    
    
    
  
    public function Delete($cat_id)
    {
        $this->db->set('cat_delete','1');
        $this->db->where('cat_id',$cat_id);
        $this->db->update('tbl_category');
        $messge = array('message_del' => 'Category Delete Successfully','message_type' => 'Success');
        $this->session->set_flashdata('item', $messge);
        return 1;    
    }

    
}