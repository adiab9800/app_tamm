<?php

class Packagemodel extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
	
	
	public function get_vehl_data()
	{
	    $query = $this->db->get('tbl_type');
        return $query->result_array();
	}
	
	public function get_service_bytype($vah_type)
	{
	    $query = $this->db->get_where('tbl_category',array('vehicle_type'=>$vah_type,'cat_delete'=>'0'));
        return $query->result_array();
	}
	
    public function get_package_data()
    {
        $this->db->order_by('package_id','DESC');
        $this->db->join('tbl_category', 'tbl_category.cat_id = tbl_package.package_service');
        $this->db->join('tbl_type', 'tbl_type.type_id = tbl_package.package_vehicle');
        $query = $this->db->get_where('tbl_package',array('package_delete'=>'1'));
        return $query->result_array();
    }

    public function selected_pack_data($package_slug)
    {
       
        $query = $this->db->get_where('tbl_package',array('package_slug'=>$package_slug));
        return $query->row_array();
    }
    
    public function get_data_byfield($field, $value)
    {
        $query = $this->db->get_where('tbl_sub_subcategory',array(''.$field.''=>$value));
        return $query->row_array();
    }
    
    public function Add_c($data_ct)
    {
        $query =  $this->db->insert('tbl_package',$data_ct);
        if($query)
        {
           
           
            $messge = array('message_del' => 'Packages Added Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Packages already Exist','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }
    
    public function edit_c($data_ct,$pack_id)
    {
                    $this->db->where('package_id',$pack_id);
            $query =$this->db->update('tbl_package',$data_ct);
        if($query)
        {
          
            
            $messge = array('message_del' => 'Package Edited Successfully','message_type' => 'Success');
            $this->session->set_flashdata('item', $messge);
        }
        else
        {
            $messge = array('message_del' => 'Something went wrong.please try again','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
        }
       
        return 1;
    }

    public function Delete($package_slug)
    {
        $this->db->set('package_delete','2');
        $this->db->where('package_slug',$package_slug);
        $this->db->update('tbl_package');
        $messge = array('message_del' => 'Package Deletes Successfully','message_type' => 'Success');
        $this->session->set_flashdata('item', $messge);
        return 1;    
    }

    public function subsubcat_bysubcat($subsubcat_id)
    {
        $query = $this->db->get_where('tbl_subcategory',array('cat_id'=>$subsubcat_id));
        return $query->result_array();
    }
    
    public function get_bysubsubcategory($subcategoryid)
    {
        $query = $this->db->get_where('tbl_sub_subcategory',array('ssubcat_subcatid'=>$subcategoryid));
        return $query->result_array();
    }

   public function subsubcat_bysubcat2($subsubcat_id)
    {
        $query = $this->db->get_where('tbl_sub_subcategory',array('ssubcat_id'=>$subsubcat_id));
        return $query->result_array();
    }
    
}