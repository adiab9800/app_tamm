<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privacy_Policy extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		
	}
	
	public function index()
	{
     
		if ($this->session->userdata('is_admin_login')) {
           
            $data['term_data'] = $this->db->get('tbl_policy')->row_array();
            $this->load->view('policy',$data);
        } 
        else
        {
            $messge = array('message_del' => 'Login Please...','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
               redirect('Home');
       	}
    }
    
    
    public function Edit_c()
	{
	    $data = array(
            'policy_msg' => $this->input->post('policy_msg')
            );
             $this->db->where('policy_id','1');
        $this->db->update('tbl_policy',$data);
        
        redirect('Privacy_Policy');
    } 
    
    
}
?>