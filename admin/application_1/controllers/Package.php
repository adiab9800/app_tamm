<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Package extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Servicesmodel');
        $this->load->model('Packagemodel');
	}
	
	public function index()
	{     
	
        $data['pack_data'] = $this->Packagemodel->get_package_data();
        $this->load->view('package/view',$data);
       
    }
    
    public function get_service_bytype($vah_type)
    {
         $data = $this->Packagemodel->get_service_bytype($vah_type);
       echo json_encode($data); 
    }
    
    public function Add()
	{
        $data['vhl_data'] = $this->Packagemodel->get_vehl_data();
        $this->load->view('package/add',$data);
    } 
    
    public function Add_subsubcat()
	{
	    if($this->input->post('su_sub_ca') == '50')
       {  
           if(!empty($_FILES['cat_image']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
               $currentTime = date( 'd-m-Y h:i:s A', time () );
               $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/packageimage/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['cat_image']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('cat_image'))
                {
                    $uploadData = $this->upload->data();
                    $cat_image = $uploadData['file_name'];
                    
                }
                else
                {
                    $cat_image = '';
                }
            }
      
     
      
      
       $cat_slu = $this->common->slug_generate($this->input->post('package_name'));
       
        $data_ct = array(
            'package_type' => $this->input->post('package_type'),
            'package_service' => $this->input->post('package_service'),
            'package_name' => $this->input->post('package_name'),
            'package_vehicle'=>$this->input->post('package_vehicle'),
            'package_slug' => $cat_slu,
            'package_about' => $this->input->post('package_about'),
            'package_price' => $this->input->post('package_price'),
            'package_image' => $cat_image,
            'package_delete'=>'1',
            );
       
        $this->Packagemodel->Add_c($data_ct);
       }
        redirect('Package');
    } 
    
     public function Edit($package_slug)
	{
	    $data['pack_data'] = $this->Packagemodel->selected_pack_data($package_slug);
	    $data['vhl_data'] = $this->Packagemodel->get_vehl_data();
        $data['cat_data'] = $this->Packagemodel->get_service_bytype($data['pack_data']['package_vehicle']);
        $this->load->view('package/edit',$data);
    } 
    public function View($package_slug)
	{
	    $data['pack_data'] = $this->Packagemodel->selected_pack_data($package_slug);
	    $data['vhl_data'] = $this->Packagemodel->get_vehl_data();
        $data['cat_data'] = $this->Packagemodel->get_service_bytype($data['pack_data']['package_vehicle']);
        $this->load->view('package/view_data',$data);
    } 
    
    public function Edit_pack()
	{
       $pack_id= $this->input->post('package_id');
       $old_img = $this->input->post('old_img');
       
       
       if($this->input->post('su_sub_pack') == '50'){
	        if(!empty($_FILES['cat_image']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
               $currentTime = date( 'd-m-Y h:i:s A', time () );
               $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/packageimage/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['cat_image']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('cat_image'))
                {
                    $uploadData = $this->upload->data();
                    $cat_image = $uploadData['file_name'];
                    
                }
                else
                {
                    $cat_image = $old_img;
                }
            }
            else
                {
                    $cat_image = $old_img;
                }
     
      
      
       $cat_slu = $this->common->slug_generate($this->input->post('package_name'));
       
        $data_ct = array(
            'package_type' => $this->input->post('package_type'),
            'package_service' => $this->input->post('package_service'),
            'package_name' => $this->input->post('package_name'),
            'package_vehicle'=>$this->input->post('package_vehicle'),
            'package_slug' => $cat_slu,
            'package_about' => $this->input->post('package_about'),
            'package_price' => $this->input->post('package_price'),
            'package_image' => $cat_image,
            'package_delete'=>'1',
            );
       
        $this->Packagemodel->edit_c($data_ct,$pack_id);
	    }
	    
        redirect('Package');
    } 
    
    public function Delete($package_slug)
    {
        $this->Packagemodel->Delete($package_slug);
        redirect('Package');
    }

    public function get_bysubcategory($subcategoryid) {
       
        $data = $this->Packagemodel->subsubcat_bysubcat($subcategoryid);
        echo json_encode($data); 
    }
    
    public function get_byPackagemodel($subcategoryid) {
       $data = $this->Packagemodel->get_byPackagemodel($subcategoryid);
       echo json_encode($data); 
    }    
}
?>