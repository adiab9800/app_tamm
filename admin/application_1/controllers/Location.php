<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Servicesmodel');
		$this->load->model('Locationmodel');
	}
	
	public function index()
	{
         $data['cat_data'] = $this->Servicesmodel->get_cat_data();
		$data['subcat_data'] = $this->Locationmodel->get_subcat_data();
        $this->load->view('location/view',$data);
        
    }
    public function Add()
	{
        $data['cat_data'] = $this->Servicesmodel->get_cat_data();
        $this->load->view('location/add',$data);
    } 
    
    public function Add_loc()
	{
       if($this->input->post('sub_ca') == '8')
       {
         
        $data_ca = array(
            'location_address'=> $this->input->post('location_address'),
            'location_state'=> $this->input->post('location_state'),
            'location_city'=> $this->input->post('location_city'),
            'location_wiseservice'=>implode(",",$this->input->post('service')),
            'location_status'=>$this->input->post('status_loc'),
            'location_delete'=>'1',
            );
          
       $this->Locationmodel->Add_loc($data_ca);
       }
       
        redirect('Location');
    } 
    
     public function Edit($location_id)
	{
        $data['cat_data'] = $this->Servicesmodel->get_cat_data();
	    $data['loc_data'] = $this->Locationmodel->selected_location_data($location_id);
        $this->load->view('location/edit',$data);
    } 
    
    public function Edit_loc()
	{
	    if($this->input->post('sub_ca') == '8')
       {
         $data_ca = array(
            'location_id'=> $this->input->post('location_id'),
            'location_address'=> $this->input->post('location_address'),
            'location_state'=> $this->input->post('location_state'),
            'location_city'=> $this->input->post('location_city'),
            'location_wiseservice'=>implode(",",$this->input->post('service')),
            'location_status'=>$this->input->post('status_loc'),
            'location_delete'=>'1',
            );
         $this->Locationmodel->Edit_loc($data_ca);
       }     
         redirect('Location');
    } 
    
    public function Delete($location_id)
    {
        $this->Locationmodel->Delete($location_id);
        redirect('Location');
    }
    
    public function get_bycategory() {
        $categoryid= $this->input->post('categoryid');
        $data = $this->Locationmodel->subcat_bycat($categoryid);
        echo json_encode($data); 
    }
}
?>