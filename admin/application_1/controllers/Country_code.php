<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country_code extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		
	    $this->load->library('upload');
	}
	
	public function index()
	{
         $data['countries_data'] = $this->db->get('tbl_countries')->result_array();
         $this->load->view('countries/view',$data);
    }
    public function Add()
	{
	     $this->load->view('countries/add',$data);
    } 
    
    public function Add_c()
	{  
       
       
          $array = array(
            'name' => $this->input->post('name'),
            'phonecode' => $this->input->post('phonecode'),
            );
            $result=$this->db->insert('tbl_countries',$array);
             if($result)
            {
                   $messge = array('message_del' => 'Data inserted successfully.','message_type' => 'success');
                   $this->session->set_flashdata('item', $messge);
            }
            else{
                   $messge = array('message_del' => 'data not inserted.Please try again.','message_type' => 'error');
                   $this->session->set_flashdata('item', $messge);
             
            }
       
      
      
      
        redirect('Country_code');
    } 
    
     public function Edit($id)
	{
    	 $this->db->where('id',$id);
	 	$data['countries_data'] = $this->db->get('tbl_countries')->row_array();
       
           $this->load->view('countries/edit',$data);
    } 
    
    public function View($id)
	{
    	 $this->db->where('id',$id);
	 	$data['countries_data'] = $this->db->get('tbl_countries')->row_array();
       
           $this->load->view('countries/view_data',$data);
    } 
    
    public function Edit_c()
	{
	    $id= $this->input->post('id');
        
         
         
           $array = array(
            'name' => $this->input->post('name'),
         'phonecode' => $this->input->post('phonecode'),
               );
           $this->db->where('id',$id);
            $result=$this->db->update('tbl_countries',$array);
            if($result)
            {
                   $messge = array('message_del' => 'Data updated successfully.','message_type' => 'success');
                   $this->session->set_flashdata('item', $messge);
            }
            else{
                   $messge = array('message_del' => 'data not updated.Please try again.','message_type' => 'error');
                   $this->session->set_flashdata('item', $messge);
             
            }
       
    redirect('Country_code');
    } 
    
    public function Delete($id)
    {   
        
        $this->db->where('id',$id);
        $result=$this->db->delete('tbl_countries');
        if($result)
        {
               $messge = array('message_del' => 'Data deleted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not deleted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('Country_code');
      
    
    }
}
?>