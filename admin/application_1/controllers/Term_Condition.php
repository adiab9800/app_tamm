<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Term_Condition extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		
	}
	
	public function index()
	{
     
		if ($this->session->userdata('is_admin_login')) {
           
            $data['term_data'] = $this->db->get('tbl_terms')->row_array();
            $this->load->view('term',$data);
        } 
        else
        {
            $messge = array('message_del' => 'Login Please...','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
               redirect('Home');
       	}
    }
    
    
    public function Edit_c()
	{
	      $data = array(
            'term_desc' => $this->input->post('term_desc')
            );
            $this->db->where('term_id','1');
        $this->db->update('tbl_terms',$data);
        redirect('Term_Condition');
    } 
    
    
}
?>