<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model('Homemodel');
	}
	
	public function index()
	{   
	    $date=date('Y-m-d');
		if ($this->session->userdata('is_admin_login')) {
		    $this->db->select('tbl_category.cat_name,tbl_service_center.service_center_name,tbl_vehicle.vhl_no,tbl_vehicle.vhl_name,tbl_vehicle.vhl_id,tbl_washer.user_id as washer_id,tbl_washer.user_fname as washer_fname,tbl_washer.user_lname as washer_lname,tbl_washer.user_id,tbl_user.user_fname,tbl_user.user_lname,tbl_booking.* ');
            $this->db->join('tbl_vehicle','tbl_booking.vehicle_id=tbl_vehicle.vhl_id','LEFT');
		    $this->db->join('tbl_category','tbl_booking.cat_id=tbl_category.cat_id','LEFT');
		    $this->db->join('tbl_service_center','tbl_booking.service_center_id=tbl_service_center.service_center_id','LEFT');
		    $this->db->join('tbl_washer','tbl_washer.user_id=tbl_booking.washer_id','LEFT');
		    $this->db->join('tbl_user','tbl_user.user_id=tbl_booking.user_id','LEFT');
		    $this->db->where('tbl_booking.service_date',$date);
		    $data['booking']=$this->db->get_where('tbl_booking')->result_array();
		    $this->load->view('dashboard',$data);
        } 
        else
        {
			$messge = array('message_del' => 'Login Please...','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
               redirect('Home');
       	}
    }


	

}
