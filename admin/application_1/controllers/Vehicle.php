<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Vehiclemodel');
		
	}
	
	public function index()
	{
     
		$data['subcat_data'] = $this->Vehiclemodel->get_vehicle_data();
        $this->load->view('vehicle/view',$data);
        
    }
    public function Add()
	{
        $data['cat_data'] = $this->Vehiclemodel->get_vehivle_type_data();
        $this->load->view('vehicle/add',$data);
    } 
    
    public function brand()
	{
     
		$data['brand_data'] = $this->db->get('tbl_brand')->result_array();
        $this->load->view('vehicle/brand/view',$data);
        
    }
    
    public function model()
	{
        $this->db->join('tbl_brand','tbl_brand.brand_id=tbl_model.brand_id');
		$data['model_data'] = $this->db->get('tbl_model')->result_array();
        $this->load->view('vehicle/model/view',$data);
        
    }
    
    public function type()
	{
     
		$data['type_data'] = $this->db->get('tbl_type')->result_array();
        $this->load->view('vehicle/type/view',$data);
        
    }
    
    
    public function add_brand()
	{
     
		$data['brand_data'] = $this->db->get('tbl_brand')->result_array();
        $this->load->view('vehicle/brand/add',$data);
        
    }
    
    public function add_model()
	{
     
		$data['model_data'] = $this->db->get('tbl_model')->result_array();
        $this->load->view('vehicle/model/add',$data);
        
    }
    
    public function add_type()
	{
	   
     	$data['type_data'] = $this->db->get('tbl_type')->result_array();
        $this->load->view('vehicle/type/add',$data);
       
    }
    
    public function add_brand_c()
	{   
         
		 if(!empty($_FILES['brand_logo']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
               $currentTime = date( 'd-m-Y h:i:s A', time () );
               $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['brand_logo']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('brand_logo'))
                {
                    $uploadData = $this->upload->data();
                    $brand_logo = base_url().'upload/'.$uploadData['file_name'];
                    
                }
                else
                {
                    $brand_logo = '';
                }
            }
	     $array=array('brand_name'=>$this->input->post('brand_name'),
	     'brand_status'=>$this->input->post('brand_status'),
        'brand_logo'=>$brand_logo);
        $result=$this->db->insert('tbl_brand',$array);
        if($result)
        {
               $messge = array('message_del' => 'Data inserted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not inserted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('Vehicle/brand');
       
        
    }
    
    public function add_model_c()
	{   
         
		 if(!empty($_FILES['model_logo']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
               $currentTime = date( 'd-m-Y h:i:s A', time () );
               $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['model_logo']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('model_logo'))
                {
                    $uploadData = $this->upload->data();
                    $model_logo = base_url().'upload/'.$uploadData['file_name'];
                    
                }
                else
                {
                    $model_logo = '';
                }
            }
	     $array=array('model_name'=>$this->input->post('model_name'),
         'brand_id'=>$this->input->post('brand_id'),
         'model_logo'=>$model_logo);
        $result=$this->db->insert('tbl_model',$array);
        if($result)
        {
               $messge = array('message_del' => 'Data inserted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not inserted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('Vehicle/model');
       
        
    }
    
    public function add_type_c()
	{
	      if(!empty($_FILES['type_logo']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
               $currentTime = date( 'd-m-Y h:i:s A', time () );
               $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['type_logo']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('type_logo'))
                {
                    $uploadData = $this->upload->data();
                    $type_logo = base_url().'upload/'.$uploadData['file_name'];
                    
                }
                else
                {
                    $type_logo = '';
                }
            }
	     $array=array('type_name'=>$this->input->post('type_name'),
        'type_logo'=>$type_logo);
        $result=$this->db->insert('tbl_type',$array);
        if($result)
        {
               $messge = array('message_del' => 'Data inserted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not inserted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('Vehicle/type');
       
    }
    
    
    public function edit_brand($id)
	{
        $this->db->where('brand_id',$id);
		$data['brand_data'] = $this->db->get('tbl_brand')->row_array();
        $this->load->view('vehicle/brand/edit',$data);
        
    }
     public function view_brand($id)
	{
        $this->db->where('brand_id',$id);
		$data['brand_data'] = $this->db->get('tbl_brand')->row_array();
        $this->load->view('vehicle/brand/view_data',$data);
        
    }
    
    public function edit_model($id)
	{
        $this->db->where('model_id',$id);
		$data['model_data'] = $this->db->get('tbl_model')->row_array();
        $this->load->view('vehicle/model/edit',$data);
        
    }
     public function view_model($id)
	{
        $this->db->where('model_id',$id);
		$data['model_data'] = $this->db->get('tbl_model')->row_array();
        $this->load->view('vehicle/model/view_data',$data);
        
    }
    public function edit_type($id)
	{
        $this->db->where('type_id',$id);
		$data['type_data'] = $this->db->get('tbl_type')->row_array();
		$this->load->view('vehicle/type/edit',$data);
        
    }
    
     public function view_type($id)
	{
        $this->db->where('type_id',$id);
		$data['type_data'] = $this->db->get('tbl_type')->row_array();
		$this->load->view('vehicle/type/view_data',$data);
        
    }
    
    
      public function edit_brand_c()
	{
     
		  $id=$this->input->post('brand_id');
	     $this->db->where('brand_id',$id);
		 $brand_data = $this->db->get('tbl_brand')->row_array();
	    
		 if(!empty($_FILES['brand_logo']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
               $currentTime = date( 'd-m-Y h:i:s A', time () );
               $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['brand_logo']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('brand_logo'))
                {
                    $uploadData = $this->upload->data();
                    $brand_logo = base_url().'upload/'.$uploadData['file_name'];
                    
                }
                else
                {
                    $brand_logo = $brand_data['brand_logo'];
                }
            }
            else
                {
                    $brand_logo = $brand_data['brand_logo'];
                }
	   
	   $array=array('brand_name'=>$this->input->post('brand_name'),
	   'brand_status'=>$this->input->post('brand_status'),
        'brand_logo'=>$brand_logo);
        $this->db->where('brand_id',$id);
        $result=$this->db->update('tbl_brand',$array);
        if($result)
        {
               $messge = array('message_del' => 'Data inserted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not inserted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('Vehicle/brand');
       
        
    }
    
    public function edit_model_c()
	{
         $id=$this->input->post('model_id');
	     $this->db->where('model_id',$id);
		 $brand_data = $this->db->get('tbl_model')->row_array();
	    
	if(!empty($_FILES['model_logo']['name']))
            {
                date_default_timezone_set('Asia/Kolkata');
               $currentTime = date( 'd-m-Y h:i:s A', time () );
               $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['model_logo']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('model_logo'))
                {
                    $uploadData = $this->upload->data();
                    $model_logo = base_url().'upload/'.$uploadData['file_name'];
                    
                }
                else
                {
                    $model_logo = $brand_data['model_logo'];
                }
	   
            }
            else
                {
                    $model_logo = $brand_data['model_logo'];
                }
	   
	     $array=array('model_name'=>$this->input->post('model_name'),
	     'brand_id'=>$this->input->post('brand_id'),
	     'model_status'=>$this->input->post('status'),
       
        'model_logo'=>$model_logo);
        
        $this->db->where('model_id',$id);
        $result=$this->db->update('tbl_model',$array);
        if($result)
        {
               $messge = array('message_del' => 'Data updated successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not updated.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('Vehicle/model');
       
        
        
    }
    
    public function edit_type_c()
	{    
	     $id=$this->input->post('type_id');
	     $this->db->where('type_id',$id);
		 $type_data = $this->db->get('tbl_type')->row_array();
	     if(!empty($_FILES['type_logo']['name']))
         {
                date_default_timezone_set('Asia/Kolkata');
                $currentTime = date( 'd-m-Y h:i:s A', time () );
                $date_string = strtotime($currentTime);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $date_string.$_FILES['type_logo']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('type_logo'))
                {
                    $uploadData = $this->upload->data();
                    $type_logo = base_url().'upload/'.$uploadData['file_name'];
                    
                }
                else
                {
                    $type_logo = $type_data['type_logo'];
                }
            }
             else
                {
                    $type_logo = $type_data['type_logo'];
                }
	     $array=array('type_name'=>$this->input->post('type_name'),
        'type_logo'=>$type_logo,
         'type_status'=>$this->input->post('status'),
        );
        $this->db->where('type_id',$id);
        $result=$this->db->update('tbl_type',$array);
        if($result)
        {
               $messge = array('message_del' => 'Data updated successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not updated.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('Vehicle/type');
      
    }
    
    
   public function delete_type($id)
	{    
         $this->db->where('type_id',$id);
        $result=$this->db->delete('tbl_type');
        if($result)
        {
               $messge = array('message_del' => 'Data deleted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not deleted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('Vehicle/type');
      
    
    }
    
    public function delete_brand($id)
	{    
         $this->db->where('brand_id',$id);
        $result=$this->db->delete('tbl_brand');
        if($result)
        {
               $messge = array('message_del' => 'Data deleted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not deleted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
         redirect('Vehicle/brand');
      
    
    }
    
    public function Add_vhl()
	{
       if($this->input->post('sub_ca') == '8')
       {
         $subcat_slug = $this->common->slug_generate($this->input->post('vhl_no'));
       
         $data_su_ca = array(
            'brand_id'=> $this->input->post('brand_id'),
            'model_id'=> $this->input->post('model_id'),
            'type_id'=> $this->input->post('type_id'),
            'vhl_slug'=>$subcat_slug,
            'vhl_no'=> $this->input->post('vhl_no'),
            'user_id'=> $this->input->post('user_id'),
            'vhl_delete'=>'1'
            );
       
        $result=$this->db->insert('tbl_vehicle',$data_su_ca);
        if($result)
        {
               $messge = array('message_del' => 'Data inserted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not inserted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        } 
           
       }
       
        redirect('Vehicle');
    } 
    
     public function Edit($vhl_slug)
	{
        $data['cat_data'] = $this->Vehiclemodel->get_vehivle_type_data();;
	    $data['vhl_data'] = $this->Vehiclemodel->selected_vhl_data($vhl_slug);
        $this->load->view('vehicle/edit',$data);
    } 
    
     public function View($vhl_slug)
	{
        $data['cat_data'] = $this->Vehiclemodel->get_vehivle_type_data();;
	    $data['vhl_data'] = $this->Vehiclemodel->selected_vhl_data($vhl_slug);
        $this->load->view('vehicle/view_data',$data);
    } 
    
    public function Edit_subcat()
	{
	    if($this->input->post('sub_c') == '14')
       {
         $subcat_slug = $this->common->slug_generate($this->input->post('vhl_name'));
       
         $data_s_cat = array(
               'brand_id'=> $this->input->post('brand_id'),
            'model_id'=> $this->input->post('model_id'),
            'type_id'=> $this->input->post('type_id'),
            'vhl_slug'=>$subcat_slug,
            'vhl_no'=> $this->input->post('vhl_no'),
            'user_id'=> $this->input->post('user_id'),
         );
          $this->db->where('vhl_id',$this->input->post('vhl_id'));
          $result= $this->db->update('tbl_vehicle',$data_s_cat);
         if($result)
        {
               $messge = array('message_del' => 'Data updated successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not updated.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
           
       }     
         redirect('Vehicle');
    } 
    
    public function Delete($id)
    {
        $this->db->where('vhl_id',$id);
        $result=$this->db->delete('tbl_vehicle');
     if($result)
        {
               $messge = array('message_del' => 'Data deleted successfully.','message_type' => 'success');
               $this->session->set_flashdata('item', $messge);
        }
        else{
               $messge = array('message_del' => 'data not deleted.Please try again.','message_type' => 'error');
               $this->session->set_flashdata('item', $messge);
         
        }
               redirect('Vehicle');
  }
    
    public function get_bycategory() {
        $categoryid= $this->input->post('categoryid');
        $data = $this->Vehiclemodel->subcat_bycat($categoryid);
        echo json_encode($data); 
    }
}
?>