<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
	    $date=date('Y-m-d');
      if ($this->session->userdata('is_admin_login')) {
		    $this->db->select('tbl_vehicle.vhl_name,tbl_vehicle.vhl_id,tbl_package.package_id,tbl_package.package_name,tbl_washer.user_id as washer_id,tbl_washer.user_fname as washer_fname,tbl_washer.user_lname as washer_lname,tbl_washer.user_id,tbl_user.user_fname,tbl_user.user_lname,tbl_booking.* ');
		    $this->db->join('tbl_vehicle','tbl_booking.vehicle_id=tbl_vehicle.vhl_id','LEFT');
		    $this->db->join('tbl_package','tbl_booking.subpackage_id=tbl_package.package_id','LEFT');
		    $this->db->join('tbl_user','tbl_user.user_id=tbl_booking.user_id','LEFT');
		    $this->db->join('tbl_washer','tbl_washer.user_id=tbl_booking.washer_id','LEFT');
		    $this->db->where('tbl_booking.service_date',$date);
		    $data['booking']=$this->db->get_where('tbl_booking')->result_array();
		    $data['report_name']="Today's ";
		    $this->load->view('booking/report',$data);
        } 
        else
        {
			$messge = array('message_del' => 'Login Required.','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
               redirect('Home');
       	}
        
    }
	public function total()
	{
	  if ($this->session->userdata('is_admin_login')) {
		    $this->db->select('tbl_vehicle.vhl_name,tbl_vehicle.vhl_id,tbl_package.package_id,tbl_package.package_name,tbl_washer.user_id as washer_id,tbl_washer.user_fname as washer_fname,tbl_washer.user_lname as washer_lname,tbl_washer.user_id,tbl_user.user_fname,tbl_user.user_lname,tbl_booking.* ');
		    $this->db->join('tbl_vehicle','tbl_booking.vehicle_id=tbl_vehicle.vhl_id','LEFT');
		    $this->db->join('tbl_package','tbl_booking.subpackage_id=tbl_package.package_id','LEFT');
		    $this->db->join('tbl_user','tbl_user.user_id=tbl_booking.user_id','LEFT');
		    $this->db->join('tbl_washer','tbl_washer.user_id=tbl_booking.washer_id','LEFT');
		    $data['booking']=$this->db->get_where('tbl_booking')->result_array();
		    $data['report_name']="Total ";
		    $this->load->view('booking/report',$data);
        } 
        else
        {
			$messge = array('message_del' => 'Login Required.','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
               redirect('Home');
       	}
        
    }
    
    public function status($status)
	{
	  if ($this->session->userdata('is_admin_login')) {
		    $this->db->select('tbl_vehicle.vhl_name,tbl_vehicle.vhl_id,tbl_package.package_id,tbl_package.package_name,tbl_washer.user_id as washer_id,tbl_washer.user_fname as washer_fname,tbl_washer.user_lname as washer_lname,tbl_washer.user_id,tbl_user.user_fname,tbl_user.user_lname,tbl_booking.* ');
		    $this->db->join('tbl_vehicle','tbl_booking.vehicle_id=tbl_vehicle.vhl_id','LEFT');
		    $this->db->join('tbl_package','tbl_booking.subpackage_id=tbl_package.package_id','LEFT');
		    $this->db->join('tbl_user','tbl_user.user_id=tbl_booking.user_id','LEFT');
		    $this->db->join('tbl_washer','tbl_washer.user_id=tbl_booking.washer_id','LEFT');
		    $this->db->where('tbl_booking.booking_status',$status);
		    $data['booking']=$this->db->get_where('tbl_booking')->result_array();
		    $data['report_name']=$status.' ';
		    $this->load->view('booking/report',$data);
        } 
        else
        {
			$messge = array('message_del' => 'Login Required.','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
               redirect('Home');
       	}
        
    }
    
    public function washer()
	{
	  if ($this->session->userdata('is_admin_login')) {
		    $this->db->select('tbl_vehicle.vhl_name,tbl_vehicle.vhl_id,tbl_package.package_id,tbl_package.package_name,tbl_washer.user_id as washer_id,tbl_washer.user_fname as washer_fname,tbl_washer.user_lname as washer_lname,tbl_washer.user_id,tbl_user.user_fname,tbl_user.user_lname,tbl_booking.* ');
		    $this->db->join('tbl_vehicle','tbl_booking.vehicle_id=tbl_vehicle.vhl_id','LEFT');
		    $this->db->join('tbl_package','tbl_booking.subpackage_id=tbl_package.package_id','LEFT');
		    $this->db->join('tbl_user','tbl_user.user_id=tbl_booking.user_id','LEFT');
		    $this->db->join('tbl_washer','tbl_washer.user_id=tbl_booking.washer_id','LEFT');
		    $this->db->where('tbl_booking.washer_id',$_POST['washer_id']);
		    $data['booking']=$this->db->get_where('tbl_booking')->result_array();
		    $data['report_name']='Washer';
		    $this->load->view('booking/report_washer',$data);
        } 
        else
        {
			$messge = array('message_del' => 'Login Required.','message_type' => 'error');
            $this->session->set_flashdata('item', $messge);
               redirect('Home');
       	}
        
    }
}
?>