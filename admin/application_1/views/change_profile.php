<?php 
include"include/header.php";
include"include/sidebar.php";
?>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">General Setting</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url();?>Change_Profile/edit_prf" method="POST">
                  <input type="hidden" value="" name="old_pass" />
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Name</label>
                            <input name="admin_name" type="text" class="form-control"  value="<?= $pf_data['admin_name'];?>" id="exampleInputEmail1" placeholder="Enter Name">
                        </div>
                       <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Email</label>
                            <input name="admin_email" type="email" value="<?= $pf_data['admin_email'];?>" class="form-control" id="exampleInputEmail1" placeholder="Enter Email">
                        </div>
                         
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="add_cpf"  value="10" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
           <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Change Password</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                <form role="form" action="<?=base_url();?>Change_Profile/edit_pass" method="POST">
                  <input type="hidden" value="<?= $pf_data['admin_password'];?>" name="old_pass" />   
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Old Password</label>
                            <input name="ol_password" type="password" class="form-control"   id="exampleInputEmail1" placeholder="Enter Old Password">
                        </div>
                       <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">New Password</label>
                            <input name="new_password" type="password"  class="form-control" id="exampleInputEmail1" placeholder="Enter New Password">
                        </div>
                         
                    </div>
                    
                 
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="add_caty"  value="14" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include"include/footer.php";
?> 
