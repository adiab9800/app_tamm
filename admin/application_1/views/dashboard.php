 <link rel="stylesheet" href="<?=base_url(); ?>assets/css/app.min.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/bundles/jqvmap/dist/jqvmap.min.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/bundles/flag-icon-css/css/flag-icon.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?=base_url(); ?>assets/css/style.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?=base_url(); ?>assets/css/custom.css">

<?php include('include/header.php');
      include('include/sidebar.php');
      $date=date('Y-m-d');
      $total_booking=$this->db->query("SELECT COUNT(booking_id) as total FROM tbl_booking ")->row_array();
      $today_booking=$this->db->query("SELECT COUNT(booking_id) as total,SUM(total_amount) as total_amt FROM tbl_booking WHERE service_date='$date' ")->row_array();
      $total_user=$this->db->query("SELECT COUNT(user_id) as total FROM tbl_user")->row_array();
     $pending_booking=$this->db->query("SELECT COUNT(booking_id) as total FROM tbl_booking WHERE booking_status='Pending' ")->row_array();
  
?>

    <section class="section">
          <div class="row ">
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="card">
                <div class="card-statistic-4">
                  <div class="align-items-center justify-content-between">
                    <div class="row ">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                        <div class="card-content">
                          <h5 class="font-15">Today's Booking</h5>
                          <h2 class="mb-3 font-18"><?=$today_booking['total']; ?></h2>
                          <p class="mb-0"><span class="col-green">10%</span> Increase</p>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                        <div class="banner-img">
                          <img src="<?=base_url(); ?>assets/img/banner/1.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="card">
                <div class="card-statistic-4">
                  <div class="align-items-center justify-content-between">
                    <div class="row ">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                        <div class="card-content">
                          <h5 class="font-15"> Customers</h5>
                          <h2 class="mb-3 font-18"><?=$total_user['total']; ?></h2>
                          <p class="mb-0"><span class="col-orange">09%</span> Decrease</p>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                        <div class="banner-img">
                          <img src="<?=base_url(); ?>assets/img/banner/2.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="card">
                <div class="card-statistic-4">
                  <div class="align-items-center justify-content-between">
                    <div class="row ">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                        <div class="card-content">
                          <h5 class="font-15">Pending Orders</h5>
                          <h2 class="mb-3 font-18"><?=$pending_booking['total']; ?></h2>
                          <p class="mb-0"><span class="col-green">18%</span>
                            Increase</p>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                        <div class="banner-img">
                          <img src="<?=base_url(); ?>assets/img/banner/3.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="card">
                <div class="card-statistic-4">
                  <div class="align-items-center justify-content-between">
                    <div class="row ">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                        <div class="card-content">
                          <h5 class="font-15">Today's Revenue</h5>
                          <h2 class="mb-3 font-18">₹ <?=$today_booking['total_amt'] ?></h2>
                          <p class="mb-0"><span class="col-green">42%</span> Increase</p>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                        <div class="banner-img">
                          <img src="<?=base_url(); ?>assets/img/banner/4.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="card col-12">
            <div class="card-header">
               <div class="col-md-9">
                     <h4 >Today's Booking</h4>
                  </div>
                 <div class="col-md-3">
                 </div>   
             
             </div>
            <!-- /.card-header -->
                   <div class="card-body">
               <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Customer</th>
                  <th>Vehicle</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php $sr_no = "1";
                 foreach($booking as $val_data){
                 
                 if($val_data['booking_status']=='Pending')
                 {
                 $booking_status_div='danger';}
                 else{
                      $booking_status_div='primary'; } ?>
                 <tr>
                   <td><?=$sr_no;?></td>
                   <td><?=$val_data['user_fname'];?>&nbsp;<?=$val_data['user_lname'];?></td>
                   
                   <td><?=$val_data['vhl_no'];?></td>
                   <td>₹ <?=$val_data['total_amount'];?></td>
                   <td><button class="btn btn-<?=$booking_status_div; ?>"> <?=$val_data['booking_status'];?></button></td>
                   <td>
                       <a href="<?= base_url();?>Booking/Edit/<?= $val_data['booking_id'];?>"><i class="nav-icon fas fa-edit"></i></a>
                   </td>
                 </tr>
                <?php $sr_no++;}?>
                </tbody>
                <tfoot>
               <tr>
                  <th>#</th>
                  <th>Customer</th>
                  <th>Vehicle</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                 </tfoot>
              </table>
                 </div>
             </div>
           </div> 
         </div>
         <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-6">
                <div class="card">
                  <div class="card-header">
                    <h4>Earning</h4>
                  </div>
                  <div class="card-body">
                    <div class="recent-report__chart">
                      <div id="donutChart"></div>
                    </div>
                  </div>
                </div>
              </div>
          
   
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-6">
                <div class="card">
                  <div class="card-header">
                    <h4>Users</h4>
                  </div>
                  <div class="card-body">
                    <div class="recent-report__chart">
                      <div id="barChart"></div>
                    </div>
                  </div>
                </div>
              </div>
             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-6">
                <div class="card">
                  <div class="card-header">
                    <h4>Washer</h4>
                  </div>
                  <div class="card-body">
                    <div class="recent-report__chart">
                      <div id="pieChart"></div>
                    </div>
                  </div>
                </div>
              </div>
             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-6">
                <div class="card">
                  <div class="card-header">
                    <h4>Bookings</h4>
                  </div>
                  <div class="card-body">
                    <div class="recent-report__chart">
                      <div id="chart2"></div>
                    </div>
                  </div>
                </div>
              </div>
          
            
        </div> 
         </section>
 <script src="<?=base_url(); ?>assets/js/app.min.js"></script>
  <script src="<?=base_url(); ?>assets/bundles/chartjs/chart.min.js"></script>
  <script src="<?=base_url(); ?>assets/bundles/jquery.sparkline.min.js"></script>
  <script src="<?=base_url(); ?>assets/bundles/apexcharts/apexcharts.min.js"></script>
  <script src="<?=base_url(); ?>assets/bundles/jqvmap/dist/jquery.vmap.min.js"></script>
  <script src="<?=base_url(); ?>assets/bundles/jqvmap/dist/maps/jquery.vmap.world.js"></script>
  <script src="<?=base_url(); ?>assets/bundles/jqvmap/dist/maps/jquery.vmap.indonesia.js"></script>
  <!--<script src="<?=base_url(); ?>assets/js/page/widget-chart.js"></script>-->
  
  <script src="<?=base_url(); ?>assets/bundles/amcharts4/core.js"></script>
  <script src="<?=base_url(); ?>assets/bundles/amcharts4/charts.js"></script>
  <script src="<?=base_url(); ?>assets/bundles/amcharts4/animated.js"></script>
  <script src="<?=base_url(); ?>assets/bundles/amcharts4/worldLow.js"></script>
  <script src="<?=base_url(); ?>assets/bundles/amcharts4/maps.js"></script>
  <!-- Page Specific JS File -->
  <!--<script src="<?=base_url(); ?>assets/js/page/chart-amchart.js"></script>-->
  <!-- Template JS File -->
 <script src="<?=base_url(); ?>assets/bundles/apexcharts/apexcharts.min.js"></script>
  <!-- Page Specific JS File -->
 
  
  <script src="<?=base_url(); ?>assets/js/scripts.js"></script>
  <script src="<?=base_url(); ?>assets/js/custom.js"></script>

<?php //include('include/footer.php');?> 



<script>
 
$(function () {
    chart2();
    barChart();
    pieChart();
    donutChart();
});

function donutChart() {

$.ajax({
        url: "<?=base_url(); ?>home/year_earning_count",
        success: function (response) {  
   am4core.useTheme(am4themes_animated);
  var chart = am4core.create("donutChart", am4charts.PieChart);

  var json = $.parseJSON(response);
   chart.data =json;

  // Set inner radius
  chart.innerRadius = am4core.percent(50);

  // Add and configure Series
  var pieSeries = chart.series.push(new am4charts.PieSeries());
  pieSeries.dataFields.value = "visits";
  pieSeries.dataFields.category = "country";
  pieSeries.slices.template.stroke = am4core.color("#fff");
  pieSeries.slices.template.strokeWidth = 2;
  pieSeries.slices.template.strokeOpacity = 1;
  pieSeries.labels.template.fill = am4core.color("#9aa0ac");

  // This creates initial animation
  pieSeries.hiddenState.properties.opacity = 1;
  pieSeries.hiddenState.properties.endAngle = -90;
  pieSeries.hiddenState.properties.startAngle = -90;
        }
    });
}

function pieChart() {
 $.ajax({
        url: "<?=base_url(); ?>home/year_washer_count",
        success: function (response) {  
    am4core.useTheme(am4themes_animated);
  // Themes end

  // Create chart instance
  var chart = am4core.create("pieChart", am4charts.PieChart);

   var json = $.parseJSON(response);
   chart.data =json;

  
  // Add and configure Series
  var pieSeries = chart.series.push(new am4charts.PieSeries());
  pieSeries.dataFields.value = "visits";
  pieSeries.dataFields.category = "country";
  pieSeries.slices.template.stroke = am4core.color("#fff");
  pieSeries.slices.template.strokeWidth = 2;
  pieSeries.slices.template.strokeOpacity = 1;
  pieSeries.labels.template.fill = am4core.color("#9aa0ac");

  // This creates initial animation
  pieSeries.hiddenState.properties.opacity = 1;
  pieSeries.hiddenState.properties.endAngle = -90;
  pieSeries.hiddenState.properties.startAngle = -90;
        }
 });
}

function barChart() {
   $.ajax({
        url: "<?=base_url(); ?>home/year_user_count",
        success: function (response1) { 
          am4core.useTheme(am4themes_animated);
          var chart = am4core.create("barChart", am4charts.XYChart);
          chart.scrollbarX = new am4core.Scrollbar();
          var json = $.parseJSON(response1);
          chart.data =json;

          // Create axes
          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "country";
          categoryAxis.renderer.grid.template.location = 0;
          categoryAxis.renderer.minGridDistance = 30;
          categoryAxis.renderer.labels.template.horizontalCenter = "right";
          categoryAxis.renderer.labels.template.verticalCenter = "middle";
          categoryAxis.renderer.labels.template.rotation = 270;
          categoryAxis.tooltip.disabled = true;
          categoryAxis.renderer.minHeight = 110;
          categoryAxis.renderer.labels.template.fill = am4core.color("#9aa0ac");
        
          var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
          valueAxis.renderer.minWidth = 50;
          valueAxis.renderer.labels.template.fill = am4core.color("#9aa0ac");
        
          // Create series
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.sequencedInterpolation = true;
          series.dataFields.valueY = "visits";
          series.dataFields.categoryX = "country";
          series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
          series.columns.template.strokeWidth = 0;
        
        
          series.tooltip.pointerOrientation = "vertical";
        
          series.columns.template.column.cornerRadiusTopLeft = 10;
          series.columns.template.column.cornerRadiusTopRight = 10;
          series.columns.template.column.fillOpacity = 0.8;
        
          // on hover, make corner radiuses bigger
          let hoverState = series.columns.template.column.states.create("hover");
          hoverState.properties.cornerRadiusTopLeft = 0;
          hoverState.properties.cornerRadiusTopRight = 0;
          hoverState.properties.fillOpacity = 1;
        
          series.columns.template.adapter.add("fill", (fill, target) => {
            return chart.colors.getIndex(target.dataItem.index);
          })
        
          // Cursor
          chart.cursor = new am4charts.XYCursor();
        }
   });
    
}


    function chart2() {

    $.ajax({
        url: "<?=base_url(); ?>home/year_booking_count",
        success: function (response) {  
        
        var options = {
            chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        position: 'top', // top, center, bottom
                    },
                }
            },
            dataLabels: {
                enabled: true,
                formatter: function (val) {
                    return val;
                },
                offsetY: -20,
                style: {
                    fontSize: '12px',
                    colors: ["#9aa0ac"]
                }
            },
            series: [{
                name: 'Bookings',
                data: response
            }],
            xaxis: {
                categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                position: 'top',
                labels: {
                    offsetY: -18,
                    style: {
                        colors: '#9aa0ac',
                    }
                },
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false
                },
                crosshairs: {
                    fill: {
                        type: 'gradient',
                        gradient: {
                            colorFrom: '#D8E3F0',
                            colorTo: '#BED1E6',
                            stops: [0, 100],
                            opacityFrom: 0.4,
                            opacityTo: 0.5,
                        }
                    }
                },
                tooltip: {
                    enabled: true,
                    offsetY: -35,
    
                }
            },
            fill: {
                gradient: {
                    shade: 'light',
                    type: "horizontal",
                    shadeIntensity: 0.25,
                    gradientToColors: undefined,
                    inverseColors: true,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [50, 0, 100, 100]
                },
            },
            yaxis: {
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false,
                },
                labels: {
                    show: false,
                    formatter: function (val) {
                        return val;
                    }
                }
    
            },
            title: {
                text: 'Monthly Booking Report',
                floating: true,
                offsetY: 320,
                align: 'center',
                style: {
                    color: '#9aa0ac'
                }
            },
        }
    
        var chart = new ApexCharts(
            document.querySelector("#chart2"),
            options
        );
    
        chart.render();
     }
      
});
}

    
</script>