<?php 
include "include/header.php";
include "include/sidebar.php";
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0)">Banner</a></li>
        </ol>
        
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">BANNER DETAIL</h3>
              <button type="button" class="btn mb-1 btn-primary" style="    float: right;"><a href="<?= base_url();?>Banner/add" style="color:white;">Add Banner</a></button>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                   <th style="width:2%;">#</th>
                   <th style="width:10%;">Image</th>
                   <th style="width:10%;">Status</th>
                   <th style="width:25%;">Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php $i=1;
                                foreach($slider_data as $value){?>

                                    <tr>
                                    <td><?= $i;?></td>
                                    <td><img src="<?= base_url();?>upload/banner/<?= $value['banner_img'];?>" style="height: 60px;width: 100%;"/></td>
                                    <td>
                                    <input type="hidden" name="banner_id" id="banner_id" value="<?= $value['banner_id'];?>" />
                                       <select name="order_status_edit" id="order_status_edit" class="form-group">
                                       
                                        <option value="0" <?php if($value['banner_status'] == '0'){ echo "Selected";}?>>Active</option>
                                        <option value="1" <?php if($value['banner_status'] == '1'){ echo "Selected";}?>>Inactive</option>
                                        </select>
                                        
                                    </td>
                                    <td>
                                      <a onclick="return confirm('Are You Sure Delete Data?');" href="<?= base_url();?>Banner/delete/<?= $value['banner_id'];?>/<?= $value['banner_img'];?>"><i class="fa fa-fw fa-trash"></i> </a>
                                    </td>
                                   
                                </tr>
                                <?php $i++;} ?>
                </tbody>
                <tfoot>
                <tr>
                   <th style="width:2%;">#</th>
                   <th style="width:10%;">Image</th>
                   <th style="width:10%;">Status</th>
                   <th style="width:25%;">Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <?php
 include "include/footer.php";
?> 
    

<script>
$('select#order_status_edit').on('change',function(){
    //var address_state = $('#update_return_status').val();
	var status_val = this.value;   
	var banner_id=$(this).closest("tr").find("input[id=banner_id]").val(); 
	

  $.ajax({       
       type : "POST",
       url  : "<?= base_url();?>Banner/update_Slider_status/"+banner_id+"/"+status_val,
       dataType : "JSON",
       data : {status_val:status_val,banner_id:banner_id},
	  
	 
	  success : function(data){
			  alert("Successfully Change Status");
			   window.location.reload();
		      
	  }	   
	  
   });   
});
</script>